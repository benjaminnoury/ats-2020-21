#!/usr/bin/gnuplot -persist
# vi:syntax=gnuplot 

reset

set terminal cairolatex pdf size 9,4 #mono
set palette cubehelix

set key top left 

tt=tan(73*pi/180)
a=240
b=3750
c=200
P=5000

set loadpath '~/.config/gnuplot-config/latex'
load 'xyborder.cfg'
load 'grid.cfg'

set xlabel '$\lambda$ (mm)'
set ylabel 'Efforts (N)'

set xrange [a:b]
set xtics 200
set mxtics 4
set mytics 2
set label 1 at  1200, -900 '{\small $Y_{32}$}' center rotate by -5 front
set label 2 at  400, 5000 '{\small $Y_{13}$}' center rotate by -5 front
set label 3 at  2600, 10200 '{\small $X_{13}$}' center rotate by +10 front


plot -x/(c*tt+b)*P t '$Y_{32}$' lw 4, +x*tt/(c*tt+b) *P t '$X_{13}$' lw 4, (1-x/(c*tt+b))*P t '$Y_{13}$' lw 4

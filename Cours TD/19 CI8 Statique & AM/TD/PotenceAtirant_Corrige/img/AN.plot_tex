% GNUPLOT: LaTeX picture with Postscript
\begingroup
  \makeatletter
  \providecommand\color[2][]{%
    \GenericError{(gnuplot) \space\space\space\@spaces}{%
      Package color not loaded in conjunction with
      terminal option `colourtext'%
    }{See the gnuplot documentation for explanation.%
    }{Either use 'blacktext' in gnuplot or load the package
      color.sty in LaTeX.}%
    \renewcommand\color[2][]{}%
  }%
  \providecommand\includegraphics[2][]{%
    \GenericError{(gnuplot) \space\space\space\@spaces}{%
      Package graphicx or graphics not loaded%
    }{See the gnuplot documentation for explanation.%
    }{The gnuplot epslatex terminal needs graphicx.sty or graphics.sty.}%
    \renewcommand\includegraphics[2][]{}%
  }%
  \providecommand\rotatebox[2]{#2}%
  \@ifundefined{ifGPcolor}{%
    \newif\ifGPcolor
    \GPcolortrue
  }{}%
  \@ifundefined{ifGPblacktext}{%
    \newif\ifGPblacktext
    \GPblacktexttrue
  }{}%
  % define a \g@addto@macro without @ in the name:
  \let\gplgaddtomacro\g@addto@macro
  % define empty templates for all commands taking text:
  \gdef\gplbacktext{}%
  \gdef\gplfronttext{}%
  \makeatother
  \ifGPblacktext
    % no textcolor at all
    \def\colorrgb#1{}%
    \def\colorgray#1{}%
  \else
    % gray or color?
    \ifGPcolor
      \def\colorrgb#1{\color[rgb]{#1}}%
      \def\colorgray#1{\color[gray]{#1}}%
      \expandafter\def\csname LTw\endcsname{\color{white}}%
      \expandafter\def\csname LTb\endcsname{\color{black}}%
      \expandafter\def\csname LTa\endcsname{\color{black}}%
      \expandafter\def\csname LT0\endcsname{\color[rgb]{1,0,0}}%
      \expandafter\def\csname LT1\endcsname{\color[rgb]{0,1,0}}%
      \expandafter\def\csname LT2\endcsname{\color[rgb]{0,0,1}}%
      \expandafter\def\csname LT3\endcsname{\color[rgb]{1,0,1}}%
      \expandafter\def\csname LT4\endcsname{\color[rgb]{0,1,1}}%
      \expandafter\def\csname LT5\endcsname{\color[rgb]{1,1,0}}%
      \expandafter\def\csname LT6\endcsname{\color[rgb]{0,0,0}}%
      \expandafter\def\csname LT7\endcsname{\color[rgb]{1,0.3,0}}%
      \expandafter\def\csname LT8\endcsname{\color[rgb]{0.5,0.5,0.5}}%
    \else
      % gray
      \def\colorrgb#1{\color{black}}%
      \def\colorgray#1{\color[gray]{#1}}%
      \expandafter\def\csname LTw\endcsname{\color{white}}%
      \expandafter\def\csname LTb\endcsname{\color{black}}%
      \expandafter\def\csname LTa\endcsname{\color{black}}%
      \expandafter\def\csname LT0\endcsname{\color{black}}%
      \expandafter\def\csname LT1\endcsname{\color{black}}%
      \expandafter\def\csname LT2\endcsname{\color{black}}%
      \expandafter\def\csname LT3\endcsname{\color{black}}%
      \expandafter\def\csname LT4\endcsname{\color{black}}%
      \expandafter\def\csname LT5\endcsname{\color{black}}%
      \expandafter\def\csname LT6\endcsname{\color{black}}%
      \expandafter\def\csname LT7\endcsname{\color{black}}%
      \expandafter\def\csname LT8\endcsname{\color{black}}%
    \fi
  \fi
    \setlength{\unitlength}{0.0500bp}%
    \ifx\gptboxheight\undefined%
      \newlength{\gptboxheight}%
      \newlength{\gptboxwidth}%
      \newsavebox{\gptboxtext}%
    \fi%
    \setlength{\fboxrule}{0.5pt}%
    \setlength{\fboxsep}{1pt}%
\begin{picture}(12960.00,5760.00)%
    \gplgaddtomacro\gplbacktext{%
      \colorrgb{0.50,0.50,0.50}%%
      \put(932,712){\makebox(0,0)[r]{\strut{}$-6000$}}%
      \colorrgb{0.50,0.50,0.50}%%
      \put(932,1194){\makebox(0,0)[r]{\strut{}$-4000$}}%
      \colorrgb{0.50,0.50,0.50}%%
      \put(932,1677){\makebox(0,0)[r]{\strut{}$-2000$}}%
      \colorrgb{0.50,0.50,0.50}%%
      \put(932,2159){\makebox(0,0)[r]{\strut{}$0$}}%
      \colorrgb{0.50,0.50,0.50}%%
      \put(932,2641){\makebox(0,0)[r]{\strut{}$2000$}}%
      \colorrgb{0.50,0.50,0.50}%%
      \put(932,3124){\makebox(0,0)[r]{\strut{}$4000$}}%
      \colorrgb{0.50,0.50,0.50}%%
      \put(932,3606){\makebox(0,0)[r]{\strut{}$6000$}}%
      \colorrgb{0.50,0.50,0.50}%%
      \put(932,4088){\makebox(0,0)[r]{\strut{}$8000$}}%
      \colorrgb{0.50,0.50,0.50}%%
      \put(932,4570){\makebox(0,0)[r]{\strut{}$10000$}}%
      \colorrgb{0.50,0.50,0.50}%%
      \put(932,5053){\makebox(0,0)[r]{\strut{}$12000$}}%
      \colorrgb{0.50,0.50,0.50}%%
      \put(932,5535){\makebox(0,0)[r]{\strut{}$14000$}}%
      \colorrgb{0.50,0.50,0.50}%%
      \put(1628,448){\makebox(0,0){\strut{}$400$}}%
      \colorrgb{0.50,0.50,0.50}%%
      \put(2283,448){\makebox(0,0){\strut{}$600$}}%
      \colorrgb{0.50,0.50,0.50}%%
      \put(2939,448){\makebox(0,0){\strut{}$800$}}%
      \colorrgb{0.50,0.50,0.50}%%
      \put(3594,448){\makebox(0,0){\strut{}$1000$}}%
      \colorrgb{0.50,0.50,0.50}%%
      \put(4249,448){\makebox(0,0){\strut{}$1200$}}%
      \colorrgb{0.50,0.50,0.50}%%
      \put(4904,448){\makebox(0,0){\strut{}$1400$}}%
      \colorrgb{0.50,0.50,0.50}%%
      \put(5559,448){\makebox(0,0){\strut{}$1600$}}%
      \colorrgb{0.50,0.50,0.50}%%
      \put(6215,448){\makebox(0,0){\strut{}$1800$}}%
      \colorrgb{0.50,0.50,0.50}%%
      \put(6870,448){\makebox(0,0){\strut{}$2000$}}%
      \colorrgb{0.50,0.50,0.50}%%
      \put(7525,448){\makebox(0,0){\strut{}$2200$}}%
      \colorrgb{0.50,0.50,0.50}%%
      \put(8180,448){\makebox(0,0){\strut{}$2400$}}%
      \colorrgb{0.50,0.50,0.50}%%
      \put(8836,448){\makebox(0,0){\strut{}$2600$}}%
      \colorrgb{0.50,0.50,0.50}%%
      \put(9491,448){\makebox(0,0){\strut{}$2800$}}%
      \colorrgb{0.50,0.50,0.50}%%
      \put(10146,448){\makebox(0,0){\strut{}$3000$}}%
      \colorrgb{0.50,0.50,0.50}%%
      \put(10801,448){\makebox(0,0){\strut{}$3200$}}%
      \colorrgb{0.50,0.50,0.50}%%
      \put(11456,448){\makebox(0,0){\strut{}$3400$}}%
      \colorrgb{0.50,0.50,0.50}%%
      \put(12112,448){\makebox(0,0){\strut{}$3600$}}%
    }%
    \gplgaddtomacro\gplfronttext{%
      \csname LTb\endcsname%%
      \put(186,3123){\rotatebox{-270}{\makebox(0,0){\strut{}Efforts (N)}}}%
      \csname LTb\endcsname%%
      \put(6853,142){\makebox(0,0){\strut{}$\lambda$ (mm)}}%
      \csname LTb\endcsname%%
      \put(1552,5352){\makebox(0,0)[r]{\strut{}$Y_{32}$}}%
      \csname LTb\endcsname%%
      \put(1552,5148){\makebox(0,0)[r]{\strut{}$X_{13}$}}%
      \csname LTb\endcsname%%
      \put(1552,4944){\makebox(0,0)[r]{\strut{}$Y_{13}$}}%
      \csname LTb\endcsname%%
      \put(4249,1942){\rotatebox{-5}{\makebox(0,0){\small $Y_{32}$}}}%
      \csname LTb\endcsname%%
      \put(1628,3365){\rotatebox{-5}{\makebox(0,0){\small $Y_{13}$}}}%
      \csname LTb\endcsname%%
      \put(8836,4619){\rotatebox{10}{\makebox(0,0){\small $X_{13}$}}}%
    }%
    \gplbacktext
    \put(0,0){\includegraphics[width={648.00bp},height={288.00bp}]{AN}}%
    \gplfronttext
  \end{picture}%
\endgroup

---
titre: Statique
type: Problème Corrigé
classe: ATS
CI: 8 - Transmettre l'énergie --- Efforts
competence: B2 Résoudre 
#question-section: 1
#ficheTD: 1
#document: twocolumn,landscape
---

# Console portante de bateau

On s’intéresse à un système de console portante
de bateau destinée à mettre les bateaux à l’eau ou
à les en retirer à partir d’un quai dans les ports de plaisance.

Un modèle de ce système est représenté par son
schéma cinématique ci-dessous.

La console **1** est en liaison avec le quai **0** par l’intermédiaire d’une liaison sphérique de centre $A$ et d’une liaison sphère cylindre de centre **B** et de direction $\vec z$

Le bateau 4 est maintenu à l’aide de câbles sur la
console **1**.
Un vérin (corps **2** + tige **3**) permet de faire pivoter la console **1** autour de l'axe $(B,\vec z)$

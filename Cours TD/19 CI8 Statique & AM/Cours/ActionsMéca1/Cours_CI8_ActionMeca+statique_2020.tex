\documentclass[]{article}
\usepackage{branly}
\usepackage{notations}
\usepackage{tableaux}
%\usepackage[europeanresistors,americaninductors,straightvoltages]{circuitikz}
\usepackage{boites}
\usepackage{tikz}\usetikzlibrary{patterns,babel}

\begin{document}
\title{Modéliser les actions mécaniques}
\type{Cours\ CI 8 Transmettre l'énergie --- Efforts}
\classe{ATS}
% \flushleft\textbf{Compétence B2 : Proposer un modèle de connaissance et de comportement}
\maketitle
% \subsubsection{Savoirs-faire abordés}
  \begin{tabularx}{\textwidth}{lX}
  % \toprule
  \textbf{Modéliser} & \labelitemi \ Associer à une liaison un torseur d’action mécanique transmissible. \\
                       & \labelitemi \  Associer un modèle à une action mécanique \\
                       & \labelitemi \  Écrire la relation entre modèle local et modèle global dans le cas d’actions réparties \\
                       & \labelitemi \   Réaliser l’inventaire des actions mécaniques extérieures s’exerçant sur un solide ou un ensemble de solides \\
                       % \midrule
                       \addlinespace
  \textbf{Résoudre} & \labelitemi \  Proposer une méthode permettant la détermination des inconnues de liaison. \\
% \bottomrule
\end{tabularx}

\section*{Objectif}
Tout mécanisme est dimensionné pour pouvoir être utilisé pendant un
temps donné. Or, la durée de vie d'une pièce dépend généralement :

\begin{itemize}\tightlist
  \item  de l'environnement dans lequel elle se trouve,
  \item  de ses dimensions,
  \item  du matériau utilisé,
  \item  \ldots{}
  \item  mais surtout des actions appliquées sur la pièce.
\end{itemize}

% \medskip

Ces actions peuvent être mesurées mais cela demande la construction d'un
prototype et/ou la mise en place d'un laboratoire de mesure. Ces
réalisations sont beaucoup trop coûteuses.


On va donc chercher à
\textbf{PRÉVOIR les actions appliquées sur un mécanisme} en utilisant des
modèles mathématiques et des lois physiques.

\section{Actions mécaniques : Généralités}

\begin{description}
  \item[Définition :] On appellera action mécanique, tout phénomène \emph{( cause )} susceptible de :
    \begin{itemize} \tightlist
      \item  provoquer ou modifier le mouvement
      \item  maintenir un corps au repos
      \item  produire des déformations
    \end{itemize}
  \item[ Problématique : ]Comment modéliser l'action d'une pièce sur une autre.
    \textit{(modéliser = remplacer un problème réel par un problème de
    mathématiques)}
\end{description}

\textbf{Exemple :} Comment modéliser l'action de la pièce sur un doigt de la
pince au point L.

\begin{minipage}{.48\textwidth}
  \centering
  \includegraphics[width=0.7\textwidth]{img/robot.png}
\end{minipage}\hfill
\begin{minipage}{.48\textwidth}
  \centering
  \import{./img/}{pince.pdf_tex}
\end{minipage}




\subsection{Modèle global / Modèle local}

Suivant l'étude à mener, nous avons à notre disposition 2 modélisations
possibles du même phénomène.

\begin{minipage}{.55\textwidth}
  \textbf{Le modèle local}, qui conduit à étudier l'action en tout point de la zone où elle s'exerce (pression de contact, contraintes, champ de pesanteur). C'est une modélisation \textbf{vue de près}. 
\end{minipage}\hfill
\begin{minipage}{.4\textwidth}
  \centering
  \import{./img/}{pinceLocal.pdf_tex}
\end{minipage}


\begin{minipage}{.55\textwidth}
  \textbf{Le modèle global}, qui proposera une représentation de l'action par un torseur qui fera disparaître la répartition des efforts. C'est une modélisation \textbf{vue de loin}
\end{minipage}\hfill
\begin{minipage}{.4\textwidth}
  \centering
  \import{./img/}{pinceGlobal.pdf_tex}
\end{minipage}


\subsection{Résultante, moment et couple}
\subsubsection{Résultante d'une action mécanique}

Les 2 modèles sont reliés entre eux par intégration :
\(\displaystyle \vec F(\text{pièce} \to \text{doigt}) = \iint_{Q \in S} \overrightarrow{\dd F_{\text{pièce} \to \text{doigt}}(Q)} \dd S\)

\begin{figure}[htb]
		\centering
		\def\svgwidth{.8\columnwidth}
		\import{./img/}{pinceGlobalLocal3D.pdf_tex}
\end{figure}

% \begin{figure}[htb]
%   \centering
%   \import{./img/}{pinceGlobalLocal3D.pdf_tex}
% \end{figure}

L'action mécanique est modélisée en un point particulier L par un
vecteur \(\vec F(\text{pièce} \rightarrow \text{doigt})\) appelé
\textbf{résultante d'action mécanique} de la pièce \textbf{sur} le doigt.

L'unité de la résultante
\(\vec F(\text{pièce} \rightarrow \text{doigt})\) est le
\textbf{NEWTON (N)}. Cette résultante est également appelée \textbf{Force}.

\subsubsection{Moment d'une action mécanique}

La modélisation de l'action mécanique par une \textbf{résultante} en un
point particulier est :

\begin{itemize}\tightlist
  \item \textbf{suffisante} pour un point appartenant au support de l’action, puisqu’elle prend en compte l’action de tirer ou pousser.
  \item \textbf{insuffisante} pour un point n’appartenant pas au support de l’action, puisqu’elle ne prend pas en compte l’action de tordre, tourner, visser ou dévisser.
\end{itemize}

\medskip

Si on s'intéresse à l'effet de l'action mécanique précédente au point
\(O\), celle-ci a tendance à :

\begin{itemize}
  \item pousser le doigt dans une direction verticale parallèle à $\vec F(\text{pièce} \rightarrow \text{doigt})$
  \item faire tourner le doigt autour de l’axe $(O,\vec x)$
\end{itemize}

\begin{figure}[htb]
	\def\svgwidth{.8\columnwidth}
  \centering
  \import{./img/}{pinceMoment.pdf_tex}
\end{figure}

Nous représenterons donc cette action mécanique :

\begin{itemize}
  \item par une \textbf{résultante} $\vec F(\text{pièce} \rightarrow \text{doigt})$ \textbf{qui a tendance à pousser} dans une direction.  \newline (résultante inchangée par rapport à celle modélisée en L)
  \item et par un deuxième vecteur appelé \textbf{moment} et noté $\vec M_{O}(\text{pièce} \rightarrow \text{doigt})$ \textbf{qui a tendance à faire tourner} autour d’un axe.
\end{itemize}

\medskip

L'unité du moment est le \textbf{NEWTON × MÊTRE (N.m)}

\begin{asavoir}{Torseur des actions mécaniques}
  On regroupe la résultante et le moment de l'action mécanique dans un torseur nommé le torseur des actions mécaniques
  \[
    \tam{1 \to 2}=
    \torseur{A}{\F (1 \rightarrow 2)\\ \Mam{A}(1\rightarrow 2)}
    = \torseur{A}{X \cdot \vec x + Y \cdot \vec y + Z \cdot \vec z \\\\ L\cdot  \vec x + M\cdot  \vec y + N\cdot  \vec z}
    =\torseur[B_0]{A}{X & L \\ Y & M\\ Z &N}
  \]

  $B_0$ est ici la base d'expression du torseur, elle n'a pas besoin d'être liée à $1$ ou $2$

  \medskip 
  La force $\F (1 \rightarrow 2)$ étant une résultante, elle ne dépend pas du point.
  \medskip

  Le moment est défini en un point B à partir de son expression en un point A par la relation de Varignon : 
  $$ \Mam{B}(1\rightarrow 2) =  \Mam{A}(1\rightarrow 2) + \overrightarrow{BA} \wedge \vec F (1 \rightarrow 2) $$

\end{asavoir}

\begin{minipage}{.68\textwidth}
  \subsubsection{Couple}
  Un couple est l'action mécanique générée par 2 résultantes (ou un nombre pair, ou une infinité) égales en normes et opposées.
  \medskip

  Si $\vec F\wedge \overrightarrow{AB}\neq \vec 0$, le moment d'une telle action n'est pas nul mais les résultantes s'annulent deux à deux.
  \medskip

  Un couple est représenté par \dots un torseur-couple:
  \[
    \tam{\text{conducteur}\to \text{Volant}}=
  \torseur{Q}{  +\F-\F\\\left[ R\cdot F-R \cdot (-F) \right ]\vec z}
  \torseur{\forall P}{\vec 0\\\\ \vec C=2\cdot R\cdot F\cdot \vec z}=\torseur[b]{\forall P}{ 0 & 0 \\ 0 & 0\\ 0 &C}
\]
\end{minipage}\hfill
\begin{minipage}{.3\textwidth}
  \import{./img/}{_couple.pdf_tex}
\end{minipage}
Ce torseur ne dépend pas du point.

\section{Quelques actions mécaniques à connaître}


\subsubsection{Action de la pesanteur}
\begin{minipage}{.55\textwidth}
  On modélisera l'action de la pesanteur sur un
  solide S par un glisseur au centre de gravité G.
  (Démonstration plus tard dans le cours)

  \begin{itemize}
    \item $m$ est la masse du solide $S$
    \item $g$ est la densité massique de force de la
      pesanteur (g=9,81 N/kg)
    \item $ \vec z_{0}$ est le vecteur indiquant la
      verticale.
  \end{itemize}
\end{minipage}\hfill
\begin{minipage}{.38\textwidth}
  \[
    \tam{\text{Pesanteur }\to S }= \torseur{G}{- m\cdot g\cdot \vec
    z_{0}\\ \vec 0}
  \]
\end{minipage}

\subsubsection{Action d'un moteur}

\begin{minipage}{.55\textwidth}
  On modélisera l'action d'un moteur par un torseur couple.
  \begin{itemize}
    \item $C_{m}$ est le couple du moteur (en N.m)
    \item $\vec x$ la direction de l'axe de rotation du moteur
  \end{itemize}
\end{minipage}\hfill
\begin{minipage}{.38\textwidth}
  \[
    \tam{ \text{Moteur} \to S }= \torseur{\forall P}{ \vec 0 \\ C_{m}\cdot
    \vec x}
  \]
\end{minipage}

\subsubsection{Action transmissible par un ressort}

\begin{minipage}{.55\textwidth}
  \begin{description}
    \item[Pour un ressort R de compression,] l'effort axial nécessaire pour obtenir une déformation $\Delta l=l-l_{0}$ est donné par la relation :
  \end{description}
  \[
    F (\text{Ressort} \to S) = - k \cdot (l - l_{0}) 
  \]
  \begin{itemize}
    \item $k$ est la raideur du ressort en \textbf{N/m};
    \item $l$ est la longueur du ressort en charge ;
    \item $l_{0}$ est la longueur du ressort à vide.
  \end{itemize}
\end{minipage}\hfill
\begin{minipage}{.38\textwidth}
  Pour un ressort de compression d'axe $(A,\vec x)$
  \[
    \tam{\text{Ressort} \to S} = \torseur{A}{ -k \cdot (l-l_{0}) \cdot \vec x \\ \vec 0}
  \]
\end{minipage}

\medskip

\begin{minipage}{.55\textwidth}
  \begin{description}
    \item[Pour un ressort de torsion,] le couple nécessaire pour obtenir
      une torsion $\Delta \theta = \theta - \theta_{0}$ est donné par
      la relation :
      \[
        C(\text{Ressort} \to S)= - K \cdot (\theta - \theta_{0})
      \]
      \begin{itemize}
        \item $K$ est la raideur en torsion du ressort (N.m/rd) ;
        \item $\theta$ est l'angle de torsion du ressort en charge ;
        \item $\theta_0$ est l'angle de torsion du ressort à vide.
      \end{itemize}
  \end{description}
\end{minipage}\hfill
\begin{minipage}{.38\textwidth}
  Pour un ressort de torsion de direction $\vec x$
  \[
    \tam{\text{Ressort} \to S}=\torseur{\forall P}{\vec  0 \\ -K \cdot (\theta-\theta_0)\cdot\vec x}
  \]
\end{minipage}


\section{Action Mécanique transmissible par une liaison
parfaite}

\noindent Il y a une forte corrélation entre le torseur Cinématique
d'une liaison et son torseur des actions transmissibles.

Lorsqu'un degré de liberté est supprimé entre 2 solides 1 et 2, il en
résulte alors une composante dans le torseur de l'action mécanique
transmissible de 1→2 (qui empêche le mouvement).


Exemple : dans le cas d'une liaison pivot parfaite d'axe
\((A,\vec x_1 )\)

\begin{center}
  \begin{tikzpicture}
    \tikzstyle{boite}=[text width=6.5cm, text badly centered, rectangle, draw]

    \node[boite] at (0,0) {Torseur cinématique\\
        \medskip
      \[\tv{2/1} = \torseur[b]{A}{\omega_{x}&0\\0&0\\0&0}\]};

    \node[boite] at (7,0) {Torseur d'action mécanique\\
        \medskip
      \[\tam{1 \to 2} = \torseur[b]{A}{X_{12}&0\\Y_{12}&M_{12}\\Z_{12}&N_{12}}\]};

    \draw[<->] (1.36,-1.2)|-++(0,-1)--++ (7.3-1.25,0)node[midway,above]{"Translation"} --++(0,.95);
    \draw[<->] (.7,-1.2)|-++(0,-1.7)--++(8.1-.6,0)node[midway, above]{"Rotation"}--++(0,1.65);

    \node (A) at (-.5,0.5) {$\vec \Omega({2/1})$};
    \draw[->] (A)-|(0.7,.1);
    \node (B) at (7-.9,0.5) {$\overrightarrow R (1 \to  2)$};
    \draw[->] (B)-|(7.3,.1);

    \node (C) at (2.3,0.5) {$\V{A}({2/1})$};
    \draw[->] (C)-|(.75+0.6,.1);

    \node (D) at (9.5,0.5) {$\Mam{A}({1 \to 2 })$};
    \draw[->] (D)-|(8.1,.1);
  \end{tikzpicture}
\end{center}


\subsubsection{Degrés de liaison}

On appelait \textbf{degrés de liberté} les mouvements possibles dans une
liaison.

Un \textbf{degré de liaison} est un mouvement impossible dans une liaison et
donc une action transmissible.


En dernière page est donné un tableau récapitulatif des 2 torseurs pour chaque liaisons.


\subsubsection{Cas particulier : Problème Plan}

Dans le cas particulier où il existe un même plan de symétrie pour
\begin{itemize}\tightlist
  \item la géométrie et la répartion des masses des corps étudiés (\emph{"la géométrie est contenue dans un plan"}) ;
  \item le chargement (ensemble des actions mécanique appliqué au système) ;
\end{itemize}
on peut alors se ramener à un problème plan. Les actions hors de ce plan ne sont pas considérées.

Par exemple, pour un problème admettant $(O,\vec x, \vec y)$ comme plan d'étude, les torseurs prendront la forme :
\[
  \tam{1\to2}=\torseur[(\vec x,\vec y,\vec z)]{A}{X & 0 \\ Y & 0 \\ 0 & N}
\] 
Pour insister sur le fait que les zéros sont des valeurs fixées par l'hypothèse de problème plan et ne peuvent prendre aucune autre valeur, on les remplace parfois par des $-$.
\[
  \tam{1\to2}=\torseur[(\vec x,\vec y,\vec z)]{A}{X & - \\ Y & - \\ - & N}
\] 
\newpage

\section{Principe fondamental de la statique}

\subsection{Énoncé du principe fondamental de la statique (PFS)}
% \begin{asavoir}{Principe fondamental de la statique}
Pour  un système matériel $E$ au repos dans un référentiel galiléen, la somme des actions mécaniques s'exerçant sur E est nulle.

\[
  \sum_{S\not\in E}  \tam{S \to E} = \{ O \} \text{ \em(torseur nul)}
\] 

\subsection{Application du principe fondamental de la dynamique}

La difficulté dans l'applications du P.F.S est qu'il faut choisir le
système matériel \(E\) à étudier -- on utilise le terme ``Isoler
\(E\)''.

\subsubsection{Bilan des actions mécaniques extérieures}

Quand on isole \(E\), seules les actions mécaniques s'appliquant à \(E\)
interviennent. Répertorier l'ensemble de ces actions s'appelle faire le
\textbf{Bilan des actions mécaniques extérieures}

\subsubsection{Graphe des actions mécaniques}

Pour s'aider à faire ce bilan, il est conseillé de toujours commencer
par faire un graphe des actions mécaniques. Ce graphe des actions
mécaniques est le graphe des liaisons sur lequel on rajoute le nombre de
degrés de liaisons et les actions extérieures.

Par exemple, le graphe des liaisons de la potence à tirant (exercice
corrigé) devient :

\begin{center}
  \hfill
  \begin{minipage}{.4\textwidth}
    \includegraphics[width=\textwidth]{img/GrapheLiaisons}
  \end{minipage}\hfill
  {\huge$\Rightarrow$}\hfill
  \begin{minipage}{.4\textwidth}
    \includegraphics[width=\textwidth]{img/GrapheAM}
  \end{minipage}
  \hfill\hfill
\end{center}

\subsubsection{Isolement}

Après avoir choisi l'ensemble \(E\) à isoler (une ou plusieures pièces),
faire le bilan des actions mécaniques extérieures consiste à faire
l'inventaire des torseurs d'action mécanique agissant sur \(E\).

\fbox{
  \(\Rightarrow\) Voir le TD potence à tirant pour un détail de la méthode.
}

\subsubsection{Théorèmes déduits du PFS}

Il n'est pas toujours nécessaire, pour résoudre un problème de statique, de vérifier l'ensemble du PFS. On peut déduire 2 équations vectorielles de l'expression vue dans l'énoncé du PFS.

\begin{itemize}\tightlist
  \item Théorème de la résultante statique : $\displaystyle \sum_{S\not\in E} \vec F (S\to E)=\vec 0$
  \item Théorème du moment statique : $\displaystyle \sum_{S\not\in E} \Mam{A}(S\to E)=\vec 0$
\end{itemize}

\subsection{Mouvements particuliers}

Il existent des mouvement particuliers pour lesquels le PFS s'applique bien que le système isolé soit en mouvement :
\begin{itemize}\tightlist
  \item Si le mouvement est une translation rectiligne à vitesse constante.
  \item Si le mouvement est une rotation autour d'un axe fixe et que le système est équilibré.
\end{itemize}


\input{liaisons}
\label{tableauLiaisons}

\end{document}

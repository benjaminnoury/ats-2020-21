\documentclass{recours}


\usepackage{longtable}





\newenvironment{Question}
{\question{}
{}}



\pagestyle{fancy}
\fancyhead{}
\fancyhead[L]{Cours\ CI 8 Transmettre l'énergie --- Efforts}
\fancyhead[R]{\textbf{ATS} \quad\thepage/\pageref{LastPage}}
\fancyfoot{} % clear all footer fields
\renewcommand{\headrulewidth}{0.4pt}


\providecommand{\tightlist}{%
\setlength{\itemsep}{0pt}\setlength{\parskip}{0pt}}



\begin{document}
%\begin{flushleft}
\flushleft\textbf{Compétence B2 : Proposer un modèle de connaissance et de comportement}

\mktitre{Modéliser les actions mécaniques}

%\end{flushleft}

\parindent=\oldparindent

\begin{tabularx}{\textwidth}{lX}
    \bb{B Modéliser} & Écrire la relation entre le \bb{modèle local} et le \bb{modèle global} dans le cas d'actions réparties \\ 
    & Modéliser une action mécanique avec ou sans \bb{frottement} (lois de Coulomb) \\
    & Associer un \bb{torseur d'action mécanique transmissible} à une liaison \\
\end{tabularx}

\section*{Objectif}

Tout mécanisme est dimensionné pour pouvoir être utilisé pendant un
temps donné.

Or, la durée de vie d'une pièce dépend généralement :

\begin{itemize}
\tightlist
\item
  de l'environnement dans lequel elle se trouve,
\item
  de ses dimensions,
\item
  du matériau utilisé,
\item
  \ldots{}
\item
  mais surtout des actions appliquées sur la pièce.
\end{itemize}

Ces actions peuvent être mesurées mais cela demande la construction d'un
prototype et/ou la mise en place d'un laboratoire de mesure. Ces
réalisations sont beaucoup trop coûteuses.

On va donc chercher à
\bb{PRÉVOIR les actions appliquées sur un mécanisme} en utilisant des
modèles mathématiques et des lois physiques.

\hypertarget{actions-muxe9caniques-guxe9nuxe9ralituxe9s}{%
\section{Actions mécaniques :
Généralités}\label{actions-muxe9caniques-guxe9nuxe9ralituxe9s}}

\bb{Définition : }On appellera action mécanique, tout phénomène ( cause
) susceptible de :

\begin{itemize}
\tightlist
\item
  provoquer ou modifier le mouvement
\item
  maintenir un corps au repos
\item
  produire des déformations
\end{itemize}

~

\bb{Problématique : }Comment modéliser l'action d'une pièce sur une
autre.
\textit{(modéliser = remplacer un problème réel par un problème de mathématiques)}

~

\bb{Exemple :} Comment modéliser l'action de la pièce sur un bras de la
pince au point L.

\begin{multicols}{2}
    \image[.8]{img/robot.png}
    
    \image{img/pince.png}
\end{multicols}

\hypertarget{moduxe8le-global-moduxe8le-local}{%
\subsection{Modèle global / Modèle
local}\label{moduxe8le-global-moduxe8le-local}}

Suivant l'étude à mener, nous avons à notre disposition 2 modélisations
possibles du même phénomène.

\begin{multicols}{2}
\bb{Le modèle local}, qui conduit à étudier l'action en tout point de la zone où elle s'exerce (pression de contact, contraintes, champ de pesanteur. C'est la modélisation \bb{vue de près}. 

\image[.9]{img/pinceLocal.png}
\end{multicols}

\begin{multicols}{2}
\bb{Le modèle global}, qui proposera une représentation de l'action par un torseur qui fera disparaître la répartition des efforts. C'est la modélisation \bb{vue de loin}

\image[.9]{img/pinceGlobal.png}
\end{multicols}

\newpage

\hypertarget{ruxe9sultante-moment-et-couple}{%
\subsection{Résultante, moment et
couple}\label{ruxe9sultante-moment-et-couple}}

\hypertarget{notion-de-ruxe9sultante}{%
\subsubsection{Notion de résultante}\label{notion-de-ruxe9sultante}}

Les 2 modèles sont reliés entre eux par intégration :
\(\displaystyle \vec R(\text{pièce} \rightarrow \text{doigt}) = \int_{Q \in S} \ovr{d F_{\text{pièce} \rightarrow \text{doigt}}(Q)}\)
\image[.9]{img/pinceGlobalLocal3D.png}

L'action mécanique est modélisée en un point particulier A par un
vecteur \(\vec R(\text{pièce} \rightarrow \text{doigt})\) appelé
résultante de la pièce sur le doigt.

~

L'unité de la résultante
\(\vec R(\text{pièce} \rightarrow \text{doigt})\) est le
\bb{NEWTON (N)}.

\hypertarget{notion-de-moment}{%
\subsubsection{Notion de moment}\label{notion-de-moment}}

La modélisation de l'action mécanique par une \bb{résultante} en un
point particulier est :

\begin{itemize}
\item \bb{suffisante} pour un point appartenant au support de l’action, puisqu’elle prend en compte l’action de tirer ou pousser.
\item \bb{insuffisante} pour un point n’appartenant pas au support de l’action, puisqu’elle ne prend pas en compte l’action de tordre, tourner, visser ou dévisser.
\end{itemize}

~

Si on s'intéresse à l'effet de l'action mécanique précédente au point O,
celle-ci a tendance à :

\begin{itemize}
\item pousser le doigt dans une direction verticale parallèle à $\vec R(\text{pièce} \rightarrow \text{doigt})$
\item faire tourner le doigt autour de l’axe $(O,\vec z)$
\end{itemize}

\Begin{multicols}{2}
\image{img/pinceMoment.png}

Nous représenterons donc cette action mécanique :

\begin{itemize}
\tightlist
\item
  par une \bb{résultante}
  \(\vec R(\text{pièce} \rightarrow \text{doigt})\)
  \bb{qui a tendance à pousser} dans une direction. \newline (résultante
  inchangée par rapport à celle modélisée en A)
\end{itemize}

~

\begin{itemize}
\tightlist
\item
  et par un deuxième vecteur appelé \bb{moment} et noté
  \(\vec M_{O}(\text{pièce} \rightarrow \text{doigt})\)
  \bb{qui a tendance à faire tourner} autour d'un axe.
\end{itemize}

~

L'unité du moment est le \bb{NEWTON × MÊTRE (N.m)} \End{multicols}

\Begin{asavoir}{Torseur des actions mécaniques}

On regroupe la résultante et le moment de l'action mécanique dans un
torseur nommé le torseur des actions mécaniques \[
\tam{1 \to 2}=
\torseur{A}{\vec R (1 \rightarrow 2)\\\vec M_{A}(1\rightarrow 2)}
= \torseur{A}{X \vec x_{} + Y \vec y_{} + Z \vec z_{} \\ L \vec x_{} + M \vec y_{} + N \vec z_{}}
=\torseur[R]{A}{X & L \\ Y & M\\ Z &N}
\]

La résultante \(\vec R (1 \rightarrow 2)\) ne dépend pas du point.

~

Le moment est défini en un point B à partir de son expression en un
point A par la relation de Varignon :
\[ \ovr{M_{B}}(1\rightarrow 2) =  \ovr{M_{A}}(1\rightarrow 2) + \ovr{BA} \wedge \vec R (1 \rightarrow 2) \]

~

\End{asavoir}

\hypertarget{couple}{%
\subsubsection{Couple}\label{couple}}

\begin{coteAcote}[.2]
Un couple est l'action mécanique générée par 2 résultantes (ou un nombre pair, ou une infinité) égales et opposées mais non-colinéaires.

Le moment d'une telle action n'est pas nul mais les résultantes s'annulent deux à deux.

Un couple est représenté par .... un torseur-couple:
$$
\torseur{*}{\vec 0\\ \vec C}=\torseur[R]{*}{ 0 & L \\ 0 & M\\ 0 &N}
$$

\tcblower

\image{img/couple.png}
    
\end{coteAcote}

\section{Quelques actions mécaniques à connaître}

\hypertarget{action-de-la-pesanteur}{%
\subsubsection{Action de la pesanteur}\label{action-de-la-pesanteur}}

\begin{coteAcote}[.3]
On modélisera l'action de la pesanteur sur un solide S par un glisseur au centre de gravité G. (Démonstration plus tard dans le cours)

\begin{itemize}
\item $m$ est la masse du solide $S$
\item $g$ est la densité massique de force de la pesanteur (g=9,81 N/kg)
\item $ \vec z_{0}$ est le vecteur indiquant la verticale.
\end{itemize}

\tcblower

$$
\tam{Pesanteur \to S }= \torseur{G}{- m g \vec z_{0}\\ \vec 0}
$$
\end{coteAcote}

\hypertarget{action-dun-moteur}{%
\subsubsection{Action d'un moteur}\label{action-dun-moteur}}

\begin{coteAcote}[.3]
On modélisera l'action d'un moteur par un torseur couple.


\Begin{itemize}
    \item $C_{m}$ est le couple du moteur (en N.m)
    \item $\vec x$ l'axe de rotation du moteur
\End{itemize}

\tcblower

$$
\tam{ Moteur \to S }= \torseur{*}{ \vec 0 \\ C_{m} \vec x}
$$
    
\end{coteAcote}

\hypertarget{action-transmissible-par-un-ressort}{%
\subsubsection{Action transmissible par un
ressort}\label{action-transmissible-par-un-ressort}}

\begin{coteAcote}[.3]
Pour un ressort R de compression, l'effort axial nécessaire pour obtenir une déformation $\Delta l=l-l_{0}$ est donné par la relation :
$\boxed{
F (R \to S) = - k \cdot (l - l_{0}) 
}$

\Begin{itemize}
    \item $k$ est la raideur du ressort en \bb{N/m};
    \item $l$ est la longueur du ressort en charge ;
    \item $l_{0}$ est la longueur du ressort à vide.
\End{itemize}

\tcblower

Pour un ressort d'axe $(A,\vec x)$
$$
\tam{Ressort \to S} = \torseur{A}{ -k \cdot (l-l_{0}) \cdot \vec x \\ \vec 0}
$$
\end{coteAcote}

\begin{coteAcote}[.3]
Pour un ressort de torsion, le couple nécessaire pour obtenir une torsion $\Delta \theta = \theta - \theta_{0}$ est donné par la relation :
$C(R \to S)= K \cdot (\theta - \theta_{0})$

\end{coteAcote}

\hypertarget{action-muxe9canique-transmissible-par-une-liaison-parfaite}{%
\subsection{Action Mécanique transmissible par une liaison
parfaite}\label{action-muxe9canique-transmissible-par-une-liaison-parfaite}}

Il y a une forte corrélation entre le torseur Cinématique d'une liaison
et son torseur des actions transmissibles.

Lorsqu'un degré de liberté est supprimé entre 2 solides 1 et 2, il en
résulte alors une composante dans le torseur de l'action mécanique
transmissible de 1→2 (qui empêche le mouvement).

Exemple : dans le cas d'une liaison pivot parfaite d'axe
\((A,\vec x_1 )\)

\begin{center}
\begin{tikzpicture}

\tikzstyle{boite}=[text width=6.5cm, text badly centered, rectangle, draw]
\node[boite] at (0,0) {Torseur cinématique\\

\medskip

$$ \tv{2/1} = \torseur[R_{1}]{A}{\omega_{x}&0\\0&0\\0&0}$$};
\node[boite] at (7,0) {Torseur d'action mécanique\\

\medskip

$$ \tam{2 \to 1} = \torseur[R_{1}]{A}{X_{12}&0\\Y_{12}&M_{12}\\Z_{12}&N_{12}}$$};

\draw[<->] (1.25,-1.3)|-++(0,-1)--++(7.3-1.25,0)node[midway, above]{"Translation"}--++(0,1);
\draw[<->] (.6,-1.3)|-++(0,-1.7)--++(8.1-.6,0)node[midway, above]{"Rotation"}--++(0,1.7);

\node (A) at (-.5,0.5) {$\vec \Omega({2/1})$};
\draw[->] (A)-|(0.6,.1);
\node (B) at (7-.5,0.5) {$\vec R_{1 \to  2}$};
\draw[->] (B)-|(7.3,.1);

\node (C) at (2.1,0.5) {$\vec V_A({2/1})$};
\draw[->] (C)-|(.75+0.5,.1);

\node (D) at (9.5,0.5) {$\vec M_A({1 \to 2 })$};
\draw[->] (D)-|(8.1,.1);


\end{tikzpicture}
\end{center}

\subsubsection{Degrés de liaison}

On appelait \bb{degrés de liberté} les mouvements possibles dans une
liaison.

Un \bb{degré de liaison} est un mouvement impossible dans une liaison et
donc une action transmissible.

\input{liaisons}

\section{Principe fondamental de la statique}

\begin{asavoir}{Énoncé du principe fondamental de la statique (PFS)}
Soit un système matériel $E$ au repos ou évoluant à vitesse constante dans un référentiel galiléen. Alors, la somme des actions mécaniques s'exerçant sur E est nulle.

$$
\tam{\bar E \to E} = \{ O \}
$$

\end{asavoir}


\end{document}

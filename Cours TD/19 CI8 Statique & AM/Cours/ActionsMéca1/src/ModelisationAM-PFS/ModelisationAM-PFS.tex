\documentclass{recours}

\classe{TSI 1}
\chapitre{Actions mécanique et Statique}
\type{Cours}

\renewcommand{\question}[1]{\begin{flushleft} \addtocounter{ques}{1} \textbf{(Question  \thesection.\theques)} #1 \end{flushleft}}

\begin{document}
\mktitre{Modélisation des Actions Mécaniques}
\begin{obj}
	Tout mécanisme est dimensionné pour \underline{pouvoir être utilisé pendant un temps donné}.
	
	Or, la durée de vie d’une pièce dépend généralement :
	\begin{itemize}
		\item de l’environnement dans lequel elle se trouve,
		\item de ses dimensions,
		\item du matériau utilisé,
		\item ...
		\item mais surtout des actions appliquées sur la pièce.
	\end{itemize}
	
	Ces actions peuvent être mesurées mais cela demande la construction d’un prototype et/ou la mise en place d'un laboratoire de mesure. Ces réalisations sont beaucoup trop coûteuses. 
	
	On va donc chercher à \bb{PRÉVOIR les actions appliquées sur un mécanisme} en utilisant des modèles mathématiques et des lois physiques.
	
\end{obj}
	
	%\newpage
	
	\section{Actions mécaniques : Généralités}
	\begin{obj}[Définition]
	On appellera action mécanique, tout phénomène ( cause ) susceptible de :
	\begin{itemize}
		\item provoquer ou modifier le mouvement
		\item maintenir un corps au repos
		\item produire des déformations	
	\end{itemize}
	\end{obj}
	
	Comment modéliser l'action d'une pièce sur une autre. \textit{(modéliser = remplacer un problème réel par un problème de mathématiques)}\\
	
	\bb{Exemple :} Comment modéliser l'action de la pièce sur un bras de la pince au point L.
	
	\begin{multicols}{2}
		\image[.8]{img/robot.png}
		
		\image{img/pince.png}
	\end{multicols}
	\subsection{Modèle global / Modèle local}
	
	
	
		Suivant l'étude à mener, nous avons à notre disposition 2 modélisations possibles du même phénomène.
		\begin{multicols}{2}
			\bb{Le modèle local}, qui conduit à étudier l'action en tout point de la zone où elle s'exerce (pression de contact, contraintes, champ de pesanteur. C'est la modélisation \bb{vue de près}. 
			
			\image[.9]{img/pinceLocal.png}
		\end{multicols}
		\begin{multicols}{2}
			\bb{Le modèle global}, qui proposera une représentation de l'action par un torseur qui fera disparaître la répartition des efforts. C'est la modélisation \bb{vue de loin}
			
			\image[.9]{img/pinceGlobal.png}
		\end{multicols}
			
		
		
		
		
		\subsection{Résultante, moment et couple}
		\subsubsection{Notion de résultante}
		
		Les 2 modèles sont reliés entre eux par intégration :  $\displaystyle \vec R(\text{pièce} \rightarrow \text{doigt}) = \int_{Q \in S} \ovr{d  F_{\text{pièce} \rightarrow \text{doigt}}(Q)}$
		\image[.9]{img/pinceGlobalLocal3D.png}
		
		L'action mécanique est modélisée en un point particulier A par un vecteur $\vec R(\text{pièce} \rightarrow \text{doigt})$ appelé résultante de la pièce sur le doigt.
		
		\medskip
		\medskip
		\medskip
		
		L'unité de la résultante $\vec R(\text{pièce} \rightarrow \text{doigt})$ est le \bb{NEWTON (N)}.

		\subsubsection{Notion de moment}
		La modélisation de l’action mécanique par une \bb{résultante} en un point particulier est :
		\begin{itemize}
			\item \bb{suffisante} pour un point appartenant au support de l’action, puisqu’elle prend en compte l’action de tirer ou pousser.
			\item \bb{insuffisante} pour un point n’appartenant pas au support de l’action, puisqu’elle ne prend pas en compte l’action de tordre, tourner, visser ou dévisser.
			
			\end{itemize}
		
		\medskip
		\medskip
		\medskip
		
		Si on s’intéresse à l’effet de l’action mécanique précédente au point O, celle-ci a tendance à :
		\begin{itemize}
			\item pousser le doigt dans une direction verticale parallèle à $\vec R(\text{pièce} \rightarrow \text{doigt})$
			\item faire tourner le doigt autour de l’axe $(O,\vec z)$
		\end{itemize}
		
		\begin{multicols}{2}
			\image{img/pinceMoment.png}
			
			\begin{itemize}
				\item par une \bb{résultante} $\vec R(\text{pièce} \rightarrow \text{doigt})$ \bb{qui a tendance à pousser} dans une direction. (résultante inchangée par rapport à celle modélisée en A)
				\item et par un deuxième vecteur appelé \bb{moment} et noté $\vec M_{O}(\text{pièce} \rightarrow \text{doigt})$ \bb{qui a tendance à faire tourner} autour d’un axe.
			\end{itemize}
				
				\medskip
				
			L'unité du moment est le \bb{NEWTON × MÊTRE (N.m)}
		\end{multicols}
			

		
		
		\begin{asavoir}{Torseur des actions mécaniques}
			On regroupe la résultante et le moment de l'action mécanique dans un torseur nommé le torseur des actions mécaniques
			$$
				\left \{ 1 \rightarrow 2 \right \} = \tc{A}{\vec R (1 \rightarrow 2)\\\vec M_{A}(1\rightarrow 2)} = \tc{A}{X \vec x_{} + Y \vec y_{} + Z \vec z_{} \\ L \vec x_{} + M \vec y_{} + N \vec z_{}}=\tc[R]{A}{ X & L \\ Y & M\\ Z &N}
			$$
			
			La résultante $\vec R (1 \rightarrow 2)$ ne dépend pas du point.
			
			Le moment est défini en un point B à partir de son expression en un point A par la relation de Varignon : \\
			$$ \ovr{M_{B}}(1\rightarrow 2) =  \ovr{M_{A}}(1\rightarrow 2) + \ovr{BA} \wedge \vec R (1 \rightarrow 2) $$
		\end{asavoir}

		\subsubsection{Couple}		
		\begin{minipage}{.65\lw}\parindent=15pt
				Un couple est l'action mécanique générée par 2 résultantes (ou un nombre pair, ou une infinité) égales et opposées mais non-colinéaires.
			
			Le moment d'une telle action n'est pas nul mais les résultantes s'annulent deux à deux.
			
			Un couple est représenté par .... un torseur-couple:
			$$
				\tc{*}{\vec 0\\ \vec C}=\tc[R]{*}{ 0 & L \\ 0 & M\\ 0 &N}
			$$
		\end{minipage}\hfill
		\begin{minipage}{.28\lw}
				\image{img/couple.png}
		\end{minipage}

		\section{Quelques actions mécaniques à connaître}
			\begin{minipage}[t]{.46\lw}\parindent=15pt
				On modélisera l'action de la pesanteur sur un solide S par un glisseur au centre de gravité G. (Démonstration plus tard dans le cours)
				$$
					\left \{ Pesanteur \rightarrow S \right \}= \tc{G}{- m g \vec z_{0}\\ \vec 0}
				$$
				\begin{itemize}
					\item $m$ est la masse du solide $S$
					\item $g$ est la densité massique de force de la pesanteur (g=9,81 N/kg)
					\item $ \vec z_{0}$ est le vecteur indiquant la verticale.
				\end{itemize}
		
			\end{minipage}\hfill\vrule\hfill
			\begin{minipage}[t]{.46\lw}\parindent=15pt
				On modélisera l'action d'un moteur par un torseur couple.
				$$
					\left \{ Moteur \rightarrow S \right \}= \tc{*}{ \vec 0 \\ \vec C_{m}}
				$$
				
				$C_{m}$ est le couple du moteur
				
				et $\vec x_{}$ l'axe de rotation du moteur
			\end{minipage}
		
			

		\subsection{Action Mécanique transmissible par une liaison parfaite }
			
			Il y a une forte corrélation entre le torseur Cinématique d'une liaison et son torseur des actions transmissibles.
			
			Lorsqu’un degré de liberté est supprimé entre 2 solides 1 et 2, il en résulte alors une composante dans le torseur de l’action mécanique transmissible de 1→2 (qui empêche le mouvement).
			
			
			Exemple : dans le cas d’une liaison pivot parfaite d’axe (A, x ~ 1 )
			\begin{center}
				\begin{tikzpicture}

	 \tikzstyle{boite}=[text width=6.5cm, text badly centered, rectangle, draw]
		\node[boite] at (0,0) {Torseur cinématique\\
		
		\medskip
		
		$$ \left \{ 2/1 \right \} = \tc[R_{1}]{A}{\omega_{x}&0\\0&0\\0&0}$$};
		\node[boite] at (7,0) {Torseur d'action mécanique\\
		
		\medskip
		
		$$ \left \{ 1 \rightarrow 2\right \} = \tc[R_{1}]{A}{X_{12}&0\\Y_{12}&M_{12}\\Z_{12}&N_{12}}$$};
		
		\draw[<->] (1.12,-1.3)|-++(0,-1)--++(6.2,0)node[midway, above]{"Translation"}--++(0,1);
		\draw[<->] (.48,-1.3)|-++(0,-1.7)--++(7.9,0)node[midway, above]{"Rotation"}--++(0,1.7);
		
		\node (A) at (-.5,0.5) {$\vec \Omega({2/1})$};
		\draw[->] (A)-|(0.4,.1);
		\node (B) at (7-.5,0.5) {$\vec R_{1\!\rightarrow 2}$};
		\draw[->] (B)-|(7+0.4,.1);
		
		\node (C) at (1.5+.5,0.5) {$\vec V_A({2/1})$};
		\draw[->] (C)-|(.75+0.4,.1);
		
		\node (D) at (9.5,0.5) {$\vec M_A({1\!\rightarrow\!2 })$};
		\draw[->] (D)-|(7+1+0.4,.1);
		

\end{tikzpicture}
			\end{center}
			
			
			
			\subsubsection{Degrés de liaison}
			
			On appelait \bb{degrés de liberté} les mouvements possibles dans une liaison.
			
			Un \bb{degré de liaison} est un mouvement impossible dans une liaison et donc une action transmissible.
		
		
		%\newpage{}


		\section{Principe fondamental de la statique}
	

	\begin{asavoir}{Énoncé du principe fondamental de la statique (PFS)}
		Soit un système matériel $E$ au repos ou évoluant à vitesse constante dans un référentiel galiléen. Alors, la somme des actions mécaniques s'exerçant sur E est nulle.
		
		$$
			\sum \left \{ \bar E \rightarrow E \right \} = \{ O \}
		$$
		
	\end{asavoir}

\end{document}

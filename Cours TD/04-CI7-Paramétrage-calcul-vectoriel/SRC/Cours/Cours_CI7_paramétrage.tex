\documentclass{recours}

\usepackage{longtable}

\newenvironment{Question}
{\question{}
{}}

\pagestyle{fancy}
\fancyhead{}
\fancyhead[L]{Cours\ CI 7 Transmettre l'énergie}
\fancyhead[R]{\textbf{ATS} \quad\thepage/\pageref{LastPage}}
\fancyfoot{} % clear all footer fields
\renewcommand{\headrulewidth}{0.4pt}


\providecommand{\tightlist}{%
\setlength{\itemsep}{0pt}\setlength{\parskip}{0pt}}



\begin{document}
\flushleft\textbf{Compétence B2 : proposer un modèle de connaissance et de comportement}

\mktitre{Paramétrage d'un mécanisme}


En mécanique, on étudiera le mouvement de solides les uns par rapport
aux autres. Dans l'exemple ci-dessous, on parlerait du \textbf{mouvement
relatif du bras 1 par rapport au bras 2}. \fbox{On note ce mouvement
\textbf{(1/2)}}.

Pour pouvoir quantifier (c'est-à-dire mesurer) ce mouvement, il est
nécessaire de choisir des \textbf{dimensions et des directions de
référence}. C'est le but du paramétrage.


\section{Définitions : Solide indéformable, repère,
référentiel.}

\subsubsection{Solide indéformable}

Un solide indéformable est un système matériel S tel que pour tout
ensemble de 2 point A et B du solide S, la distance AB reste constante
au cours du temps:
\[S \text{ indéformable} \Leftrightarrow \forall (A,B) \in S, \norm{\ovr{AB}}=\text{constante}\]

\subsubsection{Repère d'espace et de temps}

\begin{center}
\begin{minipage}{.50\linewidth}

Un repère de l'espace est composé :

\begin{itemize}
\item
  d'un ensemble de vecteurs (3 dans l'espace et 2 dans le plan)
  permettant de désigner n'importe quel autre vecteur de manière unique
\item
  d'une origine.
\end{itemize}
\medskip
En mécanique, les repère seront tous \textbf{orthonormés} : 
\begin{itemize}
	\item les vecteurs de la base sont orthogonaux \\ (\(\vec x \perp \vec y \perp \vec z\)) 
	\item et de longueur unitaire (\(\|\vec x\|=\|\vec y\|=\|\vec z\|=1\))
\end{itemize}



\end{minipage}\hfill
\begin{minipage}{.45\linewidth}
	{\centering
	\includegraphics[width=.7\textwidth]{img/repere}}

Le repère \(R_1\ (O, \vec x_{1}, \vec y_{1}, \vec z_{1} )\) est associé
au solide \(S_1\)

\end{minipage}
\end{center}

Les mouvements seront étudiés par rapport à un repère d'espace (le plus
souvent orthonormé direct) attaché au solide de référence (\(R_1\) pour
le solide \(S_1\)).

L'étude complète des mouvements nécessitera une horloge (base de temps
en secondes) et une origine des temps.

\medskip

\textbf{L'association d'un repère d'espace et d'un repère de temps
définit un référentiel pour l'étude.}
\newpage
\section{Repérage d'un repère \(R_2\) en mouvement par
rapport à un repère
\(R_1\)}

\subsection{Notion de paramètre}

\begin{center}
\begin{minipage}{.45\lw}\parindent=\oldparindent

On appelle paramètres toutes les variables géométriques (distances ou angles) qui permettent d’obtenir à tout instant la position d'un point $P$ d'un solide $S_2$ par rapport à un repère $R_1$

Le repère $R_2$ lié au solide $S_2$ est en mouvement par rapport au repère $R_1$ lié au solide $S_1$    

\ 

Ce mouvement peut se décomposer en :
\end{minipage}
\hfill
\begin{minipage}{.45\lw}
\image[1]{img/R1R2} 
\end{minipage}
\end{center}

\begin{description}
\tightlist
\item[Une translation]
Elle sera paramétrée par le vecteur \(\ovr{O_1 O_2}\) exprimée en
\emph{coordonées cartésiennes} soit \textbf{3} coordonnées (paramètres)
dans l'espace ou \textbf{2} dans le plan
\end{description}

\[\ovr{O_1 O_2} = \vc{x\\y\\z}_{B_1} = x \, \vec x_{1} + y \, \vec y_{1} + z \, \vec z_{1} \]

\begin{description}
\tightlist
\item[Une rotation]
En pratique, on peut décomposer toute rotation de l'espace en 3 rotation
élémentaires ne faisant intervenir qu'un seul angle chacune. On se
trouve dans le cas de \emph{bases consécutives}
\end{description}

\begin{center}
\begin{minipage}{.4\linewidth}
\image[.7]{img/figRot}
\end{minipage}\hfill
\begin{minipage}{.55\linewidth}

\textbf{Un seul} angle est nécessaire

Cet angle est orienté de \(\vec x_1\) vers \(\vec x_2\) (et de
\(\vec y_1\) vers \(\vec y_2\))

On note \(\alpha=(\vec x_1,\vec x_2)=(\vec y_1,\vec y_2)\)
\end{minipage} \end{center}

\begin{description}
\item[Conclusion]
Par rapport à un solide de référence, le repérage d'un solide par
rapport à un autre nécessite \textbf{3 paramètres de positions et 3
paramètres angulaires.}

Ces six possibilités élémentaires de déplacement dans l'espace
correspondent aux \textbf{six degrés de liberté possibles} du solide
\(S_{2}\) par rapport à \(S_{1}\) .

En pratique, certaines possibilités de mouvement sont généralement
restreintes par des contacts entre des surfaces géométriques appartenant
à chacun des solides (voir cours sur les liaisons (CI9).
\end{description}

\medskip

\hfill\begin{minipage}{.3\lw}

Exemple : \image[.7]{img/pivot}

\end{minipage}
\begin{minipage}{.6\lw}

Le solide \(S_2\) ne peut que changer d'orientation par rapport à
\(S_1\) puisque seule une rotation autour de l'axe commun est
possible.\\
\end{minipage}

\newpage
\subsection{Exemple de paramétrage}

Le robot 2 axes ci-dessous est modélisé par le schéma cinématique
ci-dessous :

\begin{center}
\begin{minipage}{.1\lw}
    \image{img/robot}
\end{minipage}
\begin{minipage}{.45\lw}
    \image{img/robotSchema3}
\end{minipage}\hfill
\begin{minipage}{.4\lw}\parindent=\oldparindent
    
Les angles $\alpha$ et $\beta$ permettent de définir entièrement et sans ambiguité l'orientation des bras.

\ 
    
Pour connaitre la position du point P, il est nécessaire de connaître en plus les longueurs $l_{1}$ et $z$.
    
\ 

Le point $P$ est donc paramétré par $\alpha$, $\beta$, $l_{1}$ et $z$ et on a : 
    
$$\ovr{O_{1}P} = \ovr{O_{1}O_{2}} + \ovr{O_{2}P}= l_{1} \vec z_{1} + z \vec z_{2}$$
    

\emph{Est-ce que $\gamma$ est un paramètre ?}
\end{minipage}  
\end{center}

\subsubsection{Remarque}

Les notations en mécanique étant lourdes, on aura tendance par la suite
à assimiler les solides et leur repère associé pour simplifier
l'écriture.


\end{document}

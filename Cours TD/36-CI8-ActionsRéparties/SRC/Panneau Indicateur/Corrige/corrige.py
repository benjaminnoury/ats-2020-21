#!/usr/bin/python3

import math as m

N=-7875
Ty=0
Tz=6000

Mt=30E6
x=7.5E3
Mfy=54E6+x*6000
Mfz=-38.375E6

d_2=200
d_1=160
print('================ Question 3 ================')
S=m.pi*(d_2**2-d_1**2)
print('Section : {0:.3g} mm2'.format(S))

Ig=m.pi*(d_2**4-d_1**4)/64
print('Igz=Igy={0:1.3g} mm4'.format(Ig))

I0=m.pi*(d_2**4-d_1**4)/32
print('I0={0:.3g} mm4'.format(I0))

r=100

def sigma(y,z):
	return abs(N/S - Mfz/Ig*y + Mfy/Ig*z)
	
sigma_max=max(sigma(r,r),sigma(-r,r),sigma(r,-r),sigma(-r,-r),)
#print(sigma_max)
print('Contrainte normale maxi {0:2.2f} MPa'.format(sigma_max))

tau=Tz/S + Mt*r/I0

print('Contrainte tangentielle maxi {0:.2f} MPa'.format(tau))


sigma_VM=m.sqrt(sigma_max**2+3*tau**2)
print('Contrainte equivalente de Von Mises : {0:.2f} MPa < Re'.format(sigma_VM))
Re=350
secu=Re/sigma_VM
print('coeff de securite : {0:.2f}'.format(secu))

print('================ Question 4 ================')
print('1 - Compression ')
E=210E3
DL = N/(E*S)*x
print('Delta_L = {0:.2g} mm '.format(DL))

print('2 - Flexion y')
y=Mfz/(E*Ig)*x**2/2

print('fleche y : {0:.2f} mm'.format(y))

print('3 - Flexion z')
z=(54E6*x**2/2+6000*x**3/6)/(E*Ig)

print('fleche z : {0:.2f} mm'.format(z))

print('Conclusion')


OA=[(x+DL),0+y,0+z]

print('OA initial : {} (en m)'.format([i/1000 for i in [x,0,0]]))
print('OA apres deformation : [{0:1.6g},{1:.3g},{2:.3g}] (en m)'.format((x+DL)/1000,0+y/1000,0+z/1000))

print('================ Question 5 ================')
# Torsion
AB=3E3
G=81E3
theta=Mt/(G*I0)*x # Angle de torsion en A
print('theta={}'.format(theta))
D_torsion=[1,m.cos(theta),m.sin(theta)] # taux de deformation


# flexion y
dy=Mfz/(E*Ig)*x
print("f'y={}".format(dy))
D_fy=[m.sin(dy),m.cos(dy),1] # taux de deformation

dz=(54E6*x+6000*x**2/2)/(E*Ig)
print("f'z={}".format(dz))
D_fz=[-m.sin(dz),1,m.cos(dy)] # taux de deformation

# th de superposition 
OA=[x+DL,y,z]

OB_f=[]
for i in range(3):
	temp=OA[i]+(D_torsion[i]*D_fy[i]*D_fz[i])*AB
	OB_f.append(temp)
	
print('OB apres deformation : [{0:.3g},{1:.3g},{2:.3g}] (en m)'.format(OB_f[0]/1000,OB_f[1]/1000,OB_f[2]/1000))
\documentclass[]{article}
\usepackage{branly}
\usepackage{notations}
\usepackage{tableaux}
%\\usepackage{boites}
%\usepackage[europeanresistors,americaninductors,straightvoltages]{circuitikz}
\usepackage{tikz}\usetikzlibrary{patterns}


\begin{document}
\title{Cinématique des solides}
\type{\textbf{Cours CI7} Transmettre l'énergie -- Mouvements}
\classe{ATS}

\textbf{Compétence C2 Procéder à la mise en œuvre d'une démarche de résolution analytique}
\maketitle



\begin{rem}
	{Savoirs-Faire abordés}
	\begin{tabularx}{\textwidth}{>{\bfseries}l@{\ \ \labelitemi \ }X}
		B Modéliser & Paramétrer les mouvements d’un solide indéformable \\
			    & Qualifier les grandeurs d’entrée et de sortie d’un système isolé\medskip\\
		C Résoudre & Proposer une démarche permettant de déterminer une loi de mouvement \\
			   & Déterminer la loi entrée-sortie d’une chaîne cinématique simple\\
			   & Déterminer la trajectoire d’un point d’un solide par rapport à un
			   autre solide\\
			   & Déterminer le vecteur-vitesse d’un point d’un solide par rapport à
			   un autre solide \\
			   & Déterminer le vecteur-accélération d’un point d’un solide par
			   rapport à un autre solide \\
	\end{tabularx}
\end{rem}

\tableofcontents{}

\newpage
\section{Types de problèmes}

La cinématique d'un système est généralement définie par un schéma
cinématique.

Les problèmes de cinématique sont de 2 types :

\subsubsection*{Loi entrée-sortie en vitesse pour un mécanisme à structure
fermée.}

On souhaite déterminer le lien entre le mouvement en entrée et le
mouvement en sortie.

\begin{minipage}{.45\textwidth}
	\begin{description}
		\item [Exemple]

			Connaissant la vitesse de rotation du moteur, on
			souhaite déterminer la vitesse de déplacement du piston
			de compresseur. 

			Cela permet d'obtenir le débit du
			compresseur. 

	\end{description} 
\end{minipage}\hfill
\begin{minipage}{.45\textwidth}
	\includegraphics[width=.45\textwidth]{img/miniCompresseur.png} \hfill
	\includegraphics[width=.45\textwidth]{img/schemaCompresseur.png} 
\end{minipage}



\subsubsection*{Vitesse du point en bout de chaîne ouverte pour un mécanisme à structure ouverte.}

\begin{minipage}{.45\textwidth}
	\begin{description}
		\item [Exemple]

			Connaissant les vitesses de rotation des différentes
			articulations d'un robot, on cherche à déterminer la
			vitesse de déplacement du pied du robot en bout de
			chaîne ouverte (pied non posé sur le sol).

			Le problème inverse existe aussi :  déterminer les
			vitesses de rotations des différents moteurs pour avoir
			une vitesse de marche donnée.

	\end{description} 
\end{minipage}\hfill 
\begin{minipage}{.45\textwidth}
	\includegraphics[width=.3\textwidth]{img/Nao.jpg} \hfill 
	\includegraphics[width=.45\textwidth]{img/robot.png} 
\end{minipage}



\section{Hypothèses de la cinématique}

Les hypothèses utilisées lors des études cinématiques sont issues des
hypothèses mises en place lors la modélisation des liaisons (parfaites):

\begin{itemize}\tightlist
	\item	géométrie parfaite,
	\item	liaison parfaite (sans jeu et sans frottement),
	\item	solides indéformables.
\end{itemize}

\begin{asavoir}{Solide Indéformable}

	Un solide indéformable est un milieu continu tel que la distance entre 2
	points M et N quelconques du solide reste toujours constante :
	\[\displaystyle \text{S indéformable} \Leftrightarrow \forall (M,N) \in S , \|\overrightarrow{MN}\|=\text{constante}\]
\end{asavoir}

\begin{description}

	\item[Liaison "Souple"]

		Dans le cas où une pièce se déforme de façon importante, on modélisera les
		mouvements autorisés entre ses éléments par une liaison parfaite. Le reste de
		la structure sera alors solide.

		\textbf{Exemple :} L'articulation d'un classeur modélisable par une liaison
		pivot

		\medskip

	\item[Ressorts] Les ressorts dont l'ensemble de la structure est déformable n'interviennent pas
		dans les lois de la cinématique notamment lorsqu'un guidage est prévu en
		parallèle (c'est généralement le cas).

		En effet, l'étude cinématique s'intéresse aux seuls déplacements sans modéliser
		les actions mécaniques qui les provoquent. Le lien entre les actions mécaniques
		et les mouvements sera étudié dans le cours de dynamique.
\end{description}

\section{Trajectoires}

\begin{asavoir}{Trajectoire d'un point}
	\textbf{Définition :} {La trajectoire est l'ensemble des positions
	occupées par un point au cours d'un mouvement}

	La trajectoire dépend de l'ensemble cinématique auquel le point est
	attaché et de l'ensemble cinématique d'observation. 
\end{asavoir}

\subsubsection*{Exemples}


\begin{minipage}{.55\textwidth}
	\textbf{Le mouvement (1/0) est une rotation d'axe $(A,\vec z)$}

	\begin{tikzpicture}
		\coordinate (O) at (0,-1) ;
		\coordinate (A) at (0,0) ;
		\coordinate (B) at (30:2) ;
		\coordinate (C) at (5,0) ;
		\coordinate (D) at (8,0) ;
		\coordinate (E) at (6.5,-1) ;
		\coordinate (F) at (6.5,0) ;

		\draw (O) node[above left]{0};
		\draw[thick] (O)--++(-.5,0)--++(1,0);
		\fill[pattern=north west lines] (O)--++(-.5,0) rectangle ++(1,-.2);

		\draw (E) node[above left]{0};
		\draw (E)--++(-.5,0)--++(1,0);
		\fill[pattern=north west lines] (E)--++(-.5,0) rectangle ++(1,-.2);
		
	

		\draw (O)--(A)--(B) node[midway,above ]{1}--(C)node[midway,above]{2}--(F)--(E);
		\draw[fill=white] (F)-|++(-.4,.2) rectangle ++(.8,-.4);
		\draw (C)--(D)node[above left]{3};

		\draw[fill=white] (B) circle (.1) node[above]{B};
		\draw[fill=white] (C) circle (.1) node[above right]{C};
		\draw[<->] (0,.6)node[left]{$\vec y$}--(0,0)--(.6,0)node[below]{$\vec x$};
		\draw[fill=white] (A) circle (.1) node[left]{A};
		%\draw[dashed] (A) circle (2);
		\draw[dashed] (B) arc (30:100:2);
		\draw[dashed] (B) arc (30:-30:2);
		\draw (100:2.23) node[left]{$T_{B}(1/0)$};
		\draw[dashed] (C) arc (0:30:5) node[midway, right]{$T_{C}(1/0)$};
		\draw[dashed] (C) arc (0:-30:5);
		\draw (100:2.23) node[left]{$T_{B}(1/0)$};
	\end{tikzpicture}
\end{minipage}\hfill
\begin{minipage}{.45\textwidth}
	\begin{itemize}
		\item La trajectoire de B \(T_{B}(1/0)\) est un arc de cercle de centre A et
			de rayon AB\medskip

		\item La trajectoire de C \(T_{C}(1/0)\) est un arc de cercle de centre A et
			de rayon AC\medskip

		\item La trajectoire de A \(T_{A}(1/0)\) est un arc de cercle de centre A et
			de rayon AA( c'est-à-dire le point A)
	\end{itemize}
\end{minipage}

\medskip{}

\begin{minipage}{.55\textwidth}
	\textbf{Le mouvement (3/0) est une translation de direction  $\vec x$}

	\begin{tikzpicture}
		\coordinate (O) at (0,-1) ;
		\coordinate (A) at (0,0) ;
		\coordinate (B) at (30:2) ;
		\coordinate (C) at (5,0) ;
		\coordinate (D) at (8,0) ;
		\coordinate (E) at (6.5,-1) ;
		\coordinate (F) at (6.5,0) ;

		\draw (O) node[above left]{0};
		\draw (O)--++(-.5,0)--++(1,0);
		\fill[pattern=north west lines] (O)--++(-.5,0) rectangle ++(1,-.2);

		\draw (E) node[above left]{0};
		\draw (E)--++(-.5,0)--++(1,0);
		\fill[pattern=north west lines] (E)--++(-.5,0) rectangle ++(1,-.2);


		\draw (O)--(A)--(B) node[midway,above ]{1}--(C)node[midway,above]{2}--(F)--(E);
		\draw[fill=white] (F)-|++(-.4,.2) rectangle ++(.8,-.4);
		\draw (C)--(D)node[above left]{3};

		\draw[fill=white] (B) circle (.1) node[above]{B};
		\draw[fill=white] (C) circle (.1) node[above right]{C};
		\draw[<->] (0,.6)node[left]{$\vec y$}--(0,0)--(.6,0)node[below]{$\vec x$};
		\draw[fill=white] (A) circle (.1) node[below left]{A};

		\draw[dashed] (-1.2,0)node[above ]{$T_{A}(3/0)$}--(2,0);
		\draw[dashed] (B)--++(-1.2,0)node[above]{$T_{B}(3/0)$}--(B)--++(2,0);
		\draw[dashed] (5-1.2,0)node[below]{$T_{C}(3/0)$}--(5+.1,0);

	\end{tikzpicture}
\end{minipage}\hfill
\begin{minipage}{.45\textwidth}
	\begin{itemize}
		\item La trajectoire de B \(T_{B}(3/0)\) est une droite parallèle à \(\vec x\)
			passant par B\medskip

		\item La trajectoire de C \(T_{C}(3/0)\) est une droite parallèle à \(\vec x\)
			passant par C\medskip

		\item La trajectoire de A \(T_{A}(3/0)\) est une droite parallèle à \(\vec x\)
	passant par A ( c'est la même que \(T_{C}(3/0)\)) \end{itemize}
\end{minipage} 

\section{Vitesse de rotation}

\subsection{Définition}

\begin{asavoir}{Vitesse de rotation autour d'un axe $(A,\vec u)$}

	La vitesse de rotation \(\vec \Omega(2/1)\) d'un solide 2 par rapport à
	un référentiel (ou solide) 1 autour de l'axe \((A, \vec u)\) a pour
	caractéristique :

	\[
		\vec \Omega(2/1) = \frac{\text{d}\theta}{\text{dt}} \cdot \vec u =\dot \theta \cdot \vec u
	\]

\end{asavoir}
\begin{itemize}
	\item	\(\Omega(2/1)\) : Vitesse de rotation algébrique (nombre) en radian par
		seconde ( rd/s )
	\item	\(\displaystyle \frac{\text{d}\theta}{\text{dt}}\) ou \(\dot \theta\) :
		dérivée temporelle du paramètre angulaire \(\theta\)\\
		Le signe dépend du sens de rotation
		% \begin{tikzpicture}
		% 	\node[] (ordre) at (0,0) {$\overrightarrow{\underleftarrow{ x y z x y}}$};
		% 	\draw (ordre.north) node[]{+};
		% 	\draw (ordre.south) node[]{-};
		% \end{tikzpicture}
		% \(\displaystyle {\overrightarrow{\underleftarrow{ x y z x y}}}^{+}_{-}\)
	\item	\(\vec u\) : Direction (vecteur) parallèle à l'axe de rotation.
\end{itemize}\medskip

\begin{figure}[htpb]
	\textbf{Exemple :} Liaison Pivot d'axe \((A, \vec y_{1})\) entre 1 et 2 avec
	\(\alpha = (\vec x_{1}, \vec x_{2})\)

	\begin{minipage}{.3\textwidth}
		\centering
		\includegraphics[width=.5\textwidth]{img/pivot3D}
	\end{minipage}
	\begin{minipage}{.3\textwidth}
		\centering
		\includegraphics[width=.5\textwidth]{img/figPivot}
	\end{minipage}
	\begin{minipage}{.3\textwidth}
		\[
			\vec \Omega(2/1) = \dot \alpha .  \vec y_{1}
		\]
	\end{minipage}
\end{figure}

\subsection{Composition des vitesses
angulaires}

\begin{asavoir}{Composition des vitesses angulaires}
	Lorsque plusieurs paramètres angulaires interviennent, l'expression de
	\(\vec \Omega(N/0)\) s'obtient souvent par la composition des vecteurs
	vitesses de rotation.
	\[
		\vec \Omega_{N/0} = \vec \Omega(N/(N-1)) + \vec \Omega((N-1)/(N-2)) + \cdots + \vec \Omega(3/2) + \vec \Omega(2/1) + \vec \Omega(1/0)
	\] 
\end{asavoir}

\subsubsection*{$\to$ Exemple : Robot 2 axes}

\begin{minipage}{.55\textwidth}
	 % \includegraphics[width=.67\textwidth]{img/Robot2axes.png}
		\rmfamily
		\centering
	\includegraphics[width=.3\textwidth]{img/FanucRobot-M-10.jpg} \hfill
		\def\svgwidth{.6\columnwidth}
		\import{./img/}{robot2axes.pdf_tex}
\end{minipage}
\begin{minipage}{.4\textwidth}
	\medskip
	\begin{itemize}
		\item	\(\vec \Omega(1/0) = \dot \alpha \cdot \vec z_{0}\)
		\item	\(\vec \Omega(2/1) = \dot \beta \cdot \vec z_{0}\)
			~
		\item	\(\begin{aligned}[t] \vec \Omega(2/0) & = \vec \Omega(2/1) + \vec \Omega(1/0) \\ &= \dot \beta \cdot \vec z_{0} + \dot \alpha \cdot  \vec z_{0}\\ &= \left ( \dot \alpha + \dot \beta \right ) \cdot  \vec z_{0}\\ &= \left ( \dot \alpha + \dot \beta \right ) \cdot  \vec z_{2} \end{aligned}\)
	\end{itemize} 
\end{minipage}



\section{Vitesse d'un point dans le mouvement d'un solide}

% On s'intéresse maintenant aux mouvement des solides.

Un solide est toujours soit en rotation soit en translation soit les 2 à
la fois.

\begin{itemize}	\tightlist
	\item	La vitesse de rotation du solide \textbf{ne dépend pas} du point du solide
		où on l'exprime.
	\item	La vitesse linéaire ( de translation ) \textbf{dépend} du point du solide
		où on l'exprime.
\end{itemize}

\subsubsection*{Vecteur Vitesse d'un point appartenant à un solide}

Soit un solide 2 attaché à un repère \(R_2\) , en mouvement par
rapport à un repère \(R_{0}\).
On note \(\overrightarrow V_{M}(2/0)\) la vitesse du point M appartenant au solide
2 dans son mouvement par rapport à \(R_{0}\)

\textbf{Remarques : }la vitesse exprime la variation de position au cours du temps
\begin{itemize}
	% \item	L'ensemble des vecteurs vitesse des points du solide 2 est appelé champ	des vecteurs vitesse du solide 2.
	\item	vitesse nulle si la position est fixe,
	\item	vitesse d'autant plus grande que la position varie de façon importante au cours du temps.
	\item \(\V{M}(0/2) = - \V{M}(2/0)\) 
\end{itemize}

\subsection{Détermination analytique de la vitesse d'un point d'un solide}

\subsubsection*{\fbox{Première méthode : Propriétés de la Cinématique du Solide}}

\begin{asavoir}{{Composition des vecteurs vitesse}}

	La composition des vitesses de rotation et la formule de distribution
	des vitesses conduisent à la formule de composition des vitesses .
	\[
		\overrightarrow{V_{A}}(N/0) = \overrightarrow{V_{A}}(N/(N-1)) + \cdots + \overrightarrow{V_{A}}(3/2) + \overrightarrow{V_{A}}(2/1) +\overrightarrow{V_{A}}(1/0)
	\] 
\end{asavoir}

\begin{asavoir}{Formule de distribution des vitesses de Varignon }
	\[
		\forall (A,B), \  \overrightarrow{V_{B}}(2/1) = \overrightarrow{V_{A}}(2/1) + \overrightarrow{BA} \wedge \overrightarrow \Omega(2/1)
	\]
	Il n'y a pas besoin de réfléchir à l'appartenance ou non du point au
	solide considéré.\medskip

	\(\rightarrow\) Moyen mnémotechnique pour retenir cette formule
	incontournable : B-A-B-A-R 
\end{asavoir}

\textbf{Remarques :} 
\begin{itemize}
	\item	La vitesse en un point de l'axe d'une liaison pivot est nulle entre les
		2 solides en liaison.
	\item	La vitesse de rotation entre 2 solides en liaison glissière est nulle.
\end{itemize}

\subsubsection*{$\to$ Exemple : Robot 2 axes}
\begin{minipage}{.48\textwidth}
		\def\svgwidth{.9\columnwidth}
		\import{./img/}{robot2axes.pdf_tex}
	% \includegraphics[width=\textwidth]{img/Robot2axes.png}

	% \centering
	Calculer \(\overrightarrow V_{B}(1/0)\), \(\overrightarrow V_{C}(2/1)\) et \(\overrightarrow V_{C}(2/0)\)
\end{minipage}\hfill
\begin{minipage}{.48\textwidth}
\end{minipage}

\subsubsection*{\fbox{Deuxième Méthode : Dérivation du vecteur position}} 

Il faut faire très attention quand on utilise cette méthode : 
% On va chercher à utiliser la dérivée du vecteur position tel que vu page \pageref{dérive} en faisant attention à la nuance ci-dessous.


\fbox{
	\begin{minipage}{\textwidth}
		\begin{itemize}\tightlist
			\item 	Si le point M est un point du Solide 2, alors : $\displaystyle \overrightarrow V_{M}(2/0) =\overrightarrow{V}(M/0) $ 
			\item Si le point M est un point d'un autre solide, alors $\displaystyle \overrightarrow V_{M}(2/0) =\overrightarrow{V}(M/0) $ + conditions supplémentaires  
		\end{itemize}
	\end{minipage}
}


\subsubsection*{$\to$ Exemple : Robot 2 axes}

\begin{minipage}{.48\textwidth}
		\def\svgwidth{.9\columnwidth}
		\import{./img/}{robot2axes.pdf_tex}
	% \includegraphics[width=\textwidth]{img/Robot2axes.png}
\end{minipage}\hfill
\begin{minipage}{.48\textwidth}
	\(\begin{array}{ll} C \in (2) \text{ donc} & \overrightarrow V_{C}(2/0) = \overrightarrow V(C/0) \\ C \not \in (1) \text{ donc} & \overrightarrow V_{C}(1/0) \not = \overrightarrow V(C/0) \\ & \overrightarrow V_{C}(1/0) = \overrightarrow V(C/0)_{\left [ (1+2) \text{ indéformable} \right ] } \end{array}\)

	Pour calculer \(\overrightarrow V_{C}(1/0)\) par la dérivation vectorielle, il faut
	donc considérer \(\overrightarrow \Omega(2/1) = \vec 0\)

	~

	\footnotesize \textbf{Remarque :} Le calcul de \(\overrightarrow V_{C}(1/0)\)
	servirait dans le dimensionnement du moteur assurant la rotation (1/0)
\end{minipage}

Une bonne habitude pour ne pas se tromper : si le point étudié n'appartient pas au solide, utilisez la méthode précédente.


% \subsection{Vitesse d'un point par rapport à un repère (Définition)}

% \begin{description}
% \end{description}

\begin{asavoir}{Dérivée du vecteur position}

	On appelle vitesse d'un point M par rapport à un repère R, le vecteur
	\(\vec V (M/R)\) défini comme étant la dérivée temporelle du vecteur
	position par rapport au repère R.
	\begin{center}
		\begin{tikzpicture}
			\node[] () at (0,0) {$\displaystyle
					\V{M}(1/\boxed{0})=\left [
					\frac{\text{d}\,\overrightarrow{AM}}{\text{d}t}	\right]
					_{\boxed{\mathcal{R}_0}}
				$};
			\draw[<->] (-.7,-.12)|-++(2,-.9)node[below]{Repère de dérivation}-|(1.5,-.62);
		\end{tikzpicture}
	\end{center}
	\begin{itemize}\tightlist
		\item Le point A est un point fixe du repère $\mathcal{R}_0$, par exemple, l'origine du repère ;
		\item les vecteurs du repère de dérivation sont supposés fixes (ce sont des constantes) ;
		\item il n'est pas obligé d'exprimer le résultat dans le repère de dérivation.
	\end{itemize}
	% \includegraphics[width=.95\textwidth]{img/base.png}
\end{asavoir}

\subsection{Dérivation d'un vecteur}

\textbf{Formule de Bour} La dérivée par rapport au temps d'un vecteur
\(\vec v\) dans un repère \(R_{0}\) s'écrit :
\[
	\left [ \frac{\text{d}\,\vec v}{\text{d}t}  \right]_{R_{0}} = \left [ \frac{\text{d}\,\vec v}{\text{d}t}  \right]_{R_{1}} + \overrightarrow \Omega(1/0) \wedge \vec v
\] 

\begin{asavoir}{Dérivée d'un vecteur unitaire}
	\textbf{Dans le cas d'un vecteur unitaire}$\left [ \frac{\text{d}\,\vec x_{1}}{\text{d}t}\right]_{\mathcal{R}_0} =\vec 0 + \vec \Omega(1/0)\wedge \vec x_{1}$

	On peut alors simplifier le calcul en utilisant les propriétés de la
	dérivation d'un produit :
	$(f \cdot g)' = f' \cdot g + g' \cdot f$

	Soit $\vec U = a \cdot \vec x_{1}$ un vecteur alors
	\[
		\left [ \frac{\text{d}\,a\cdot \vec x_{1}}{\text{d}t}\right]_{\mathcal{R}_0} 
		=
		\frac{\text{d}\,a}{\text{d}t} \cdot \vec x_{1}
		+
		a\cdot \left [ \frac{\text{d}\,\vec x_{1}}{\text{d}t}\right]_{\mathcal{R}_0} 
		=
		\dot a\cdot \vec x_{1}
		+
		a \cdot  \left( \vec \Omega(1/0)\wedge \vec x_{1} \right) 
	\] 
\end{asavoir}

\subsubsection*{$\to $ Exemple : Robot 2 axes}

\begin{minipage}{.48\textwidth}
	\centering
		\def\svgwidth{.9\columnwidth}
		\import{./img/}{robot2axes.pdf_tex}
	% \includegraphics[width=\textwidth]{img/Robot2axes.png}

	Calculer \(\overrightarrow V_{B}(1/0)\) et \(\overrightarrow V_{C}(2/0)\) 
\end{minipage}

\newpage
\section{Accélération d'un point appartenant à un solide}

\begin{asavoir}{Accélération d'un point appartenant à un solide}
	On appelle accélération d'un point M par rapport à un repère R, la
	dérivée temporelle du vecteur vitesse de ce point par rapport au repère
	R : 
	\[
		\vec a_{M}(1/0)=\left [ \frac{\text{d}\,\V{M}(1/0)}{\text{d}t}\right]_{\mathcal{R}_0} 
	\] 
	\textbf{Remarque :} On ne peut pas composer les accélérations :
	$\vec a_{M} (2/0) \boxed{\neq}\,  \vec a_{M} (2/1) + \vec a_{M} (1/0)$
\end{asavoir}


\subsubsection*{Exemple Robot 2 axes}

\begin{minipage}{.48\textwidth}
	\centering
		\def\svgwidth{.9\columnwidth}
		\import{./img/}{robot2axes.pdf_tex}
	% \includegraphics[width=\textwidth]{img/Robot2axes.png}

	Calculer l'accélération \(\vec a_{C} (2/0)\)
\end{minipage}\hfill

\vfill
\section{Rappel des notations}

\begin{tabularx}{.9\textwidth}{rX}
	\((2/1)\) & Mouvement du solide 2 par rapport au référentiel galiléen fixe lié au solide 1. \\
	\(T_{A}(2/1)\) & Trajectoire du point A dans le mouvement \((2/1)\) : Ensemble des positions prises par le point A dans le mouvement (2/1) \\
	\(\overrightarrow \Omega(2/1)\) & Vecteur vitesse de rotation du solide 2 dans son mouvement par rapport 1. \\
	\(\overrightarrow{V_{A}}(2/1)\) & Vecteur Vitesse du point A dans le mouvement de
	2 par rapport à 1.\\
	\(\overrightarrow{a_{A}}(2/1)\) ou \(\overrightarrow{\Gamma_{A}}(2/1)\) & Vecteur accélération du point A dans le mouvement de 2 par rapport à 1. \\
\end{tabularx}


\end{document}

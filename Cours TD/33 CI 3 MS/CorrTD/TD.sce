L=20e-3 //H
f=50 //Hz
V=220 // V
w=2*%pi*f // rd/s
p=2

// question 1
Nm=60*f/p
printf("Nm=%.2f rd/s\n",Nm)

i=5 //A
k=220/(i*f)
printf("k=%.2f\n",k)
// Question 2

phi=36*%pi/180 // rd
I=15 //A

E2=V**2+(L*w*I)**2-2*V*L*w*I*sin(phi)
E=sqrt(E2)
i=E/(k*f)
printf("Cas phi=+36, E=%.0f V et i=%.2f A \n",E,i)


phi=-36*%pi/180 // rd
E2=V**2+(L*w*I)**2-2*V*L*w*I*sin(phi)
E=sqrt(E2)
i=E/(k*f)
printf("Cas phi=-36, E=%.0f V et i=%.2f A \n",E,i)

printf("Q=3VIsin(phi)=+/%3.0f VAR\n",3*V*I*sin(phi))

printf("Couple moteur Cem=%.1f N.m",8000/(2*%pi/60*Nm))

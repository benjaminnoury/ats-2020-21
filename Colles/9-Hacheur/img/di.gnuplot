#!/usr/bin/gnuplot -persist
# vi:syntax=gnuplot 

reset

set terminal cairolatex pdf size 3,2 #mono
set palette cubehelix

unset key

set loadpath '~/.config/gnuplot-config/latex'
load 'xyborder.cfg'
load 'grid.cfg'

set xlabel '$\alpha$'
set ylabel '$(1-\alpha)\times \alpha$'

set xrange [0:1]
#set xrange [.79:.83]
set xtics .25

f(x)=-x**2+x

plot (x<0.8?f(x):x>.82?f(x):1/0) lc 4, (x>0.83?1/0:x>.79?f(x):1/0) lc 1 lw 5

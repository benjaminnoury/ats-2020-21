#!/usr/bin/gnuplot -persist
# vi:syntax=gnuplot 

reset

set terminal cairolatex pdf size 6,4 mono
set palette cubehelix

set key right bottom box

set loadpath '~/.config/gnuplot-config/latex/'
load 'xyborder.cfg'
load 'grid.cfg'


set xlabel 't (s)'
set ylabel 'rd/s'

A=1.
E=11-4

set xrange [0:12]
#set yrange [0:13]
set ytics 1
set xtics 1
set samples 1000

tau=6/3

e(x)=x<2? 0:E+4
s(x)=x<2? 4:A*E*(1-exp(-(x-2)/tau))+4


plot e(x) t '$\omega_\text{consigne}(t)$', s(x) t '$\omega_\text{mot}(t)$' 

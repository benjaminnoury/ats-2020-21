#!/usr/bin/gnuplot -persist
# vi:syntax=gnuplot 

reset

set terminal cairolatex pdf size 6,3.5 mono
set palette cubehelix

set key bottom right box

set loadpath '~/.config/gnuplot-config/latex/'
load 'xyborder.cfg'
load 'grid.cfg'


set xlabel 't (s)'
set ylabel '°'

set xrange [0:12]
set yrange [0:13]
set ytics 1
set xtics 1

A=1
E=12
tau=8/3

e(x)=E
s(x)=A*E*(1-exp(-x/tau))

plot e(x) t '$\theta_\text{Consigne} (t)$', s(x) t '$\theta (t)$'

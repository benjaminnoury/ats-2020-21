#!/usr/bin/gnuplot -persist
# vi:syntax=gnuplot 

reset

set terminal cairolatex pdf size 7,4 mono
set palette cubehelix

set key bottom right box

set loadpath '~/.config/gnuplot-config/latex/'
load 'xyborder.cfg'
load 'grid.cfg'


set xlabel 't (s)'
set ylabel 'distance (m)'
set samples 1000

set xrange [0:12]
set yrange [0:16]
set ytics 1
set mytics 2
set xtics 1

A=.9
E=10
om=5
m=.1
op=om*sqrt(1-m**2)
phi=acos(m)

e(x)=E
s(x)=A*E*(1-exp(-m*om*x)*sin(op*x+phi)/sqrt(1-m**2))

plot e(x) t 'e(t)', s(x) t 's(t)'

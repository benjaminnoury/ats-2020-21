#!/usr/bin/gnuplot -persist
# vi:syntax=gnuplot 

reset

set terminal cairolatex pdf size 7,4 mono
set palette cubehelix

set key bottom right box

set loadpath '~/.config/gnuplot-config/latex/'
load 'xyborder.cfg'
load 'grid.cfg'


set xlabel 't (s)'
set ylabel 'Tension (V)'
set samples 1000

set xrange [0:12]
 set yrange [0:17]
set ytics 1
set xtics 1

A=1.2
E=12
om=3
m=.9
op=om*sqrt(1-m**2)
phi=acos(m)

e(x)=x<3 ? 2:E+2
s(x)=A*E*(1-exp(-m*om*x)*sin(op*x+phi)/sqrt(1-m**2))
s2(x)=x<3 ? 2:s(x-3)+2
plot e(x) t 'e(t)', s2(x) t 's(t)'

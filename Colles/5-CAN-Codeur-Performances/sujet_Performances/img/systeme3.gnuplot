#!/usr/bin/gnuplot -persist
# vi:syntax=gnuplot 

reset

set terminal cairolatex pdf size 7,4 mono
set palette cubehelix

set key bottom right box

set loadpath '~/.config/gnuplot-config/latex/'
load 'xyborder.cfg'
load 'grid.cfg'


set xlabel 't (s)'
set ylabel 'distance (m)'
set samples 1000

set xrange [-1:12]
 set yrange [0:11]
set ytics 1
set mytics 2
set xtics 1

A=1
E=8
om=3
m=1/sqrt(2)
op=om*sqrt(1-m**2)
phi=acos(m)

e(x)=E
s(x)=A*E*(1-exp(-m*om*x)*sin(op*x+phi)/sqrt(1-m**2))

e2(x)=x<0 ? 1:e(x)+1
s2(x)=x<0 ? 2:s(x)+2


plot e2(x) t 'e(t)', s2(x) t 's(t)'

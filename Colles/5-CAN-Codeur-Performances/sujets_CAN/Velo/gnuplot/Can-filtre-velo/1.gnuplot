set table "gnuplot/Can-filtre-velo/1.table"; set format "%.5f"
set samples 50.0; set parametric; plot [t=-3:0] [] [] log10(10**t),20*log10(abs(1/sqrt(1+(10*10**t)**2)))

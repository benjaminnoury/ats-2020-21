#!/usr/bin/gnuplot -persist
# vi:syntax=gnuplot 

reset

set terminal cairolatex pdf size 6,4 #mono
set palette cubehelix


set loadpath '~/.config/gnuplot-config/latex'
load 'xyborder.cfg'
load 'grid.cfg'

set xlabel 't (s)'
set ylabel '° (rd)'

set sample 500
set xrange [-1:3]
# set xtics 1 
unset key

tau=.7
A=.8
E=1
e(x)=x>0?E:0
s(x)=x>0?A*E*(1-exp(-x/tau)):0

r1(x)=A*E*.95
r2(x)=A*E*1.05

plot e(x), s(x), r1(x) , r2(x)

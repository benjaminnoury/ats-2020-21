---
titre: Centrifugeuse de laboratoire
type: Corrigé DM 1
classe: ATS
CI: 7 Transmettre l'énergie - Mouvement
competence: 
#question-section: 1
#document: twocolumn,landscape
---


\question{ $\ovr \Omega_{2/1}=\dot \alpha \vec z_{1}$,\quad $\ovr \Omega_{3/2}=\dot \beta \vec y_{2}$, \quad $\ovr \Omega_{3/1}=\dot \alpha \vec z_{1} + \dot \beta \vec y_{2}$
}

\question{ Calcul de $\V{A_{3},3/1}$
	 
\fbox{Par la cinématique du Solide (Composition + Changement de point)}
	 $\begin{aligned}[t]
	\V{A_{3},3/1}& =\V{A_{3},3/2}+\V{A_{3},2/1} \\ 
	&= \V{O_{3},3/2} + 	 \ovr{A_{3}O_{3}} \wedge \ovr \Omega_{3/2}
	+\V{O_{1},3/2} + 	 \ovr{A_{3}O_{1}} \wedge \ovr \Omega_{2/1}\\
	& = - b \, \vec x_{3} \wedge \dot \beta	\vec y_{3} - \left (h\, \vec z_{1}+ a \, \vec x_{2} +b \, \vec x_{3} \right ) \wedge \dot \alpha \vec z_{1}\\
	&= - b \dot \beta \, \vec z_{3} + \left (a + b \cos \beta \right )  \dot \alpha \, \vec y_{2}
	 \end{aligned}$

\, 

\fbox{Par la Cinématique du Point (Dérivation)}
$\begin{aligned}[t]
	A_{3} \in 3 \Rightarrow \V{A_{3},3/1}
	= \left [ \ddt[\ovr{O_{1}A_{3}}] \right]_{\mathcal \mathcal R_1}
	& = \left [ \ddt[\left (h \vec z_{1} + a \vec x_{2}+b \vec x_{3} \right )] \right]_{\mathcal R_1} \\
	& = a \left [ \ddt[\vec x_{2}] \right]_{\mathcal R_1} + b \left [ \ddt[\vec x_{3}] \right]_{\mathcal R_1} \\
	& = a \ovr \Omega_{2/1} \wedge \vec x_{2} + b \ovr \Omega_{3/1} \wedge \vec x_{3}\\
	& = a \dot \alpha \, \vec z_{2} \wedge \vec x_{2}+ b \left (\dot \alpha \vec z_{2}+ \dot \beta \vec y_{3} \right ) \wedge \vec x_{3}\\
	& = a \dot \alpha \, \vec y_{2} + b \left (- \dot \beta \, \vec z_{3}+ \dot \alpha \cos \beta \, \vec y_{2} \right )
	\end{aligned}$
}

$$\boxed{
	\V{A_{3},3/1}=- b \dot \beta \, \vec z_{3} + \left (a + b \cos \beta \right )  \dot \alpha \, \vec y_{2}
}$$
\question{Phase stabilisée pour $N_{m}=850$ tr/min, soit $\dot \beta=0$ rd/s, $\dot \alpha =c^{te}$, $\beta=6°$

d'où $\V{A_{3},3/1}=\underbrace{\left (a + b \cos \beta \right )  \dot \alpha }_{constante}\, \vec y_{2}$

$$
a_{utile}=\underbrace{\left [ \ddt[\V{A_{3},3/1}.\vec x_{3}] \right]_{\mathcal R_1}}_{\vec y_{2}. \vec x_{3}=0} - \V{A_{3},3/1} . \left [ \ddt[\vec x_{3}] \right]_{\mathcal R_1} = -
\left (a + b \cos \beta \right )  \dot \alpha \, \vec y_{2} \,\, . \,  \left ( \dot \alpha	\cos \beta \, \vec y_{2}  \right )
=- \left (a + b \cos \beta \right ) \dot \alpha^{2}\cos \beta
$$
$$\boxed{a_{utile}=-	\left (a + b \cos \beta \right ) \dot \alpha^{2}\cos \beta}$$

$a_{utile}(N_{m}=850)=189\,g$, le Cahier des charges est validé avec une marge de 10\%.
}


\question{Pour $N_{m}$ 10\,000 tr/min, $\beta=0°$, d'où $a_{utile}(N_{m}=10\,000)=26 x \,320\,g$, le cahier des charges est validé.}

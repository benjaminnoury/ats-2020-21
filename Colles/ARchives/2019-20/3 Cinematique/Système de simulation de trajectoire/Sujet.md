---
titre: Système de simulation de trajectoires
type: TD
classe: ATS
CI: 7 Transmettre l'énergie --- Mouvements
#competence: 
#question-section: 1
#document: twocolumn,landscape
---

Lors du largage d’un missile par un avion porteur, la trajectoire réelle de celui-ci s’écarte parfois considérablement de la trajectoire souhaitée. Du fait des phénomènes fortement non-linéaires qui caractérisent les écoulements aérodynamiques, il se peut que le missile larguée soit pris dans les turbulences engendrées par l’avion et revienne percuter celui-ci.

\ 

Pour étudier ces phénomènes, les ingénieurs de l’O.N.E.R.A. (Office National d’Études et de Recherches Aérospatiales) a réalisé un système de trajectographie captive. Celui-ci consiste en une maquette d’avion à l’échelle 1/6, larguant une maquette du missile. Chacune est portée par un bras articulé, doté de nombreux degrés de liberté et pouvant reproduire tous les mouvements tridimensionnels des objets portés (cf. photos ci-dessous).

\Begin{minipage}{.55\lw}
	\image{img/photo1}
\End{minipage}\hfill
\Begin{minipage}{.4\lw}
	\image{img/photo2}
\End{minipage}

\ 

L’étude porte donc sur cette maquette dont le schéma de principe (figure 2 et 3) et le schéma cinématique paramétré (figure 4) sont représentés ci –dessous.

Afin de simplifier l’étude, on se limitera au cas où le déplacement de la charge se fait dans le plan de la figure et suivant les liaisons L1, L3 et L5.

\ 

Dans l’étude suivante, on s’intéressera au système automatisé de déplacement du missile par rapport à l’avion.

Les mouvements de rotation (3/2 et 2/1) ou translation (1/0) entre les différents bras sont engendrés par des vérins électriques à vis V3, V2 et V1 (voir représentations 2D ci-dessous).

\image[.7]{img/schema}

\image[.7]{img/schemaC_Axes}


\question{Réaliser un graphe des liaisons, préciser le paramètre de position associé à chaque liaison.}

\question{}

---
# titre:  Potence à tirant --- Exercice Corrigé
type: Exercice Corrigé
classe: ATS
CI: 8 Transmettre l'énergie --- Efforts
competence: 
#question-section: 1
#ficheTD: 1
document: twocolumn,landscape
---

\section*{Potence à Tirant}


La Potence à tirant est un équipement de manutention

La colonne 1 est articulée en A et F et pivote autour de l’axe $(F,\, \vec y)$ .  Le palan peut se déplacer le long de la flèche 3. 




\image[.9]{img/planAdapt}




\begin{coteAcote}[.25]
\image{img/schema_cine}

\tcblower

\fbox{Le problème est plan}

\ 

a = 240 mm

b = 3 750 mm

c = 200 mm

d = 450 mm

e = 260 mm

f = 1 470 mm

BC = L (inconnue)

$\alpha$ = 73°

$200 < \lambda < 3 750 $mm

$P_{max}$ = 500 daN
\end{coteAcote}


\begin{rem}{Objectif}

Déterminer les actions mécaniques sur les solides (2) et (3) afin de pouvoir les dimensionner.
	
Les calculs s’effectueront de manière analytique et les résultats seront donnés en fonction du paramètre $\lambda$.

\end{rem}

La charge transportée par le palan 4 est représentée par la résultante $\vec P$ exercée en $M$.

\question{Elaborer le graphe de structure du mécanisme.}

### Isolement du Tirant 2 :

\question{Effectuer le bilan des Actions Mécaniques extérieures sur le tirant 2.  Matérialiser l’isolement sur le graphe des liaisons.}



\question{Appliquer le Principe Fondamental de la Statique sur le tirant 2. Résoudre.}

### Isolement de la flèche 3 et du palan 4 

\question{Effectuer le bilan des AM le système isolé E=\{3+4\} (matérialiser l’isolement sur le graphe des liaisons).}

\question{Appliquer le Principe Fondamental de la Statique sur le système isolé E={3+4}.}

\question{Résoudre. Tracer $X_{13}$, $Y_{13}$ et $Y_{32}$ en fonction de $\lambda$.}


\Begin{asavoir}{Résumé de la méthode}
	
1. Faire un graphe Actions mécaniques : *Graphe liaisons + Actions extérieures*

2. Isoler un solide ou un ensemble de solides
	- *Sur le graphe, entourer l'isolement.*
	- *Les actions intervenant dans le bilan sont celles qui intersectent l'isolement.*

3. Faire le bilan des actions mécaniques:

	- *Écrire tous les torseurs des actions identifiées lors de l'isolement au point le plus simple. (Point du graphe des liaisons)*
	- *Pour les liaisons, le torseur des actions transmissibles est le complémentaire du torseur cinématique*
	- *Pour les actions extérieures (Moteur, vérin, charge, pesanteur, ...), suivre le sujet : *
		- *si l'action est une force, elle sera représentée par un glisseur ;*
		- *si l'action est un couple, elle sera représentée par un torseur couple.*
	- *Penser à l'hypothèse "Problème plan".*

<!--
4. Appliquerle PFS
	- *D'abord en résultante, et vérifier s'il est possible de résoudre (répondre à l'objectif)*
	- *Puis en moment.*
		- *Il faut tous les moments au même point !*
		- *Choisir le point où il y a le plus d'inconnues, cela simplifie les calculs*

-->
5. Résoudre les équations
	- *Par le pivot de Gauss ou la méthode de substitution*

\End{asavoir}



\newpage\setcounter{ques}{0}


### Remarques générales

Le sujet fait l'hypothèse que le problème est plan. Dans notre cas, le plan est le plan $(A,\, \vec x,\, \vec y)$, les torseurs auront donc pour écriture : $$\tam{* \to *}=\tc[R]{M}{ X & - \\ Y & -\\ - &N}$$



\question{Elaborer le graphe de structure du mécanisme.}



Le graphe de structure, aussi appelé graphe des actions mécaniques ou graphe d'actions-liaison est un graphe des liaisons sur lequel apparait en plus les actions extérieures. Ici, il n'y a qu'une action extérieures celle exercée par $\vec P$ sur le palan 4.

\image[.7]{img/GrapheAM}

<!--
Les chiffres qui apparaissent sur le graphe sont les nombres d'inconnues de liaison pour chaque liaison. (*Exemple : Pour une liaison pivot, 1 degré de liberté $\to$ 5 inconues de liaison*)
-->












### Isolement du Tirant 2 :

\question{Effectuer le bilan des Actions Mécaniques extérieures sur le tirant 2.  Matérialiser l’isolement sur le graphe des liaisons.}


\begin{coteAcote}
\image{img/GrapheAM-2}
\tcblower

Sur le graphe, l'isolement intersecte 2 liaisons : $(1 - 2)$ et  $(3 - 2)$. 

Il y aura donc 2 torseurs d'actions mécaniques transmissibles à écrire : $\tam{1 \to 2}$ et $\tam{3 \to 2}$


\end{coteAcote}

\underline{Bilan des actions mécaniques extérieures (BAME)}


\hfill $\tam{1 \to 2}=\tc[R]{C}{ X_{12} & -\\ Y_{12}  & -\\ - &0}$ \hfill 
$\tam{3 \to 2}=\tc[R]{B}{ X_{32} & -\\ Y_{32}  & -\\ - &0}$ \qquad \ 


\newpage
\question{Appliquer le Principe Fondamental de la Statique sur le tirant 2. Résoudre.}


### Application du PFS en résultante : 

$$\vec R(1 \to 2) + \vec R(3 \to 2) = \vec 0$$


- en projection sur $\vec x$ : $\boxed{X_{12} + X_{32} =0}$ \dotfill (1)
- en projection sur $\vec Y$ : $\boxed{Y_{12} + Y_{32} =0}$ \dotfill (2)


\ 


Pour appliquer le PFS en moment, il faut tous les moments au même point. Choisissons le point $B$. (le calcul est semblable en $C$)

\begin{coteAcote}[.3]
$$\begin{aligned}
\ovr{M_B}(1 \to 2)& = \ovr{M_C}(1 \to 2) + \ovr{BC} \wedge \vec R (1 \to 2) \\
& = \vec 0 + L \, \vec y_{1} \wedge (X_{12} \, \vec x + Y_{12} \, \vec y) \\
& = - L \left ( X_{12} \cos (\alpha) + Y_{12} \sin (\alpha) \right ) \, \vec z
\end{aligned}$$

\tcblower
\image{img/figCalc}
\end{coteAcote}

$$\tam{1 \to 2}=
\tc[R]{C}{ X_{12} & -\\ Y_{12}  & -\\ - &0} = 
\tc[R]{B}{ X_{12} & -\\ Y_{12}  & -\\ - & - L \left ( X_{12} \cos (\alpha) + Y_{12} \sin (\alpha) \right )}$$

\ 


### Application du PFS en moment au point B : 

$$\ovr{M_B}(1 \to 2) + \ovr{M_B}(3 \to 2) = \vec 0$$

soit, en projection sur $\vec z$ : $0 - L \left ( X_{12} \cos (\alpha) + Y_{12} \sin (\alpha) \right ) = 0$

\ 

L étant non nul, on obtient : $X_{12} \cos (\alpha) + Y_{12} \sin (\alpha) = 0$, soit $\boxed{X_{12} = - Y_{12} \tan (\alpha)}$


\Begin{asavoir}{Remarque}\parindent=\oldparindent

On est ici sur un cas classique à connaitre : \bb{Solide (ou ensemble de solide) soumis à 2 glisseurs}

Dans ce cas, les résultantes des 2 glisseurs sont :

- égales en normes
- opposée 
- portées par la droite reliant les points d'applications des glisseurs.

\tcblower \parindent=\oldparindent

Dans notre cas cela donnait : 

- $\vec R(3 \to 2) = - \vec R (1 \to 2)$
- $\begin{aligned}[t]
	\vec R (1 \to 2) \sslash (BC) & \implies \vec R(1 \to 2) \sslash \vec y_{1}\\
	& \implies \vec R(1 \to 2) = R \, \vec y_{1} \\
	& \implies \left \lbrace 
		\begin{array}{l}
			X_{12} = - R \sin (\alpha)\\
			Y_{12} = R \cos (\alpha)
		\end{array} \right .\\
	& \implies X_{12} = - Y_{12} \tan (\alpha)	
	\end{aligned}$

En général, si on utilise cette méthode, on préférera s'arrêter après avoir posé $\vec R(3 \to 2) = R \, \vec y_{1}$
\End{asavoir}


### Isolement de la flèche 3 et du palan 4

\question{Effectuer le bilan des AM le système isolé E={3+4} (matérialiser l’isolement sur le graphe des liaisons).}


\begin{coteAcote}[.4]
\image{img/GrapheAM-34}
\tcblower
\parindent=\oldparindent
L'isolement intersecte 3 actions mécaniques :
\Begin{itemize}
	\item 2 actions de liaisons : \\ $\tam{2 \to 3}$ et $\tam{1 \to 3}$
	\item 1 actions extérieure : $\tam{P \to 4}$ 
\End{itemize}
\end{coteAcote}


$$\tam{1 \to 3} = \tc[R]{A}{ X_{13} & - \\ Y_{13} & -\\ - &0}
\qquad
\tam{2 \to 3} = -\tam{3 \to 2} = \tc[R]{B}{ Y_{32} \, \tan (\alpha) & - \\ - Y_{32} & -\\ - &0}$$

\ 

Pour $\tam{2 \to 3}$, on réutilise les résultats de la question précédente.

\ 

$$
\tam{P \to 4} = \tc{M}{ -P_{max} \, \vec y \\ \vec 0}
$$



\question{Appliquer le Principe Fondamental de la Statique sur le système isolé E=\{3+4\}.}


### Application du PFS en résultante :

$$\vec R(1 \to 3) + \vec R(2 \to 3) + \vec R(P \to 4) =\vec 0$$

- en projection sur $\vec x$ : $\boxed{X_{13} + Y_{32} \tan (\alpha) = 0}$
- en projection sur $\vec y$ : $\boxed{Y_{13} - Y_{32} - P_{max} =  0}$


### Application du PFS en Moment 

Pour appliquer le PFS en moment, il faut tous les moments au même point. 

\ 

\bb{Pour avoir les équations les plus simples à résoudre, on choisit comme point d'écriture le point où il y a le plus d'inconnues.}

\ 

\begin{tabular}{l@{$\to$}l@{: }l} 
 en A & 2 inconnues  & $X_{13}$ et $Y_{13}$\\
 en B & 1 inconnue & $Y_{32}$\\
 en M & 1 donnée & $P_{max}$
\end{tabular}

On choisit donc d'écrire les moments au point A.

\ 

$\begin{aligned}
\ovr{M_A}(2 \to 3) &= \vec 0 + \ovr{AB} \wedge \vec R(2 \to 3) \\
&= (b \, \vec x + c \, \vec y) \wedge (Y_{32} \, \tan (\alpha) \, \vec x - Y_{32} \, \vec y) \\
&= - (c \tan (\alpha) + b) \, Y_{32}\, \vec z
\end{aligned}$

\ 

\ 

$\begin{aligned}
\ovr{M_A}(P \to 4) & = \vec 0 + \ovr{AM} \wedge \vec R(P \to 4)\\
&= \vec 0 + \lambda \, \vec x \wedge (-P_{max} \, \vec y) \\
&= - \lambda \, P_{max}\, \vec z
\end{aligned}$

\ 

\ 

Le PFS en moment au point A donne donc :

$$- \lambda \, P_{max} - (c \tan (\alpha) + b) \, Y_{32} + 0 = 0$$

Soit : $\boxed{Y_{32} = \dfrac{-\lambda}{c \, \tan (\alpha) + b} P_{max}}$


\newpage 

\question{Résoudre. Tracer $X_{13}$, $Y_{13}$ et $Y_{32}$ en fonction de $\lambda$.}

On déduit de ce qui précède

\ 

- $\boxed{Y_{32} = \dfrac{-\lambda}{c \, \tan (\alpha) + b} \,  P_{max}}$
- $\boxed{X_{13} = - X_{32} = \dfrac{-\lambda \tan (\alpha)}{c \, \tan (\alpha) + b} \, P_{max}}$
- $\boxed{Y_{13}= Y_{32} + P_{max} = \left ( 1 - \dfrac{\lambda}{c \, \tan (\alpha) + b} \right ) P_{max}}$

\ 

Application Numérique :

\image{img/trace-AN}

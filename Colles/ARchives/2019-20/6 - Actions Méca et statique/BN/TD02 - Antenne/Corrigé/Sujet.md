---
titre: Radar Météorologique bande X
type: Corrigé 
classe: ATS
CI: 8 transmettre l'énergie --- Efforts
#question-section: 1
#ficheTD: 1
#document: twocolumn,landscape
---


\question{$$
	\tam{1 \to 2} = \tc[R_{0}]{A}{ X_{A} & 0 \\ Y_{A} & M_{A}\\ Z_{A} &N_{A}}	
$$}

\question{
$$\tam{vent \to 2} = \tc{P}{ -F_{v} \,\vec y_{0}\\ 0 }
\qquad \tam{Pesanteur \to 2} = \tc{P}{ - M_{p} \, g \,\vec z_{0} \\ \vec 0 }
\qquad \tam{Pesanteur \to 3} = \tc{B}{ - M_{3} \,g \,\vec z_{0}\\ \vec 0}$$
}

\question{ {\em rappel : $\alpha=0$}

$\ovr{M_A}(vent \to 2)= \vec 0 + \ovr{AP} \wedge \vec R(vent \to 2) = (a \, \vec z_{2} + c \, \vec y_{2}) \wedge (-F_{V} \, \vec y_{0}) = \left (a \cos (\beta) + c \sin (\beta) \right ) \, F_{v} \, \vec x_{0}$

\ 

$\ovr{M_A}(pesanteur \to 2) = \vec 0 + \ovr{AP} \wedge \vec R(pesanteur \to 2) = \left 
(a \, \vec z_{2} + c \, \vec y_{2}) \wedge (-M_{P} \, g \, \vec z_{0} \right ) = - \left (c \cos (\beta) - a \sin (\beta) \right )\, M_{P} \, g \, \vec x_{0}$

\ 

$\ovr{M_A}(pesanteur \to 3) = \vec 0 + \ovr{AB} \wedge \vec R(pesanteur \to 3) = 
(a \, \vec z_{2} - b \, \vec y_{2}) \wedge (-M_{3} \, g  \, \vec z_{0}) = \left ( a \sin (\beta) + b \cos (\beta) \right ) M_{3} \, g \, \vec x_{0}
$
}

\question{$\tam{Moteur \to 2} = \tc{*}{\vec 0 \\ C_{ms}\, \vec x_{0} }
$

On isole {2+3}. Par application du principe fondamental de la statique 

\Begin{itemize}
	\item En résultante : 
	\Begin{itemize}
		\item sur $\vec x_{0}$ : $X_{A} =0$
		\item sur $\vec y_{0}$ : $Y_{A} -F_{v}=0$
		\item sur $\vec z_{0}$ : $Z_{A} - (M_{p} +M_{3}) \, g=0$
	\End{itemize}
	
	\item En moment au point A :
	\Begin{itemize}
		\item sur $\vec x_{0}$ : $C_{ms} + \left (a \cos (\beta) + c \sin (\beta) \right ) \, F_{v} - \left (c \cos (\beta) - a \sin (\beta) \right )\, M_{P} \, g + \left ( a \sin (\beta) + b \cos (\beta) \right ) M_{3} \, g $
		
		\item sur $\vec y_{0}$ : $M_{A} = 0$
		
		\item sur $\vec z_{0}$ : $N_{A}=0$
	\End{itemize}
\End{itemize}
}


\question{Par vent nul et pour $\beta=0$, on obtient :
$C_{ms} = \left (b \, M_{3} -c \, M_{p} \right ) \, g$

\ 

L'équilibrage est donc obtenu pour : $M_{3} = \dfrac{c}{b} \, M_{p}$

}

\question{
$\displaystyle C_{ms} = \left (c \cos (\beta) - a \sin (\beta) \right )\, M_{P} \, g - \left ( a \sin (\beta) + b \cos (\beta) \right ) \frac{c}{b} \, M_{p}\, g = - a \, \left (1+ \frac{c}{b}\right ) \, M_{p} \, g$
}

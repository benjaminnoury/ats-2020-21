---
titre: Lanceur de rouleaux de papier d'imprimerie
classe : TSI 1
type: TD
chapitre: Statique avec frottement
---

\image[.6]{img/photo.png}


# Présentation.
Lorsque l’on désire imprimer un très grand nombre d’exemplaires d’un même ouvrage (journal à grand tirage, annuaire téléphonique,...) on utilise généralement des machines à impression rotative.
Ces machines sont alimentées en papier, grâce à de gros rouleaux de papier situés en amont.

Au démarrage, les moteurs d'entraînement du rouleau, n’ont pas un couple moteur suffisant pour lancer celui-ci. C’est pour cela que l’on utilise un lanceur, qui a pour fonction d’accélérer le rouleau plein, d’une vitesse initiale nulle à sa vitesse de fonctionnement normal.

\image[.6]{img/schema.png}

Le rouleau **(1)**, en liaison pivot d'axe $(A,\vec z)$ avec le bâti **(3)**, est entraîné en rotation par adhérence à l'aide d’une courroie **(4)** plaquée par un vérin **(6+7)** sur celui-ci, et entraînée par une poulie motrice d'axe $(G,\vec z)$

\newpage{}

# Dimensionnement du lanceur

**Hypothèses**

- Liaisons parfaites (sans adhérence), à l’exception du contact entre la courroie 4 et le rouleau 1.
- Modélisation globale de l'action 4 sur 1 en E par : $\{4 \rightarrow 1 \} = \tc{E}{T_{E} \vec x + N_{E} \vec y \\ \vec 0}$
- Angle d'adhérence entre Ie rouleau 1 et la courroie 4 : $\varphi$=30°
- Poids des pieces négligé.
- Rayon du rouleau plein : R = 0,6 m.
- Couple résistant au démarrage du rouleau, appliqué sur l'axe (AE) : $C_{R}=176$ N.m 


\question{On suppose qu’au démarrage le rouleau 1 est en équilibre statique, sous l’action de 4 sur 1,
de 3 sur 1, et du couple résistant $C_{R}$ .
\begin{enumerate}
	\item Donner l’expression de la résultante tangentielle $T_{E}$ en fonction de $C_{R}$, et $R$,
	\item Faire une application numérique.
	\item En déduire l’expression de la résultante normale $N_{E}$ en fonction de $\varphi$ et $T_{E}$.
	\item Faire une application numérique.
\end{enumerate}}

\begin{multicols}{2}

\question{En supposant que L est la largeur de la courroie et que la répartition de pression p entre la courroie 4 et le rouleau 1 est uniforme (schéma ci- contre), déterminer l’expression de $N_{E}$ en fonction de $p$, $\beta$ , $L$ et $R$}

\bb{Remarque} : Sur le schéma ci-contre, les
composantes élémentaires d'adhérence (ou de
résistance au glissement) $\text{d}T_{E}$ n'ont pas été
représentées.	
	
Pour les questions suivantes, on prendra $T_{E}=300$ N et $N_{E}=500$ N.
	\image{img/repartition.png}
\end{multicols}


\question{En prenant $\beta=30$° , calculer la largeur $L$ de la courroie, pour que la pression de contact $p_{1}$ ne dépasse pas 1 daN.cm$^{2}$}

On pose $\vec R (6 \rightarrow  5) = X_{F}\vec x+Y_{F}\vec y$

\question{Par un isolement judicieux, donner une relation entre $X_{F}$, $Y_{F}$, c et d}

\question{En précisant le système isolé, déterminer une 2ème relation entre $X_{F}$, $Y_{F}$, $T_{E}$ et $N_{E}$}

\question{En déduire $X_{F}$, $Y_{F}$ et $\norm{\vec R(6 \rightarrow 5)}$. Faire l'application numérique.}

\question{Sachant que la pression d'huile $P_{h}$ est de 6 bar, calculer la section utile $S_{u}$ du vérin}

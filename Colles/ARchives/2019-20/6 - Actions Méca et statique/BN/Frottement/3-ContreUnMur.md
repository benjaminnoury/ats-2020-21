# Pièce coincée contre un mur

\setcounter{ques}{0}

\image[.7]{img/mur.png}

La pièce (1) est maintenue en équilibre contre une paroi verticale (0) par un galet (2). Ce galet(2) est poussé vers la gauche par un ressort (4).

Le galet est en liaison pivot par rapport à (3). Cette liaison est sans frottement.

Le coefficient de frottement entre (0) et (1) est f = $\tan \varphi$ = 0,2.

La répartition de pression q(x) entre (0)
et (1) est supposée trapézoïdale. Le problème est assimilé à un problème
plan, q(x) est donc une répartition de
pression linéaire (q(x) en N/m).

L’action $\vec F(0 \rightarrow 1)$ est modélisée par un glisseur dont le support passe par D, point de AB.



P = poids de (1) = 200N

L=0,5 m ; h=1 m



\question{Déterminer l’action minimale du galet (2) sur la pièce (1) pour que celle-ci ne glisse pas sur (0).}

\question{Déterminer dans ces conditions, le point d’application de l’action de $\{0 \rightarrow 1 \}$.}

Le ressort est taré de telle façon que $\norm{\vec F(2 \rightarrow 1)}$ = 2000 N. 
\question{Déterminer le nouveau point d’application de $\vec F (0 \rightarrow  1)$.}


\question{Justifier le fait que la répartition de pression ne soit pas uniforme.}

\question{Déterminer $q_1$ et $q_{2}$ pour les conditions de la question 1 et pour les conditions de la question 4.}

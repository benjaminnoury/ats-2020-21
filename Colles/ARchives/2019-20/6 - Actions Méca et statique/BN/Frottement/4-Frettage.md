# Assemblage par Frettage

\setcounter{ques}{0}

Le frettage consiste à encastrer deux pièces en utilisant le phénomène d’adhérence.

\hfill \includegraphics[width=.4\lw]{img/frett1.png} \hfill	\includegraphics[width=.4\lw]{img/frett2.png} \hfill


Avant l’assemblage réalisé à l’aide d’une presse, l’arbre (1) possède un diamètre légèrement supérieur à celui de l’alésage (trou cylindrique) de la pièce (2) dans laquelle il vient se loger. 
	
Après frettage, il subsiste donc une pression de contact p (souvent supposée uniforme sur toute la surface de contact) entre les deux pièces. \\
	



Les caractéristiques de cet assemblage par frettage sont :

- Rayon de l’arbre 1 : R
- Longueur du contact : L
- Coefficient d’adhérence entre les deux pièces :  $f$

\newpage 

## Effort Axial Maximal Transmissible	
\hfill \includegraphics[width=.4\lw]{img/frettA1.png}\hfill \ 

L’effort axial maximal transmissible correspond à la valeur maximale de la
composante axiale de la résultante de l’action mécanique qui peut être transmise
d’une pièce à l’autre sans qu’elles se désolidarisent.

Pour simplifier notre étude, on considère la pièce 2 fixe et on cherche à
déterminer la composante axiale de la résultante de l’action mécanique à
appliquer à la pièce 1 pour atteindre le glissement de 1/2 suivant $- \vec z$





\question{Refaire en grand les 2 schémas ci-dessous : un dans le
plan $(\vec y_{}, \vec z_{})$ et l’autre dans le plan $(\vec x_{}, \vec y_{})$, en plaçant les actions
élémentaires normale et tangentielle de 2 sur 1 en un point Q
quelconque de la surface de contact.}
\image[.7]{img/frettA2.png}
\question{Exprimer $\ovr{\text{d}F_{2 \rightarrow 1}(Q)}$}

\question{Déterminer la résultante axiale maximale
transmissible en fonction de p et des caractéristiques
géométriques du frettage.}



\newpage{}

## Couple Maximal Transmissible



\hfill \includegraphics[width=.4\lw]{img/frettC1.png}\hfill \ 

Le couple (ou moment) maximal transmissible correspond à la valeur du moment résultant de l’action
maximale de la composante sur l’axe $\vec z$ mécanique qui peut être transmise d’une pièce à l’autre sans qu’elles se
désolidarisent.

Pour simplifier notre étude, on considère la pièce 2 fixe et on cherche à 
déterminer la composante sur l’axe $\vec z$ du moment résultant de l’action mécanique à appliquer à la pièce 1 pour atteindre le glissement de 1/2 autour de $\vec z$






\question{Refaire en grand les 2 schémas ci-contre : un dans le
plan $(\vec y_{}, \vec z_{})$ et l’autre dans le plan $(\vec x_{}, \vec y_{})$, en plaçant les actions
élémentaires normale et tangentielle de 2 sur 1 en un point Q quelconque de la surface de contact.}
\image[.7]{img/frettC2.png}

\question{Exprimer $\ovr{\text{d}F_{2 \rightarrow 1}(Q)}$}

\question{Déterminer le couple maximal
transmissible en fonction de p et des caractéristiques
géométriques du frettage.}

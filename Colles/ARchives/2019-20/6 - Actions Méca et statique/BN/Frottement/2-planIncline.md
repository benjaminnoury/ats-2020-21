# Parallélépipède sur plan incliné


\setcounter{ques}{0}

\image[.7]{img/rouleau.png}

Sur un plan incliné [0] un parallélépipède rectangle [1] retient une barre cylindrique de révolution [2].

\question{Écrire les équations scalaires déduites du principe
fondamental de la statique appliqué à [1] puis à [2].}

On suppose qu'à la rupture de l'équilibre, [1] glisse
sur [0] sans basculer, [2] roule sans glisser sur [0] et
roule et glisse sur [1].

\question{Écrire les 2 équations scalaires que l'on obtient lorsque le système est à la limite du glissement.}

\question{Déterminer la valeur maximale de $\alpha$ pour que le
système reste en équilibre par rapport à [0]}

\question{À la limite du glissement, déterminer les inconnues de liaisons.}

\question{Vérifier la validité de l'hypothèse de rupture d'équilibre faite à la question 2.}



Solide [1] (Parallélépipède)

:	m=4 kg ; l=10cm ; h=15cm

Solide [2] (Cylindre) 

:	M=12 kg ; r=10 cm \\

g=10 m/s$^{2}$ ; f=0,2 entre les 3 solides

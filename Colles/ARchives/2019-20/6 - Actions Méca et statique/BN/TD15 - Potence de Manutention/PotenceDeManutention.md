---
titre: Potence de manutention
type: TD
classe: TSI 1
chapitre: Statique Plane
#question-section: 1
#document: twocolumn,landscape
---



Une potence utilisée en manutention se compose d'une flèche (3) articulée en A sur une colonne pivotante (1) et d'un tirant (2) articulé en D sur (1) et en B sur (3). 

L'ensemble est en liaison pivot (axe vertical EF) avec le le mur (0). 
Cette liaison pivot est réalisée par une *rotule* en F et une *linéaire annulaire* d'axe (E,$\vec y$)

\image{img/schemaRetouch.png}

Objectif:
:	Déterminer les actions mécaniques dans les liaisons en F et E. Cette étude permettra le choix des roulements à implanter.
	
	Déterminer les actions mécaniques dans le tirant (2), cela permettra d'étudier son comportement à la déformation (cours de 2^ème^ Année)

Modélisation:
:	\ 

	- Les solides (3) et (6) sont supposés solidaires
	- Le poids de la charge à lever est modélisé par un glisseur de résultante $\vec P$ en M avec $\norm{\vec P} = P= 2000$ daN
	- Le poids des autres solides est négligé.
	- Le problème est plan



\question{On représente l'action de liaison en B entre (2) et 3 par le torseur $\{2 \rightarrow 3\} = \tc{B}{X_{B} & - \\ Y_{B} & -\\ - &0}$

Isoler le tirant 2. Faire le Bilan des Actions Mécaniques Extérieures. Appliquer le PFS et déterminer une relation entre $X_{B}$ et $Y_{B}$ }

\question{Isoler la flèche (3+6). Faire le bilan des actions mécaniques extérieures. Appliquer le PFS.

En déduire les actions mécaniques en B, en D et en A  en fonction de P}


\question{Isoler l'ensemble {1+2+3+6}. Faire le bilan des actions mécaniques extérieures. Appliquer le PFS.

En déduire les actions mécaniques en  F et E en fonction de P}
\question{Faire l'application numérique}

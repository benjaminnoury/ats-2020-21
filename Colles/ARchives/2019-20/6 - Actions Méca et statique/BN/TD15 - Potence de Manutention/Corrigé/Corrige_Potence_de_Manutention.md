---
titre: 
type: Corrigé Potence de Manutention
classe: Statique
chapitre: 
#question-section: 1
#document: twocolumn,landscape
---


\question{}


(2) est soumis à 2 glisseurs $\{3 \rightarrow 2\}$ et $\{1 \rightarrow 2\}$. Donc, par application du PFS, les résultantes $\vec R(3 \rightarrow 2)$ et $\vec R(1 \rightarrow 2)$ sont :

- égales
- opposées
- portées par la droite (CD)

\begin{center}
	d'où : $\displaystyle \frac{Y_{B}}{X_{B}}= \frac{-1000}{570+2940+240-450} = -\frac{10}{33}$

\end{center}



\question{On isole 3}

$\{2 \rightarrow 3\}=\tc{B}{ X_{B} & - \\ Y_{B} & -\\ - &0}$ \quad $\{1 \rightarrow 3\}=\tc{A}{ X_{A} & - \\ Y_{A} & -\\ - &0}$ \quad $\{P \rightarrow 3\}=\tc{M}{ 0 & - \\ - P & -\\ - &0}$



PFS en résultante
:	\ 
	
	sur $\vec x$ : $X_{A}+X_{B}=0$
	
	sur $\vec y$ : $Y_{A}+Y_{B}-P=0$

Calcul des Moments
:	\ 
	
	$\vec M_{A}(2 \rightarrow 3)=\vec 0 + \ovr{BA} \wedge \vec R(2 \rightarrow 3)=1263\, X_{B}\, \vec z$
	
	$\vec M_A(P \rightarrow 3) = \vec 0 + \ovr{MA}\wedge (-P \,\vec y)=-2940\, P\, \vec z$

PFS en Moments sur $(A,\vec z)$
:	\ 
	
	$-1263 \, X_{B} - 2940 \, P = 0 \Leftrightarrow$ $X_{B}=2,33\,P= 4660$ daN
	


\begin{center}
	\fbox{$X_B=-4660$ daN  \qquad $Y_B=+1410$ daN   \qquad $X_A=+4660$ daN  \qquad $Y_A=+590$ daN	}
\end{center}


\question{On isole $\Sigma$=(1+2+3+6)}

$\{0^{Rot} \rightarrow \Sigma\}=\tc{F}{ X_{F} & - \\ Y_{F} & -\\ - &0}$  \quad 
$\{0^{LA} \rightarrow \Sigma\}=\tc{E}{ X_{E} & - \\ 0 & -\\ - &0}$  \quad
$\{P \rightarrow \Sigma\}=\tc{M}{ 0 & - \\ - P & -\\ - &0}$


PFS en résultante
:	\ 
	
	sur $\vec x$ : $X_{F}+X_{E}=0$
	
	sur $\vec Y$ : $Y_{F}-P=0 \Leftrightarrow Y_{F}=+P=2000$ daN
	
Calcul des moments en F
:	\ 
	
	$\ovr{M_F}(P \rightarrow \Sigma)=-3180 \,P\,\vec z$
	
	$\ovr{ M_F}(0^{LA} \rightarrow \Sigma)=-2020\,X_{E}\,\vec z_{}$
	
	
PFS en Moment sur $(F,\vec z)$
:	\ 
	
	$-2020\,X_{E}-3180 \,P=0 \Leftrightarrow X_{E}=1,57 \, P$


\begin{center}
	\fbox{$X_{E}=-3150$ daN  \qquad $X_{F}=3150$ daN  \qquad $Y_{F}=2000$ daN}
\end{center}

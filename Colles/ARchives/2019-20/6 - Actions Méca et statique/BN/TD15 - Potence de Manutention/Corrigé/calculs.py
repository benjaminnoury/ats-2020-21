
#Q1

# YB/XB
yb=-1000/(240+2940+570-450) 

print("YB/XB={:2g}".format(yb))

#q2

rxb=(570+2940)*yb-200

print("M_A(2 sur 3) = {:2g} Xb z".format(rxb))

P=20000
XB=2940/(rxb)*P

print("XB=%3.3G N, YB=%4.3G, XA=%3.3G, YA=%3.3G" % (XB/10,yb*XB/10,-XB,-yb*XB+P))

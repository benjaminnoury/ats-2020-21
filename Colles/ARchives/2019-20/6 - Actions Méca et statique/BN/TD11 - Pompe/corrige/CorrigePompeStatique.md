---
titre: Corrigé Mécanisme de pompe Volumétrique
type: Corrigé
classe: TSI 1
chapitre: Statique
#question-section: 1
#document: twocolumn,landscape
---

\question{En déduire le degré d'hyperstatisme de la pompe par les 2 méthodes vues en cours.}

Méthode Statique

:	$h = I_{s} - E_{s} + m = 10 - 12 + 2 = 0$

	- $I_{s}$ nombre d'inconnues statiques : 4 (Pivot Glissant) + 1 (Ponctuelle) + 5 (Pivot) = 10
	- $E_{s}$ = 6 (nb Pièce - 1) = 6 *(3-1) = 12
	- m = nombre de mouvements indépendants 2 : loi Entrée sortie (rotation moteur $\rightarrow$ translation du piston) et Rotation propre du piston (3)

Méthode cinématique 

:	$h=E_{c} - I_{c} + m = 6 - 8 + 2 = 0$

	- $E_{c}$ nombre d'équations en statique : 6 x nombre de boucle = 6 x 1 = 6
	- $I_{c}$ nombre d'inconnues cinématique : 2 (Pivot Glissant) + 5 (Ponctuelle) + 1 (Pivot) = 8
	-  m=2
	
\question{Isoler l'ensemble \{2+4\}, appliquer le PFS et en déduire les équations de l'équilibre statique de \{2+4\}}

Ne pas oublier l'hypothèse *problème plan*

\begin{multicols}{2}
	\image{img/grapheAM.pdf}
	
	$\{ 1 \rightarrow 24\} = \tc[R_{1}]{O_{1}}{ X_{O_1} & - \\ Y_{O_{1}} & -\\ - & 0 }$
	 
	$\{ C_{m} \rightarrow 24\} = \tc[R_{1}]{*}{ 0 & - \\ 0 & -\\ - & C_{m} }$ 
	
	$\{ 3 \rightarrow 24\} = \tc[R_{1}]{B}{ 0 & - \\ Y_{B} & -\\ - & 0 }$
\end{multicols}

**Application du PFS**

En résultante :

- sur $\vec x_{1}$ : \fbox{$X_{O_1}= 0$}
- sur $\vec y_{1}$ : $Y_{O_1}+Y_{B}= 0$ 

Calcul du moment en $O_{1}$  de $(3 \rightarrow 24)$ :

$\vec M_{O_{1}} (3 \rightarrow 24)=\vec 0 + \ovr{O_{1}B} \wedge Y_{B} \vec y_{1} = (-e \vec y_{2} -e	y_{1}) \wedge Y_{B}\vec y_{1} = e Y_{B} \sin \alpha \vec z_{1}$ 

PFS en moment en $O_{1}$ :

- Sur $\vec z_{1}$ :  $C_{m} + e Y_{B} \sin \alpha = 0$ $\displaystyle \Rightarrow \boxed{Y_{B} = - \frac{C_{m}}{e \sin \alpha}} \Rightarrow \boxed{Y_{O_{1}} = + \frac{C_{m}}{e \sin \alpha}}$ 


\question{Isolement de \{3\}:}

1. Par une **rapide** fermeture géométrique, déterminer la distance CB en fonction de e et $\alpha$

$$\begin{aligned}
\ovr{O_{1}A} + \ovr{AB} + \ovr{BC} + \ovr{CO_{1}} = \vec 0\\
- e \vec y_{2} - r \vec y_{1} - x \vec x_{1} + h \vec y_{1}  =\vec 0
\end{aligned}
$$

On cherche $x$, on va donc projeter l'équation vectorielle sur $\vec x_{1}$, ce qui donne :

$$
-x + e \sin \alpha = 0 \Rightarrow \boxed{x = e \sin \alpha}
$$

\newpage

2. Isoler le solide \{3\}, applique le PFS et en déduire les équations de l'équilibre statique de  \{3\}

\begin{multicols}{2}
	\image{img/grapheAM3.pdf}
	
	$\{ 24 \rightarrow 3 \} =-\{ 3 \rightarrow 24\} = \tc[R_{1}]{B}{ 0 & - \\ \frac{C_{m}}{e \sin \alpha} & -\\ - & 0 }$
	
	$\{ F_{p} \rightarrow 3 \}  = \tc[R_{1}]{D}{ 0 & - \\ F_{p} & -\\ - & 0 }$
	
	$\{ 1 \rightarrow 3 \} == \tc[R_{1}]{D}{ X_{D} & - \\ 0 & -\\ - & N_{D} }$
	
\end{multicols}


**Application du PFS**

En résultante :

- sur $\vec x_{1}$ : \fbox{$X_{D} = 0$}
- sur $\vec y_{1}$ : $F_{p} + \frac{C_{m}}{e \sin \alpha} = 0 \Rightarrow  \boxed{C_{m}=- F_{p} e \sin \alpha}$

Calcul des moments en D : 

$\vec M_{D} (24 \rightarrow 3)=\vec 0 + \ovr{DB} \wedge Y_{B} \vec y_{1} = (-CB \vec x_{1} -CD	y_{1}) \wedge Y_{B}\vec y_{1} = - CB Y_{B} \vec z_{1} = -e \sin \alpha \frac{C_{m}}{e \sin \alpha} = -C_{m}$ 

*Remarque* : On ne peut pas calculer la distance $CD$ mais celle-ci n'intervient pas dans les calculs.

PFS en moment en $D$ :

- Sur $\vec z_{1}$ :  $-C_{m} + N_{D} \rightarrow \boxed{N_{D}=C_{m}}$ 


\question{Résoudre les équations obtenues.

Déterminer les différentes actions de liaisons ainsi que le couple $C_{m}$ en fonction de $F_{p}$, e et $\alpha$.

En déduire les valeurs maximales des actions de liaisons et du couple $C_{m}$.}

Après résolution :  

$Y_{B}=F_{p}$; $Y_{O1}=-F_{p}$; $N_{D}=C_{m}=F_{p} e \sin \alpha$; $X_{O_{1}}=X_{D}=0$

La valeur max de $C_{m}$ est obtenue pour $\alpha = \pm \frac{\pi}{2}$ : \fbox{$|C_{m}^{max}|=e F_{p}$}

\question{Calculer l'effort $F_{p}$ et en déduire le couple moteur minimum nécessaire pour assurer le bon fonctionnement.}

$$F_{p}= P. S = P . \pi \frac{d^{2}}{4} =15 .\pi. \frac{16^{2}}{4}=3020 ~ N$$

*Attention aux unités :* 1 bar = 10^5^ Pa = 10^5^ N/m^2^ = 0,1 N/mm^2^

d'où $C_{m} = F_{p} . e = 3020 \times 4.10^{-3} = 12$ N.m

\question{En déduire le couple moteur nécessaire.}

$C_{m}'=1,2 \times C_{m} = 14,5$ N.m

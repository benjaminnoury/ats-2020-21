
\image[.7]{img/pompe2.pdf}

Le schéma ci-contre représente un mécanisme de transformation de mouvement utilisé pour une pompe volumétrique dont le piston\bb{(3)} est animé d’un mouvement de translation alternative.

L’arbre d’entrée \bb{(2)} possède une partie excentrée en A sur laquelle est monté un galet \bb(4) destiné à réduire les effets du frottement au niveau du contact en B.

Les dimensions caractéristiques du
mécanisme sont :
$O_{1}A$=e, AB=r et les paramètres géométriques variables sont les
grandeurs algébriques $(\vec x_1, \vec x_2) = \alpha$  et $O_{1}C$=h($\alpha$) .


L’arbre moteur est soumis à un couple $\vec C_m=C_m \vec z_{1}$ et le piston a un effort $\vec F_{p} = F_{p} \vec y_{1}$ en D du à  la pression du fluide.

\newpage

Hypothèses : 

- On suppose le problème plan,
- Les liaisons sans frottement
- on néglige les effets de l'inertie et de la  pesanteur
- On considérera 2 et 4 dans la même classe d'équivalence cinématique ( comme une seule et même pièce )




**Objectif de l'étude**

- Déterminer le couple moteur nécessaire en fonctionnement, afin de choisir le moteur actionnant la pompe.
- Déterminer les actions dans les liaisons, afin de dimensionner les composants les réalisant (Roulements, paliers lisses, ...)

# Etude de la modélisation du mécanisme

On donne le graphe des actions mécaniques suivant :

\image[.6]{img/grapheAM.pdf}

\question{En déduire le degré d'hyperstatisme de la pompe par les 2 méthodes vues en cours.}


# Relation entrée-sortie en Effort


\question{Isoler l'ensemble \{2+4\}, appliquer le PFS et en déduire les équations de l'équilibre statique de \{2+4\}}


\question{Isolement de \{3\}:
\begin{enumerate}
	\item Par une \bb{rapide} fermeture géométrique, déterminer la distance CB en fonction de e et $\alpha${}
	\item Isoler le solide \{3\}, applique le PFS et en déduire les équations de l'équilibre statique de  \{3\}
\end{enumerate}
}

\question{Résoudre les équations obtenues.

Déterminer les différentes actions de liaisons ainsi que le couple $C_{m}$ en fonction de $F_{p}$, e et $\alpha$.

En déduire les valeurs maximales des actions de liaisons et du couple $C_{m}$.}

On donne :

- excentration e=4mm
- Diamètre du piston : 16mm
- pression d'alimentation : 150 bars

\question{Calculer l'effort $F_{p}$ et en déduire le couple moteur minimum nécessaire pour assurer le bon fonctionnement.}
On préconise un coefficient de sécurité de 1.2 afin de prendre en compte le frottement dans les liaisons (négligé dans les calculs);
\question{En déduire le couple moteur nécessaire. }

---
titre:  Corrigé - Transmission par came	
classe: TSI2
type: TD
chapitre: Statique
header-includes :
---

\mktitre

\question{

$h= I_{s} - E_{s}+m = 17 - 6 \times 3 +1 = 0$
\image[.5]{img/grapheLiaisons.pdf}

}

\question{On part de la donnée $\vec F$ et on veut déterminer $C_{m}$}

+ On isole 4

\begin{multicols}{2}
\image{img/grapheLiaisons4.pdf}
    
On veut déterminer \{3 $\rightarrow$ 4 \} mais pas \{1 $\rightarrow$ 4 \} qui ne sert à rien pour déterminer $C_m$.
    
On écrit donc le Principe Fondamental de la Statique \underline{en résultante en projection sur $\vec y_1$}
       
\end{multicols}

+ test


    tests
    te
    tetete
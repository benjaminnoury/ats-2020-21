---
titre: 
type: Corrigé
classe: 
chapitre: Robot d'inspection (Statique)
#questimon-section: 1
#document: twocolumn,landscape
---

\question{}

$\{P_{A} \rightarrow \text{Robot}\}=\tc{I_{A}}{N\,\vec x_{0}+T \,\vec y_{0}\\\vec 0}$  \quad 
$\{P_{B} \rightarrow \text{Robot}\}=\tc{I_{B}}{N\,\vec x_{0}+T \,\vec y_{0}\\\vec 0}$ 

$\{P_{CD} \rightarrow \text{Robot}\}=\tc{I_{CD}}{-2\,N\,\vec x_{0}+T_{CD} \,\vec y_{0}\\\vec 0}$


\question{}

PFS en résultante sur $\vec y_{0}$ : 
:	$T_{CD}+2\,T - P = 0$

Calcul des moment en $I_{CD}$
:	\ 
	
	- $\ovr{M_{I_{CD}}}(P_{A}\rightarrow \text{Robot}) = (-(a+c) T - b\, N) \vec z_{0}$
	- $\ovr{M_{I_{CD}}}(P_{B}\rightarrow \text{Robot}) = (-(a+c) T + b\, N) \vec z_{0}$
	- $\ovr{M_{I_{CD}}}(\text{Pes} \rightarrow \text{Robot}) = c\,P\,\vec z_{0}$

PFS en Moment sur ($I_{CD},\vec z_{0}$) :
:	$-2 (a+c) T + c\,P=0 \Rightarrow \boxed{T=\displaystyle \frac{c}{2(a+c)}\,P}$ d'où $\boxed{T_{CD}=\displaystyle \frac{a}{a+c}\,P}$


\question{Application Numérique \quad $T=405$ N \quad $T_{CD}=990$ N}


\question{On doit vérifier : 

$ T \leq f_{0} \, N = 500$ N $\rightarrow $ OK

$ T_{CD} \leq f_{0} \, 2\,N = 1000$ N $\rightarrow $ OK

}

\question{On doit vérifier : $C_{R} \leq 160\times 0,35 = 56$ N.m ; or \ $C_{R}= | T \times r |=36$ N.m $\rightarrow $ OK
}

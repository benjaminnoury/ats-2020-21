---
titre: Robot d'inspection
type: TD
classe: TSI 1
chapitre: Statique (avec frottement)
#question-section: 1
#document: twocolumn,landscape
---

L'étude porte sur un robot d'inspection de cuves industrielles.

\image[.7]{img/fig1.png}


Le robot MIR est un véhicule motorisé composé d’un châssis tubulaire, de quatre bras articulés et des composants nécessaires à la mise en œuvre des contrôles et mesure. La masse est d’environ 180 kg.


Problématique:
:	Vérifier les conditions d’adhérence et de maintien du robot contre les
parois.

	Pour que l'engin soit en équilibre dans toutes les positions dans l'inter-cuve dans la zone verticale, il est nécessaire que la résultante des forces de frottement soit supérieure au poids de l'engin (1800 N). Les réactions des parois dépendent d'une part des efforts de plaquage appliqués sur les roues et d'autre part du coefficient de frottement entre roues et parois.


\vfill


**Etude de l’équilibre du robot en position verticale**


\begin{multicols}{2}
	\image{img/fig2.png}
	
	Les quatre roues de l’engin sont en contact avec la paroi en I$_{A}$, I$_{B}$ et I$_{CD}$.
	
	Les vérins de suspension exercent sur les roues des forces de plaquage.

\vfill

La symétrie permet de considérer le problème comme plan. En conséquence,
les roues C et D seront considérées comme confondues (soit I$_{CD}$ le point de contact des deux roues confondues avec la paroi) dans le même plan radial des cuves que les roues A et B.

On néglige les actions exercées par le filin de traction et par l’ombilical sur le robot.
	
	\vfill
	
	Soit $R_{0} (G,\vec x_{0},\vec y_{0},\vec z_{0})$ un repère fixe avec G le centre de gravité du robot (roues comprises)
	
	$\ovr{GI_{A}}=-a \,\vec x_{0} + b\, \vec y_{0}$
	
	$\ovr{GI_{B}}=-a\, \vec x_{0} - b \,\vec y_{0}$
	
	$\ovr{GI_{C}}=c \,\vec x_{0}$
	
	a,b,c sont des longueurs supposées constantes
	
	
	
L'effort de plaquage N est connu. Le coefficient d'adhérence est noté f$_{0}$ 
\vfill
\end{multicols}


\question{Écrire les torseurs d'actions mécaniques entre le robot et la paroi en I$_{A}$, I$_{B}$ et I$_{CD}$} 

\question{En supposant que les actions en I$_{B}$ et I$_{A}$ sont égales, calculer T et T$_{CD}$}

\question{Effectuer l’application numérique avec :
N = 1000 N, P = 1800 N, a = 385 mm, b = 380 mm, c = 315 mm}

\question{Le constructeur garantit pour le coefficient d'adhérence, entre les roues et la paroi une valeur égale à 0,5. Compte tenu des résultats précédents, indiquer si cette valeur de $f_{0}$
est suffisante.}





\question{Le rayon des roues est $r=90$ mm.

En formulant l’hypothèse d’une chaîne de transmission sans perte, déterminer le couple $C_{roueA}$ que doit exercer le motoréducteur sur la roue A pour maintenir le robot en équilibre dans la position verticale. Conclure sur la réalisation du critère de la fonction Ft213.}


Fonction | Nom | Critères | Niveaux
---------|------|----------|---------|
| Fs2 	| Avancer entre les cuves à vitesse			| Vitesse de déplacement 	| 6 mm/s $\pm$ 1mm/s
|   	| déterminée 								|  							|  
| Ft111 | Limiter la vitesse d’impact du mini 		|  Vitesse de contact 		| 0,2 m/s
|  		| bac d’inspection sur la paroi 			|
| Ft112 | Maintenir un effort presseur suffisant	| Effort normal  			| 400 N
|  		| sur le mini bac							|
| Ft12 	| Déplacer le transducteur à vitesse 		| Vitesse d’inspection 		| 5 mm/s $\pm$ 0.5 mm/s
|  		| constante
| Ft213 | Exercer un couple suffisant sur les 		| Moteur courant continu  	| Couple moteur maxi 0,35 N.m
|  		| roues 									| avec réducteur 			| Réduction 1/160 

---
titre: 
type: Corrigé Montage Renvoi d'angle
classe: Statique et Roulements
chapitre:  
#question-section: 1
document: twocolumn,landscape,A3paper
---


\question{\dotfill\emph{1 point}}
\image[.5]{img/grapheLiaisons}


\question{\dotfill\emph{1 point}
	\begin{multicols}{2}
		Par la méthode Statique: $h=I_{S}-E_{S}+m$
		\begin{itemize}
			\item m=1 (loi entrée-sortie)
			\item $E_{S}=6 \times (3-1)=12$
			\item $I_{S}=5+1+3+2=11$
		\end{itemize}
		h=0
		\vfill{}
		
		Par la méthode Cinématique: $h=E_{C}-I_{C}+m$
		\begin{itemize}
			\item m=1
			\item $E_{C}=6\times 2=12$
			\item $I_{C}=1+5+3+4=13$
		\end{itemize}
		
		h=0
		\vfill
	\end{multicols}
}

\question{\dotfill\emph{1 point}}
$$
\{\text{Courroie}\rightarrow 2\}=\tc[R_{1}]{C}{ 0 & -50 \\ -490 & 0\\ 0 &0} \qquad 
\{4 \rightarrow 2 \} = \tc[R_{1}]{D}{ 220 & 0 \\ 661 & 0\\ -2000 &0}
$$
$$
\{1^{R}\rightarrow 2\}=\tc[R_{1}]{A}{X_{A}& 0\\Y_{A}&0\\Z_{A}&0} \qquad
\{1^{LA}\rightarrow 2\}=\tc[R_{1}]{B}{0& 0\\Y_{B}&0\\Z_{B}&0} 
$$

\question{\dotfill\emph{1 point}}

En appliquant le Principe Fondamental de la statique en résultante, on obtient :

- $X_{A} +220 = 0$
- $Y_{A} +Y_{B} +171 = 0$ \qquad (en N) 
- $Z_{A} +Z_{B} -2000 = 0$

\newpage
\question{\dotfill\emph{4 points}}

- $\ovr{M_A}(\text{Courroie}\rightarrow 2)=\vc{-50\\0\\0}+\vc{150.10^{-3}\\0\\0}\wedge \vc{0\\-490\\0}=\vc{-50\\0\\-73,5}$
- $\ovr{M_A}(4\rightarrow 2)=\vc{0\\0\\0}+\vc{-50.10^{-3}\\-25.10^{-3}\\0}\wedge \vc{220\\661\\-2000}=\vc{50\\-100\\-27,55}$ \qquad{\emph{en N et en m}}
- $\ovr{M_A}(1^{LA}\rightarrow 2)=\vc{0\\0\\0}+\vc{0,1\\0\\0}\wedge \vc{0\\Y_{B}\\Z_{B}}=\vc{0\\-0,1 \cdot Z_{B}\\0,1 \cdot Y_{B}}$

En appliquant le Principe Fondamental de la statique en Moment au point A, on obtient :

- $50 - 50 =0$
- $-100 -0,1 \cdot Z_{B}=0$
- $-101,05 +0,1 \cdot Y_{B}=0$

\question{\dotfill\emph{1 point}}
$$
\{1^{R}\rightarrow 2\}=\tc[R_{1}]{A}{-220& 0\\-1182&0\\3000&0} \qquad
\{1^{LA}\rightarrow 2\}=\tc[R_{1}]{B}{0& 0\\1011&0\\-1000&0} 
\qquad\text{(en N)}
$$

\question{\dotfill}

L'axe de rotation est $(A,\vec x_{1})$

\ 

Pour le roulement A:
\begin{itemize}
	\item Charge axiale : $F^{A}_{A}=|X_{A}|=220 N$
	\item Charge Radiale :$F^{A}_{R}=\sqrt{{Y_{A}}^{2}+{Z_{A}}^{2}}=3225$ N
\end{itemize}

\ 

Pour le roulement B:
\begin{itemize}
	\item Charge axiale : $F^{B}_{A}=0 N$
	\item Charge Radiale :$F^{B}_{R}=\sqrt{{Y_{B}}^{2}+{Z_{B}}^{2}}=1422$ N
\end{itemize}


\newpage
\question{\dotfill}

Le roulement A est le plus chargé, c'est celui-là qui déterminera la durée de vie de l'ensemble.

\image{img/TableauSNR}

Calcul de la charge équivalente P :
:	\ 

	$\displaystyle \frac{F_{A}}{C_{0}}=\frac{220}{24\,000}=0,009 \leq 0,014\Rightarrow e=0,19$
	
	$\displaystyle \frac{F_{A}}{F_{R}}=0,068 \leq e \Rightarrow P=F_{R}=3\,225$ N


Calcul de la durée de vie :
:	$L_{10}=\displaystyle \left (\frac{C}{P} \right )^{n}=\left ( \frac{19\,900}{3\,325}\right )^{3}=214.10^{6}$  tr

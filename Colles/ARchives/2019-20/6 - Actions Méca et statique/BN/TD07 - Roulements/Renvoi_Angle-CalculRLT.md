---
titre: Montage de Renvoi d'angle
type: TD
classe: TSI 1
chapitre: Statique + Calcul de Roulements
#question-section: 1
#document: twocolumn,landscape
#marges: 1
---


\renewcommand{\question}{\addtocounter{ques}{1}\theques) }

# Montage de renvoi d'angle

Un pignon (engrenage) d'entrée (2) d'une boite de vitesse de machine-outil est en liaison pivot avec le carter (1) de cette dernière. Cette liaison est composée de (2) roulements à billes à contact radial situés en A et B. 

Une poulie motrice (3) est encastrée par clavetage en C sur l'arbre (2) (sur le schéma, on considère que (2) et (3) sont la même pièce).

Le pignon conique (2) transmet la puissance motrice au pignon (4). La résultante des actions de contact entre les 2 pignons s'applique en D. On représentera le contact (4--2) par une liaison ponctuelle de normale $(D,\vec z_{1})$ dont le torseur est donné ci-dessous.

La puissance transmise est de 3500 W à une fréquence de rotation de 1450 tours/min. Les actions de liaison en C et D dues à la transmission de puissance sont définies par les torseurs suivants :

$$
\tam{\text{Courroie}\to 2}=\tc[B_{1}]{C}{ 0 & -50 \\ -490 & 0\\ 0 &0} \qquad 
\tam{4 \to 2 } = \tc[B_{1}]{D}{ 220 & 0 \\ 661 & 0\\ -2000 &0} \qquad \text{(en N et en m)}
$$

![](img/schema-RA.png)

Les diverses positions sont repérées par a=50 mm, b=100mm, c=50mm,  O$_{1}$E=65 mm, ED= 25 mm.

L'arbre est en rotation uniforme. 

On ne tient pas compte des effets d'inertie, de la pesanteur et on suppose les liaisons parfaites.

Le roulement en A est modélisé par une liaison rotule et celui en B par une liaison Linéaire Annulaire.

Q\question
:	Représenter le graphe des Actions Mécaniques correspondant au modèle proposé.


Q\question
:	Calculer le degré d'hyperstatisme du système suivant les 2 méthodes vues en cours


Q\question
:	Isoler l'abre (2). Faire le bilan des actions mécaniques

Q\question
:	Écrire le système d'équations résultant de l'application, au solide (2), du principe fondamental de la statique en résultante.

Q\question
:	Écrire le système d'équations résultant de l'application, au solide (2), du principe fondamental de la statique en moments au point A.

Q\question
:	Résoudre le système des 6 équations obtenues aux questions précédentes. En déduire les actions mécaniques dans les roulements en A et B. Faire l'application numérique 


<!--
Les roulements choisis sont tous deux des 61820-2RZ vendus par SKF décrits ci-dessous.


![](img/skf-RA.png)

Q\question
:	Déduire de la question précédente les actions axiales et radiales dans chaque roulement.

\question{Calculer la durée de vie de l'ensemble}
-->

---
type: Corrigé TD à montage automatisée.
classe: ATS
#CI:
#competence: 
#question-section: 1
#ficheTD: 1
document: twocolumn,landscape
---


\question{

$\tam{sol^{A} \to grue}=\tc{A}{ Y_{A} \vec y \\ \vec 0}=\tc{B}{ Y_{A} \vec y \\ -3,6 \cdot Y_{A} \vec z_{}}$

$\tam{sol^{B} \to grue}=\tc{B}{ Y_{B} \vec y \\ \vec 0}$

$\tam{P1 \to grue}=\tc{G_{1}}{ -P_{1} \vec y \\ \vec 0}=\tc{B}{ P_{1} \vec y \\ 0,9 P_{1} \vec z}$

$\tam{P2 \to grue}=\tc{G_{2}}{ - P_{2} \vec y \\ \vec 0 }=\tc{B}{ - P_{2} \vec y \\ 3 P_{2} \vec z}$

$\tam{C \to grue}=\tc{C}{ -C \vec y \\\vec 0}=\tc{B}{ -C \vec y \\ - c C \vec z}$


\subsubsection{Application du PFD}

En résultante sur $\vec y$ : \quad
$Y_{A} + Y_{B} - P_{1} - P_{2} - C = 0$

En moment sur $(B,\vec z)$ : \quad
$-3,6 Y_{A} + 0,9 \, P_{1} + 3 \, P_{2} -c \, C=0$


\subsubsection{Résolution :}

$Y_{A}=\dfrac{ 0,9 \, P_{1} + 3 \, P_{2} -c \, C}{3,6}$

\ 

$Y_{B}= P_{1} + P_{2} + C -Y_{A} = \dfrac{3}{4} \, P_{1} + \dfrac{1}{6} \, P_{2} + \left (1 + \dfrac{c}{3,6} \right ) C $
}

\question{Lorsqu'il y a basculement, il n'y a plus de contact en A, donc  $Y_{A}=0$, soit :
$$
 0,9 \, P_{1} + 3 \, P_{2} -c \, C =0
$$
$$
C = \frac{1}{c} \, \left (  0,9 \, P_{1} + 3 \, P_{2}\right )
$$
}

\question{Pour $c=10,4$ m,

$$
C=\frac{ 0,9 \times 93\,200 + 3 \times 9,81 \times 8 \times 900 }{10,4} = 28\,440 \text{ N}
$$


Le constructeur conseille une masse maximale de 1800 kg pour c=10,4 m. Soit un coefficient de sécurité de $\frac{28\,440}{9,81 \times 1800}=1,6$
}

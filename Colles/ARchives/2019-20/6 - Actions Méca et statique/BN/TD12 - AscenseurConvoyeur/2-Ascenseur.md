

\begin{multicols}{2}
		
Le système étudié est un élément de convoyeur permettant de transporteur des objets entre poste de travail. Le module étudié est un ascenseur. \newline

\image{img/convoyeur2.jpg}
\caption{Convoyeur}
\image[.9]{img/SchCineO.png}
\caption{Modèle Cinématique de l'ascenseur}
\end{multicols}


**Objectif:** Déterminer le couple $C_{m}$ minimum pour pourvoir soulever un colis de 10 kg
\medskip{}

Hypothèses et données :

- Le problème est plan
- g = 9,81 N/kg
- On néglige les masses des composants devant celle du colis.
- pas de la vis : p = 6 mm/tour


On donne le torseur $\tam{10 \rightarrow 11} = \tc{A}{ X_{A} & L_{A} \\ Y_{A} & M_{A}\\ Z_{A} & N_{A}}$, avec $M_{A}=\frac{p}{2 \pi} Y_{A}$ 

A appartenant à l'axe de la liaison hélicoîdale

\question{Tracez le graphe d'analyse des actions mécaniques correspondant au modèle proposé.}
\question{Déterminez la liaison équivalente au 2 liaisons pivots glissants entre (11) et (0). Il n'est pas obligatoire de développer les calculs mais la liaison obtenue doit être entièrement caractérisée (par son centre et/ou son axe et/ou sa direction)}
\question{Calculez, par la méthode de votre choix, l'hyperstatisme du modèle proposé.}

\question{Est-il possible de calculer toutes les actions mécaniques dans chacune des liaisons ? Justifier votre réponse.}


\question{Peut-on résoudre le problème (cf Objectif) par un seul isolement? Justifier votre réponse.}


\question{Isoler le solide 11. Faire le Bilan des Actions Mécaniques Extérieures et déterminer $Y_{A}$ en fonction de la force $F$}

\question{Isoler le solide 10. Faire le Bilan des actions mécaniques et déterminer $M_{A}$ en fonction de $C_{m}$}

\question{Déduire de vos résultats $C_{m}$ en fonction de $F$. Faire l'application numérique ($F$ représente le poids du colis).}

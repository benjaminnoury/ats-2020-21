\question{Tracez le graphe d'analyse des actions mécaniques correspondant au modèle proposé.}

\imageT{img/grapheAM.pdf}

\question{Déterminez la liaison équivalente au 2 liaisons pivots glissants entre (11) et (0). Il n'est pas obligatoire de développer les calculs mais la liaison obtenue doit être entièrement caractérisée (par son centre et/ou son axe et/ou sa direction)}

C'est une glissière de direction $\vec y_{0}$

\question{Calculez, par la méthode de votre choix, l'hyperstatisme du modèle proposé.}

Par la méthode Statique :

:	$h= I_{s} - E_{s} + m = 18 - 6 \times2 + 1 = 7$

Par la méthode Cinématique :

:	$h= E_{c} - I_{c} + m =  6\times2 - 6 +1=7$

\question{Est-il possible de calculer toutes les actions mécaniques dans chacune des liaisons ? Justifier votre réponse.}

Ce n'est pas possible car le système est hyperstatique.


\question{Peut-on résoudre le problème (cf Objectif) par un seul isolement? Justifier votre réponse.}

Il n'y a aucun isolement faisant apparaitre 6 au plus inconnues. On ne peut donc pas résoudre en un seul isolement. On procédera à des résolutions partielles.


\question{Isoler le solide 11. Faire le Bilan des Actions Mécaniques Extérieures et déterminer $Y_{A}$ en fonction de la force $F$}

\begin{multicols}{2}
	\imageT{img/graphe11.pdf}
	
	$\{10 \rightarrow 11\} = \tc{A}{ X_{A} & L_{A} \\ Y_{A} & M_{A}\\ Z_{A} & N_{A}}$
	
	$\{0 \rightarrow 11^{B}\} = \tc{B}{ X_{B} & L_{B} \\ 0 & 0\\ Z_{B} & N_{B}}$
	
	$\{0 \rightarrow 11^{C}\} = \tc{C}{ X_{C} & L_{C} \\ 0 & 0\\ Z_{C} & N_{C}}$
	
	$\{F \rightarrow 11\} = \tc{A}{ 0 & 0 \\ F & 0\\ 0 & 0}$
	
\end{multicols}

On cherche juste à connaitre $Y_{A}$ en fonction de $F$, on peut le faire avec une seule équation : le PFS en résultante en projection sur $\vec y_{0}$.

On obtient: $Y_{A}+F=0 \Rightarrow \boxed{Y_{A}=-F}$

\newpage
\question{Isoler le solide 10. Faire le Bilan des actions mécaniques et déterminer $M_{A}$ en fonction de $C_{m}$}

\begin{multicols}{2}
	\image[.7]{img/graphe10.pdf}
	
	$\{11 \rightarrow 10\} = - \tc{A}{ X_{A} & L_{A} \\ Y_{A} & M_{A}\\ Z_{A} & N_{A}}$
	
	$\{0 \rightarrow 10\} = \tc{O}{ X_{O} & L_{O} \\ Y_{O} & 0\\ Z_{O} & N_{O}}$
	
	$\{C_{m} \rightarrow 11\} = \tc{*}{ 0 & 0 \\ 0 & C_{m}\\ 0 & 0}$
	
\end{multicols}

Pour déterminer $C_{m}$ en fonction de $M_{A}$ on utilisera le PFS en moment au point A en projection sur $\vec y_{0}$.

On a besoin de calculer $\vec y_{0}.\vec M_{A}(0 \rightarrow 10)$

$$\begin{aligned}
\vec y_{0}.\vec M_{A}(0 \rightarrow 10) &= \vec y_{0}. \left ( \vec M_{O}(0 \rightarrow 10) - AO \vec y_{0} \wedge \vec R(0 \rightarrow 10) \right )\\
& = \vec y_{0}.\vec M_{O}(0 \rightarrow 10) - \vec y_{0} . \left ( AO \vec y_{0} \wedge \vec R(0 \rightarrow 10) \right )\\
& = \vec y_{0}.\vc{L_{O}\\0\\N_{O}} - \vec y_{0} . \underbrace{\left ( AO \vec y_{0} \wedge \vec R(0 \rightarrow 10) \right )}_{\perp y_{0}} = \boxed{\vec 0}
\end{aligned}$$

On obtient donc : $- M_{A} +C_{m} = 0 \Rightarrow \boxed{M_{A}=C_{m}}$

\question{Déduire de vos résultats $C_{m}$ en fonction de $F$. Faire l'application numérique ($F$ représente le poids du colis).}

On a les 3 équations suivantes : $\displaystyle \left \{ 
\begin{array}{l}
	M_{A}=C_{m} \\
	Y_{A}=-F \\
	M_{A} = -\frac{p}{2 \pi} Y_{A} \text{ (sujet)}
\end{array} \right .$
Ce qui donne : \fbox{$\displaystyle C_{m}=\frac{p}{2 \pi}F$}

*Application Numérique*

\fbox{$\displaystyle C_{m} = \frac{6.10^{-3}}{2 \pi} 10*9,81= 94.10^{-3}$ N.m}

---
titre: Nacelle Télescopique
type: TD
classe: TSI 2
chapitre: Dynamique
#question-section: 1
#document: twocolumn,landscape
---

\begin{multicols}{2}
	Les nacelles sont utilisées pour effectuer des tra-
vaux en hauteur et éviter l’utilisation d’échafaudages.
Il existe quatre principales familles de nacelles adaptées
à des usages spécifiques

\begin{itemize}
	\item nacelles à flèche articulée,
	\item nacelles à flèche télescopique,
	\item nacelles à mat,
	\item nacelles à ciseaux.
\end{itemize}

Ces systèmes sont autopropulsés et permettent donc à l’opérateur placé dans le panier de commander non seulement la hauteur, le pivotement, mais également de piloter la translation de la nacelle pour atteindre les zones de travail.

L’étude proposée portera sur une nacelle de type flèche télescopique


\image{img/nacelle.png}
\end{multicols}


L’énergie nécessaire est fournie principalement par un moteur thermique qui entraine un groupe hydraulique. Un groupe électropompe de secours est également disponible en cas d’incident. Les différents actionneurs utilisés sont hydrauliques avec commande proportionnelle. 

Problématique
:	L’objectif de l’étude est de déterminer la durée du relevage de la flèche ainsi que la pression dans le vérin de relevage lors de l’opération de relevage.

| Id. | Exigence |
|-----|-----------
| 1.2 | Respecter un temps de montée inférieur à 1 min 30 |
| 1.5 | Alimenter le vérin avec une pression < 60 bar |

# Données et Modèle

\image{img/schema.png}	

\begin{multicols}{3}

$\ovr{OB}=c \, \vec u_{1}$  

$\ovr{OA}=a \, \vec x_{0} -b \vec y_{0} $ 

$\ovr{AB}=\lambda \, \vec u_{3}$

$\ovr{OG}=d \, \vec u_{1}$

$\ovr{OC}=3 \, d \, \vec u_{1}$

$\ovr{CK}=e \,\vec u_{1}- f \, \vec v_{2}$

\end{multicols}

\begin{center}
	a=0,5 m \quad b=1 m \quad c=2 m \quad d=6 m \quad e=0,5 m \quad f=1 m
\end{center}

\newpage{}
\begin{multicols}{2}
	
\begin{description}
	\item[Solide 1] \ 
	
	Centre d’inertie G, 
	
	masse $M_{1}$=2\,500 kg, 
	
	moment d’inertie $I_{1}$=25\,000 kg.m$^{2}$ par rapport à l’axe $(O,\vec z_{0})$ ;
	
	\item[Solide 2] \ 
	
	Centre d’inertie K, 
	
	masse $M_{2}$=500 kg , 
	
	moment d’inertie $I_{2}$=150 kg.m$^{2}$ par rapport à l’axe $(K,\vec z_{0})$ ;
\end{description}
\end{multicols}
\begin{center}
On néglige les caractéristiques d'inertie des autres pièces, 

Accélération de la pesanteur : $\vec g = - g \, \vec y_{0}$ avec g=10 m.s$^{-2}$
	
\end{center}

\begin{multicols}{2}
	\begin{description}
		\item[Vérin de relevage] \ 

		Diamètre intérieur $D_{i}=180$ mm
	
		Diamètre extérieur $D_{e}=240$ mm
	
		Débit max : $q_{max}$ =55 L/min
	\end{description}
\image[.7]{img/verin.png}
\end{multicols}

Hypothèses
:	\ 
	
	On considère le châssis à l’arrêt sur un plan horizontal ; la tourelle est immobile. 
	
	Pendant le relevage, le vérin évolue à vitesse constante. Les mouvements de télescopage ou de pivotement du panier par rapport à la tête de flèche sont inexistants.
	
	Lors de la rotation de la flèche 1 par rapport à la tourelle 0, en phase de relevage, le mouvement de compensation est supposé parfait : $\vec u_{2} =\vec x_{0}$ 	(le panier reste horizontal).


# Étude géométrique

\question{Exprimer la longueur $\lambda$ en fonction de l'angle de relevage $\theta$}

\question{En déduire la course du vérin de relevage pour $\theta \in \left [\frac{-\pi}{12}; \frac{5 \pi}{12}\right ]$}

\question{En déduire la durée de relevage pour le débit maximum.}

\question{Conclure quant au cahier des charges.}

# Étude dynamique

\begin{floatingfigure}[r]{.33\lw}
	
\end{floatingfigure}

Les masses des solides autres que (1) et (2) sont négligées. Les seuls actionneurs considérés sont le vérin de relevage et le vérin de compensation. 

L'action du vérin de compensation sur (2) est modélisable par un glisseur de résultante $\vec R_{comp}$ et d'axe $(IJ)$.


\question{Exprimer la vitesse du centre d’inertie K du panier (2) par rapport à 0.}

\question{Exprimer l’accélération du centre d’inertie K du panier (2) par rapport à 0.}

\question{Exprimer la projection du moment dynamique en O de (2) par rapport à 0 sur $\vec z_{0}$}

On considère le système matériel $\Sigma$ constitué de (1), (2) et du vérin de compensation.


\question{Exprimer la projection du moment dynamique en O de $\Sigma$ par rapport à 0 sur  $\vec z_{0}$ en fonction des éléments d’inertie et du paramètre $\theta$}

\question{Appliquer le PFD à $\Sigma$ et en déduire l'effort de poussée $F_{verin}$ du vérin de relevage.}

Pour le chargement maximum du panier et pour la flèche sortie au maximum, on donne la courbe représentative de $F_{verin}$ lors du relevage à débit maximum.

\image[.5]{img/courbe.png}

\question{En déduire la pression maximum que nécessite le vérin.}

\question{Conclure quant au cahier des charges.}

---
titre: Robot manipulateur horizontal à axes hortogonaux
type: TD
classe: ATS
CI: 8 Transmettre l'énergie
competence: 
#question-section: 1
document: twocolumn,landscape
---
\parindent=\oldparindent
Le robot manipulateur représenté ci-dessous possède 2 degrés de liberté :

- une rotation du corps (2) par rapport au socle (1) au niveau de la liaison pivot d'axe ($O_{1},z_{1}$). La rotation est repérée par l'angle $\varphi$. Un moteur électrique solidaire de (1) anime la rotation de ce corps.
- Une translation du bras (3) par rapport à (2) au niveau de la liaison glissière de direction $\vec y_{3}$ située en A. Cette transalation est paramétrée par la distance y entre $A$ et $G_{3}$. Un vérin électrique solidaire de (2) anime la translation de ce bras. 


Le mécanisme évolue dans le plan $(O_{1},\vec x_{1},\vec y_{1})$.
Les liaisons sont parfaites. 

Le but de cet étude est d'analyser le mouvement du bras (3) par rapport à (2) sous les effets d'inertie.

\image{img/schema1}

$\ovr{O_{1}A}=a \, \vec x_{2}$ ; $\ovr{AG_{3}}=y \, \vec y_{2}$ ; $(\vec x_{1},\vec x_{2})=\varphi$. Les repère $R_{2}$ et $R_{3}$ sont parallèles.

Le bras 3 a une masse $m_{3}$ et $\I_{G_{3}}(3)=\operateur{A_{3}&0&0\\0&B_{3}&0\\0&0&C_{3}}$

\newpage
\question{Réaliser le graphe de structure.}

Dans un premier temps, on suppose que le vérin électrique placé entre 2 et 3 est absent.
\question{Isoler 3. Réaliser le bilan des actions mécaniques.}
\question{Écrire le PFD sous forme torsorielle appliqué au solide 3. ( On ne demande pas de calculer tous les éléments )}
\question{Quelle équation scalaire issue du PFD permetrait d'obtenir l'équation du mouvement de 3 par rapport à 2 ?}


La vitesse de rotation de 2 est constante.
\question{Déterminer les éléments du torseur dynamique nécessaires pour résoudre l'équation choisie ci-dessus.}
\question{Écrire cette équation différentielle}
À l'instant initial, le bras 3 est immobile par rapport au corps 2, et ce dernier est animé d'une vitesse angulaire $\dot \varphi$. 

\question{Quel va être le mouvement de 3 par rapport à 2 et à quelle condition ?}
\question{Déterminer l'équation différentielle de ce mouvement sachant qu'à t=0, y=y₀}

Le vérin électrique est cette fois présent. 

\question{Déterminer l'effort F qu'il doit exercer pour maintenir 3 fixe par rapport à 2.}

from pylab import *
np.set_printoptions(formatter={'float': '{: 1.3g}'.format})

X=0
Y=-.69e-3 # mm
Z= -3.14e-3 # mm

m = 4.82e-3 # grammes

N=9000
omega=N*math.pi/30


IG=array([[121.05,0,0],[0,121.69,-18.79],[0,-18.79,41.28]])*1e-9


IOG=array([[Y*Y+Z*Z,0,0],[0,Z*Z,-Y*Z],[0,-Y*Z,Y*Y]])

IO= IG + m * IOG
print(IO)


Omega=array([0,0,omega])
Omega.shape=(3,1)



print("Resultante Cinétique")
print("{:1.2e} x_1".format(m*Y*omega))
print("Resultante Dynamique")
print("{:1.2e} y_1".format(m*Y*omega*omega))

print("Moment Cinétique en O")
sigma=dot(IO,Omega)
print(sigma)

print("Moment Dynamique en O")

print("{:1.3e} x_1".format(sigma[1,0]*omega))
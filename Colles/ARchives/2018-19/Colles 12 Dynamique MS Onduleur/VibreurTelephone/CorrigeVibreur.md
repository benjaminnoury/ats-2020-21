---
titre: Corrigé Vibreur
classe: TSI 2
type: Corrigé TD
#question-section: 1
--- 

# Paramétrage cinématique du rotor


\question{}

- $\ovr{OG}=-r \vec y_{1}$
- $\V{G}(1/0) = r \omega_{1/0} \vec x_{1}$
- $\vec a_{G} (1/0) = r \left ( \dot \omega_{1/0} \vec x_{1} +  \omega_{1/0} ^{2} \vec y_{1} \right )$

\question{}

$\vec a_{G}(1/0) = r \omega_{1/0}^{2} \vec y_{1}$, 

$\norm{\vec a_{G}(1/0)}\  \# \ 612$ m/s$^{2} \simeq 61$ g, ce qui explique pourquoi on peut négliger la pesanteur.


# Étude cinétique du rotor test

\question{Le vibreur admet le plan (G,$\vec y,\vec z$) comme plan de symétrie, ce qui rend les produits d'inertie suivants $\vec x$ nuls.
$$
\I_{G}(1)=\operateur{Lxx & 0 & 0 \\ 0 & Lyy & Lyz \\ 0 & Lzy & Lzz}
$$}

# Petits Exercices

\setcounter{ques}{0}

\begin{multicols}{2}
Placer l’action mécanique de 2 sur 1 pendant la
phase de serrage

\image{img/frott1.png}
\end{multicols}

\begin{multicols}{2}
On relâche l’effort, l’excentrique reste coincé mais
\bb{a tendance} à se desserrer.\\
Placer l’AM de 2 sur 1

\image{img/frott2.png}
\end{multicols}

\begin{multicols}{2}
On tire sur la pièce 2, elle glisse sur 0.\\
Placer, en A, l’action élémentaire de 0 sur 2

\image{img/frott3.png}
\end{multicols}
\newpage
\begin{multicols}{2}
	On tire sur la pièce 2, elle ne glisse pas sur 0. \\	
	Placer, en A, l’action de 0 sur 2.
	
	\image{img/frott4.png}
\end{multicols}

\begin{multicols}{2}
	La pièce 2, est seulement soumise à son poids et à l’action de 0.\\
	Placer, en A, l’action de 0 sur 2.
	
	\image{img/frott5.png}
\end{multicols}

\begin{multicols}{2}
	La pièce 2, est seulement soumise à son poids et à l’action de 0. \\
	Placer, en A, l’action de 0 sur 2.\\
	A partir de quelle valeur de $\alpha$ le solide 2 va-t-il se mettre à glisser ?
	
	\image{img/frott6.png}
	
\end{multicols}

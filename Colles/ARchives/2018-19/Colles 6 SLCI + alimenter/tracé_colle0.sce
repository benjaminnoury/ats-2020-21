tm=.2
ka=80
km=.3
kc=15
kr=1e-2


t=0:.05:1.5;
wb=ka*km*kr*(1-exp(-t/tm));


clf
plot(t,wb)

plot(t,ones(size(t,1),size(t,2))*wb($)*.95,"r")


[m,ind]=min(abs(wb-.95*wb($)));

ti=[t(ind),t(ind)]
wbi=[0,wb(ind)]

plot(ti,wbi,"r")

xlabel("temps t (ms)")
ylabel("$vitesse \ \omega_b \ (rad/s)$")
title("Réponse d un axe du robot à un échelon unitaire")


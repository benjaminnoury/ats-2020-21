set table "gnuplot/Colle10-Cor/14.table"; set format "%.5f"
set samples 1800.0; set parametric; plot [t=0:2.7] [] [] log10(10**t), (t<log10(1/(.05))? 0:-90)+(t<log10(1/(.0005))? 0:-90) 

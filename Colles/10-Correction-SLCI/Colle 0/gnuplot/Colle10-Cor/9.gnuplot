set table "gnuplot/Colle10-Cor/9.table"; set format "%.5f"
set samples 500.0; set parametric; plot [t=0:1.3] [] [] log10(10**t), (t<log10(1/.002)?20*log10(30):+20*log10(30*.002)+20*log10(10**t)) 

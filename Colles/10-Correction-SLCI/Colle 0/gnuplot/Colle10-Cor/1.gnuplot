set table "gnuplot/Colle10-Cor/1.table"; set format "%.5f"
set samples 500.0; set parametric; plot [t=0:4] [] [] log10(10**t), (t<log10(1/(.05))?20*log10(1):+20*log10(1/(.05))-20*log10(10**t))+(t<log10(1/(.002))?20*log10(1):+20*log10(1/(.002))-20*log10(10**t))+(t<log10(1/(.0005))?20*log10(1):+20*log10(1/(.0005))-20*log10(10**t)); 

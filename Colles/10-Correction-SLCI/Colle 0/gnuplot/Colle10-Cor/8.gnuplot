set table "gnuplot/Colle10-Cor/8.table"; set format "%.5f"
set samples 500.0; set parametric; plot [t=0:4] [] [] log10(10**t), 20*log10(abs(1/sqrt(1+(.05*10**t)**2)))+20*log10(abs(1/sqrt(1+(.002*10**t)**2)))+20*log10(abs(1/sqrt(1+(.0005*10**t)**2))) +20*log10(abs(30*sqrt(1+(.002*10**t)**2))) 

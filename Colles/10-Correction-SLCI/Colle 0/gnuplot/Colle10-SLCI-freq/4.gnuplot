set table "gnuplot/Colle10-SLCI-freq/4.table"; set format "%.5f"
set samples 50.0; set parametric; plot [t=-1:4] [] [] log10(10**t), -180/3.1415957*atan(.05*10**t)+-180/3.1415957*atan(.002*10**t)+-180/3.1415957*atan(.0005*10**t) 

set table "gnuplot/Colle10-SLCI-freq/3.table"; set format "%.5f"
set samples 1799.0; set parametric; plot [t=-1:4] [] [] log10(10**t), (t<log10(1/(.05))? 0:-90)+(t<log10(1/(.002))? 0:-90)+(t<log10(1/(.0005))? 0:-90) 

set table "gnuplot/Colle4-S0-Corrigé/1.table"; set format "%.5f"
set samples 50.0; set parametric; plot [t=2:6] [] [] log10(10**t),20*log10(abs(1/sqrt(1+(0.00005*10**t)**2)))

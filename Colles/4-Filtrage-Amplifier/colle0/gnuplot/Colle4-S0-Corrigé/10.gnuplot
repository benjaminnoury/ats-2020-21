set table "gnuplot/Colle4-S0-Corrigé/10.table"; set format "%.5f"
set samples 400.0; set parametric; plot [t=2:6] [] [] log10(10**t),(t<log10(20000)? 0:-180)

set table "gnuplot/Colle4-S0-Corrigé/5.table"; set format "%.5f"
set samples 400.0; set parametric; plot [t=2:5.3] [] [] log10(10**t),20*log10(abs(1/sqrt((1-(10**t/20000)**2)**2+(2*.707*(10**t/20000))**2)))

set table "gnuplot/Colle4-S0-Corrigé/6.table"; set format "%.5f"
set samples 400.0; set parametric; plot [t=2:5.3] [] [] log10(10**t),(t<log10(20000)?20*log10(1):+20*log10(1)+40*log10(20000)-40*log10(10**t))

set table "gnuplot/Colle4-S0-Corrigé/11.table"; set format "%.5f"
set samples 400.0; set parametric; plot [t=2:6] [] [] log10(10**t),(t<log10(1/(0.00005))?20*log10(1):+20*log10(1/(0.00005))-20*log10(10**t))

set table "gnuplot/Colle4-filtrage-Cinématique/2.table"; set format "%.5f"
set samples 500.0; set parametric; plot [t=2:6] [] [] log10(10**t),(t<log10(1/(0.0003))?20*log10(1):+20*log10(1/(0.0003))-20*log10(10**t))

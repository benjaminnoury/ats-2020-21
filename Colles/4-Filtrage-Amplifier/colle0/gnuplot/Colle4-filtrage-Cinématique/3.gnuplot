set table "gnuplot/Colle4-filtrage-Cinématique/3.table"; set format "%.5f"
set samples 500.0; set parametric; plot [t=-2:2.5] [] [] log10(10**t),(t<log10(1/(0.5))? 0:-90)

\section{Appels de puissance réactive en monophasé}


\begin{minipage}{.58\textwidth}
On modélise le réseau monophasé par une source de tension 

$e(t)=E\cdot \sqrt{2}\cdot \sin(\omega\cdot t)$ avec $E=\SI{230}{V}$ et $\omega=100\cdot \pi\si{rd/s}$.

La ligne d'alimentation est modélisée par une réactance $X$.

On notera 
\begin{itemize}\tightlist
				\item $v(t)$ la tension d'utilisation et $V$ sa valeur efficace. 
				\item $i(t)$ le courant de ligne et $I$ sa valeur efficace.
\end{itemize}
\end{minipage}
\begin{minipage}{.3\textwidth}
\centering

\begin{circuitikz}\draw
	(0,0) to[short,i=$i(t)$] (1,0)to[L,a=$X{=}L_r\cdot\omega$](3,0) to[short,-o]++(.5,0)node[right](A){A}
	(0,-2) to[sV=$e(t)$] (0,0)
	(0,-2) to[short,-o] ++(3.5,0)node[right]{B}
	to[open,v=$v(t)$] ++(0,2) 
;\end{circuitikz}
\end{minipage}\hfill




\begin{description}
				\item[Problématique] On cherche à évaluer l'impact de l'inductance de la ligne sur la chute de tension et sur la puissance réactive.
\end{description}

\subsection{Étude en court-circuit}

On obtient un court-circuit en reliant les point A et B par un fil.

\question{Exprimer en fonction de $X$ et $E$ la valeur efficace du courant de court-circuit $I_{cc}$ et la puissance apparente $S_{cc}$ en court-circuit.}

\begin{minipage}{.38\textwidth}
\begin{circuitikz}\draw
	(0,0) to[short,i=$i(t)$] (1,0)to[L,a=$X{=}L_r\cdot\omega$](3,0) to[short]++(.5,0)node[right](A){A}
	(0,-2) to[sV=$e(t)$] (0,0)
	(0,-2) to[short] ++(3.5,0)node[right]{B}
	to[short,v=$v(t)$] ++(0,2) 
;\end{circuitikz}
\end{minipage}
\begin{minipage}{.48\textwidth}
La loi des mailles donne :

$\underline{E}-j\cdot X\cdot \underline{I_{cc}}=0\implies \underline{I_{cc}}=\dfrac{\underline{E}}{j\cdot X}\implies I_{cc}=\left| \underline{I_{cc}}  \right| = \dfrac{\left| \underline{E}  \right| }{X}=\dfrac{E}{X}$ 

d'où $S_{cc}=E\cdot I_{cc}=\dfrac{E^2}{X}$
\end{minipage}


On connecte maintenant entre A et B une charge inductive de facteur de puissance $\cos\phi$.

\question{Exprimer les puissances active $P$ et réactive $Q$ absorbées par la charge en fonction de $V$, $I$ et $\phi$.}
\begin{minipage}{.38\textwidth}
\begin{circuitikz}\draw
	(0,0) to[short,i=$i(t)$] (1,0)to[L,a=$X{=}L_r\cdot\omega$](3,0) to[short]++(.5,0)node[right](A){A}
	(0,-2) to[sV=$e(t)$] (0,0)
	(0,-2) to[short] ++(3.5,0)node[right]{B}
	to[R,v=$v(t)$] ++(0,2) 
;\end{circuitikz}
\end{minipage}
\begin{minipage}{.58\textwidth}
	$P=V\cdot I\cdot \cos\phi$

	$Q=V\cdot I\cdot \sin\phi$
\end{minipage}



\question{Tracer le diagramme de Fresnel représentant les grandeurs $\underline{V}$, $\underline E$ et $\underline{I}$. On prendra $\underline{V}$ comme référence des phases et on appellera $\alpha$ le déphasage de $\underline{E}$ par rapport à $\underline{V}$.}

Loi des mailles : $\underline{E}=j\cdot X\cdot \underline{I} +\underline{V}$

\begin{figure}[htbp]
\centering
\begin{tikzpicture}[>=latex]
	\draw[dashed,gray]	(4,0) -- (-30:{4*cos(30)});
	\draw[dashed,gray] 	(0,0) -- (-30:{.5+4*cos(30)});
	\draw[->,col2]	(0,0) -- (4,0) node[midway,below right]{\underline{V}};
	\draw[->,red!40!gray] (0,0) -- (-30:2)	node[midway,below]{\underline{I}};
	\draw[->,col3] (4,0) --++ (-30+90:.5)	node[midway,right]{$j\cdot X\cdot \underline{I}$};
	\draw[<-,cyan!60!gray] (4,0)++(-30+90:.5)	-- (0,0) node[midway,above right]{$\underline{E}$};
	\draw[->] (-30:1) arc (-30:0:1) (1,0) node[midway,right]{$\phi$};
	\draw[->] (0:3) arc (0:6:3) node[midway,right]{$\alpha$};
;\end{tikzpicture}
\end{figure}

\question{Exprimer V en fonction de E, X, I, $\phi$ et $\alpha$. Que peut-on dire de $\alpha$ si on suppose $X\cdot I \ll V$ ?\\ 
En déduire sous cette condition l'expression de V en fonction E, X, I et $\phi$.}



En repartant de la loi des mailles : $\underline{V}=\underline{E}-j\cdot X\cdot \underline{I}$


\begin{minipage}{.48\textwidth}
\centering
\begin{tikzpicture}[>=latex]
	\draw[<->]	(2,0)node[below]{Re}--(0,0)--(0,2)node[right]{Im};
	\draw[<->]	(-30:2)node[below]{\underline{I}}--(0,0)--(90-30:2)node[right]{$j\cdot \underline{I}$};
	\draw[->] (-30:1) arc(-30:0:1) node[midway,right]{$\phi$};
	\draw[->] (90-30:1) arc(90-30:90:1) node[midway,above]{$\phi$};
;\end{tikzpicture}

$\text{Re}(j\cdot \underline{I})=I\cdot \sin(\phi)$
\end{minipage}\hfill
\begin{minipage}{.48\textwidth}
\begin{tikzpicture}[>=latex]
	\draw[<->]	(2,0)node[below]{Re}--(0,0)--(0,2)node[right]{Im};
	\draw[->]	(0,0)--(20:2)node[right]{$\underline{E}$};
	\draw[->] (0:1) arc(0:20:1) node[midway,right]{$\alpha$};
;\end{tikzpicture}

$\text{Re}(\underline{E})=E\cdot \cos(\alpha)$
\end{minipage}

Par projection sur l'axe des réel, on obtient : $V=E \cos	\alpha- X\cdot I \sin \phi$

En supposant $X\cdot I\ll V$, alors $\alpha\simeq0$ d'où $V\simeq E -X\cdot I \sin \phi$


\question{Exprimer alors la chute de tension relative $\dfrac{\Delta V}{V}=\dfrac{E-V}{V}$ en fonction de $S_{cc}$ et $Q$. 

On admettra pour ce calcul que $\dfrac{1}{V^2}\simeq\dfrac{1}{E^2}$.}
$\dfrac{\Delta V}{V}=\dfrac{E - E + X\cdot I\sin\phi}{V}=\dfrac{X\cdot I\sin\phi}{V}$

Or $\displaystyle  S_{cc}=\frac{E^2}{X}\implies X=\frac{E^2}{S_{cc}}$ et $\displaystyle Q=V\cdot I\cdot \sin\phi\implies I\cdot \sin\phi=\frac{Q}{V}$

On obtient : $\displaystyle \frac{\Delta V}{V}=\frac{E^2}{S_{cc}}\cdot \frac{Q}{V^2}\simeq\frac{Q}{S_{cc}}$

\begin{description}
	\item[Application numérique :] 
\end{description}

\question{Donner la chute de tension relative dans le cas d'une installation de puissance de court-circuit \SI{20}{kVA} sur le réseau \SI{230}{V} sachant que les pointes de puissance réactives sur ce réseau peuvent atteindre \SI{2}{kVAR}. Déterminer $L_r$ dans ces conditions.}

$\displaystyle \frac{\Delta V}{V}=\frac{\SI{2}{kVAR}}{\SI{20}{kVA}}=\SI{10}{\percent}$

$\displaystyle X=L_r\cdot \omega\implies L_r=\frac{X}{\omega}=\frac{E^2}{S_{cc}\cdot \omega}=\frac{\num{230}^2}{\num{20000}\cdot 50\cdot 2\cdot \pi }=\SI{8.42}{mH} $





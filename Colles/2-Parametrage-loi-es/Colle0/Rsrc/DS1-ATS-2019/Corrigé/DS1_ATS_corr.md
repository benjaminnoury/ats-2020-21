---
titre: Echelle de pompier
type: Corrigé DS1
classe: ATS
CI: 7 Transmettre l'énergie
#competence:
#question-section: 1
#document: twocolumn,landscape
---


# Étude du mécanisme

\question{
\begin{center}
	\includegraphics{img/grapheLiaisons}
\end{center}
}

# Loi de commande du vérin

\question{

$$
\begin{array}{clclcll}
	\ovr{AB} &+& \ovr{BC} &+& \ovr{CA} &= \vec 0 \\
	a \cdot \vec x_{1} - b \cdot \vec y_{1} &+& d \cdot \vec y_{4} &-& l_{2} \cdot \vec x_{2} &= \vec 0
\end{array}
$$

En projettant respectivement sur $\vec x_{1}$  et $\vec y_{1}$, on obtient :


$$
\left \lbrace
\begin{aligned}
	a - d \sin (\alpha) - l_{2} \cos (\theta) = 0 \\
	-b + d \cos (\alpha) - l_{2} \sin (\theta) = 0
\end{aligned}
\right .
\text{\qquad Soit : }
\left \lbrace
\begin{aligned}
	d \sin (\alpha) = a - l_{2} \cos (\theta) \\
	d \cos (\alpha) = b + l_{2} \sin (\theta)
\end{aligned}
\right .
$$

En élevant au carré et en sommant terme à terme, on obtient :
$$
d^{2} \underbrace{\left [\sin^{2} (\alpha) + \cos^{2} (\alpha)\right ]}_{=1}
 = a^{2} +b^{2} + l_{2}^{2} \underbrace{\left [\sin^{2} (\theta) + \cos^{2} (\theta)\right ]}_{=1} + 2 \cdot l_{2} \left ( b \sin (\theta) - a \cos (\theta) \right )
$$

$$\boxed{
d^2=a^2+b^2+{l_2}^2+2\cdot l_2 \left(b \sin\theta-a \cos \theta \right)
}$$

}

\begin{center}
\begin{minipage}[t]{.47\linewidth}
\question{
	\image[.8]{img/traceCorrige}

	$d= 0,017 \cdot \theta + 0,72$
	en mètre et en degré
}
\end{minipage}\hfill
\begin{minipage}[t]{.47\linewidth}
\question{$\dot d = 0,017 \cdot \dot \theta$

soit dans le système international (en mètre et en radian)
$$
\dot d = 0,97 \cdot \dot \theta
$$
}

\end{minipage}
\end{center}


\section{Loi de commande du vérin (2-3)}

\question{ Le mouvement (6/0) étant une translation, $\vec \Omega(6/0)=\vec  0$
$$
\V{G,6/0} = \V{D,6/0} + \overrightarrow{GD} \wedge \vec \Omega(6/0)=\V{D,6/0}
$$
}
\question{
En utilisant la méthode de dérivation  ($D \in 6$)
$$
\V{D,6/0} =
\ddv[0]{\ovr{AD}}
= \ddv[0]{L \cdot \vec x_{2}} = \dot L \vec x_{2} + L \ddv[0]{\vec x_{2}}
$$

$$
\ddv{\vec x_{2}} = \ovr \Omega(2/0) \wedge \vec x_{2} = \dot \theta \vec z_{2} \wedge \vec x_{2} = \dot \theta \vec y_{2}
\implies
\boxed{
\V{G,6/0} = \dot L \cdot \vec x_{2} + L	\ \dot \theta	\cdot \vec y_{2}
}$$


Soit $\boxed{\V{G,6/0} = \dot L \cdot \vec x_{2} + \frac{L}{K} \ \dot d \cdot \vec y_{2}}$
}

\question{
	$\displaystyle
	\ovr{\Gamma_{G,6/0}}= \ddv{\V{G,6/0}}
	=
	\ddot L \cdot \vec x_{2}
	+ \dot L \cdot \ddv{\vec x_{2}}
	+ \frac{1}{K} \left (
	\dot L \dot d \cdot \vec y_{2}
	+ L \ddot d \cdot \vec y_{2}
	+ L \dot d \ddv{\vec y_{2}}
	 \right )
	$

$\displaystyle
\ddv{\vec y_{2}}
= \vec y_{2} \wedge \ovr \Omega(0/2) = \vec y_{2} \wedge (-\dot \theta  \vec z_{2}) = - \dot \theta \vec x_{2} = - \frac{\dot d}{K} \vec x_{2}$

$$\boxed{
	\ovr{\Gamma_{G,6/0}}
	=
	\left (\ddot L - \frac{L \ \dot d ^{2} }{K^{2}}  \right )
	\cdot \vec x_{2}
	+
	\left ( 2 \frac{\dot L \ \dot d}{K} + \frac{L \  \ddot d }{K} \right )
	\cdot \vec y_{2}
}$$
}

\question{
	$\displaystyle
	\norm{\ovr{\Gamma_{G,6/0}}}
	=\sqrt{
	\left (\ddot L - \frac{L \ \dot d ^{2} }{K^{2}}  \right ) ^{2}
	+
	\left ( 2 \frac{\dot L \ \dot d}{K} + \frac{L \  \ddot d }{K} \right )^{2}
	}
	$


	Pour que la nacelle évolue à vitesse constante, $L$ et $d$ doivent vérifier :
	$$\boxed{
	\sqrt{
	\left (\ddot L - \frac{L \ \dot d ^{2} }{K^{2}}  \right ) ^{2}
	+
	\left ( 2 \frac{\dot L \ \dot d}{K} + \frac{L \  \ddot d }{K} \right )^{2}
	} = 0
	}
	$$
}

\section{Motorisation de la tourelle}

\question{
$N_1=\displaystyle \frac{n_1\cdot Z_{23}}{Z_{22}\cdot Z_1}\cdot N_{mot}
\implies
Z_{22}=\frac{n_1\cdot Z_{23}}{Z_1}\cdot \frac{N_{mot}}{N_1}$


$\dfrac{1\cdot 20}{70}\cdot 300 = 85,7 \implies \boxed{Z_{22}=86\text{ dents}}$ (le nombre de dents est entier)}

\question{
$t_1=\dfrac{V}{a} = \dfrac{1\cdot 2\pi}{60\cdot 5,2\cdot 10^{-2}}=2,0$ s

En nommant $\delta$ l'angle effectué, $\delta=t_2 \cdot \omega_{max}\implies
t_2=\dfrac{\delta}{\omega_{max}} = \dfrac{1/4}{1}=0,25\text{ min}=15$ s

L'accélération et la décélération ayant la même norme, $T-t_2=t_1$ d'où
$\boxed{T=t_1+t_2=17\text{ s}}$

}

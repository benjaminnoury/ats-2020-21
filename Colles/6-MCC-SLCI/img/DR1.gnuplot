#!/usr/bin/gnuplot -persist
# vi:syntax=gnuplot 

reset

set terminal cairolatex pdf size 6,4 #mono
set palette cubehelix

unset key

set loadpath '~/.config/gnuplot-config/latex'
load 'xyborder.cfg'
load 'grid.cfg'

set xlabel 'N (tr/min)'
set title 'DR1'
set ylabel  '$C_U$ (\si{N.m})'

set xrange [0:2000]
set yrange [0:35]
set mxtics 5
set mytics 5

C160(x)=199.3-.219*x
C310(x)=387.9-.219*x
charge(x)=21.8+(x-810)*1./810

set style line 100 lt 1 lc rgb "black" lw 1
set style line 101 lt 1 lc rgb "gray" lw 1

set grid mytics ytics ls 100, ls 101
set grid mxtics xtics ls 100, ls 101

# plot C160(x) lw 4 , C310(x) lw 4, charge(x) lw 4
plot  charge(x) lw 4

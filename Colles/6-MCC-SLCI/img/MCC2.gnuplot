#!/usr/bin/gnuplot -persist
# vi:syntax=gnuplot 

reset

set terminal cairolatex pdf size 6,4 #mono
set palette cubehelix

unset key

set loadpath '~/.config/gnuplot-config/latex'
load 'xyborder.cfg'
load 'grid.cfg'

set xlabel '$\Omega_m(\si{rd/s})$'
set ylabel '$P_C(W)$'

set xrange [0:180]
#set xtics 1

ri=1.32*25*25
CC=2*pi/60

f(x)=a*x
fit f(x) '-' via a
# 77.49 75
89.01 175
136.14 275
166.50 325
e

plot '-' u ($1):2  w p lw 7, f(x) lw 4, '-' w p ls 6 lw 7
77.49 75
89.01 175
136.14 275
166.50 325
e
77.49 75
e

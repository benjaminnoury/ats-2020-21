#,b!/usr/bin/gnuplot -persist
# vi:syntax=gnuplot 

reset

set terminal cairolatex pdf size 6,4 #mono
set palette cubehelix

unset key

set loadpath '~/.config/gnuplot-config/latex'
load 'xyborder.cfg'
load 'grid.cfg'

set xlabel 'N(tr/min)'
set ylabel 'U(V)'

set yrange [0:320]
set xrange [0:1600]
set ytics 30
set mytics 3



f(x)=a*x+b
fit f(x) '-' via a,b
740 160
850 180 
1300 260
1590 310
e

us(x)=(310-160.)/(1590-740)*(x-740)+160

plot '-' w p ls 1 lw 4, f(x) lw 4
740 160
850 180
1300 260
1590 310
e

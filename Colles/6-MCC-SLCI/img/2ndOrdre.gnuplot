#!/usr/bin/gnuplot -persist
# vi:syntax=gnuplot 

reset

set terminal cairolatex pdf size 6,3 #mono
set palette cubehelix

set key bottom right box

set loadpath '~/.config/gnuplot-config/latex'
load 'xyborder.cfg'
load 'grid.cfg'

set xlabel 't(ms)'
set ylabel 'Vitesse de rotation (rd/s)'

set xrange [0:3]
set xtics .25
set mxtics 2
set mytics 3

set samples 1000

A=1
E=.03
om=8
m=.25
op=om*sqrt(1-m**2)
phi=acos(m)

e(x)=E
s(x)=A*E*(1-exp(-m*om*x)*sin(op*x+phi)/sqrt(1-m**2))

plot  e(x) lw 4 t '$\omega_C(t)$', s(x) t '$\omega(t)$' lw 4,




---
titre: Notations en mécanique
type: Kit de survie
classe: ATS
#competence: 
#question-section: 1
#document: landscape
---

# Généralités

\begin{tabularx}{.9\textwidth}{rX} 
$\mathcal{B}$ & Base de l'espace. Dans ce document, on pose $\mathcal{B}=(\vec x,\vec y,\vec z)$ \\
$\mathcal{B}_{1}$ & Base de l'espace lié à la pièce $1$, $\mathcal{B}_{1} =(\vec x_{1},\vec y_{1},\vec z_{1})$. Fréquemment, la base lié au bâti est juste appelée $\mathcal{B}$\\
$\mathcal{R}_{1}$ & Référentiel lié à la pièce 1. Composé d'un point lié à la pièce $1$ et de la base $\mathcal{B}_{1}$. 
\end{tabularx}

# L'outil torseur

Le torseur est une représentation d'un champ de vecteurs équiprojectif. Ce champ de vecteurs est entièrement défini par : 

 - Sa valeur en un point, appelé moment ;
 - Un vecteur invariant, appelé résultante, permettant de calculer sa valeur en un autre point.

L'écriture du torseur est toujours la même (torseur cinématique dans l'exemple): 

\Begin{center}
	\includegraphics{img/test}
\End{center}


Remarque : 
:	la base d'expression peut être n'importe quelle base. Sans indication du sujet, choisir toujours la base permettant l'expression la plus simple.

# Torseur et notations en Cinématique

### Grandeurs et notations

\begin{tabularx}{\textwidth}{rX} 
\((2/1)\) & Mouvement du solide 2 par rapport au référentiel galiléen fixe lié au solide 1. \\
& \fbox{\textbf{Se prononce "2 par rapport à 1"}} et pas "2 sur 1".\\
\(T_{A}(2/1)\) & Trajectoire du point A dans le mouvement \((2/1)\)\\
 & Ensemble des positions prise par le point A dans le mouvement (2/1) \\
\(\ovr \Omega(2/1)\) & Vecteur vitesse de rotation du solide 2 dans son mouvement par rapport 1. \\
	& Unité S.I. : rd/s \\
\(\V{A,2/1}\) ou \(\V{A}(2/1)\) & Vecteur Vitesse du point A dans le mouvement de
2 par rapport à 1.\\
& Unité S.I. : m/s \\
\(\acc{A}(2/1)\) ou \(\ovr{\Gamma_{A}}(2/1)\) & Vecteur accélération du point A dans le mouvement de 2 par rapport à 1. \\
& Unité S.I. : m/s$^{2}$ \\
\(\tv{2/1}\) & Torseur cinématique du mouvement (2/1) 
\end{tabularx}



### Torseur Cinématique : \quad $\displaystyle \tv{2/1}=\torseur{A}{ \ovr \Omega(2/1)\\ \V{A}(2/1)} = 
\torseur{A}{ 
\omega_{x} & V_{A,x} \\
\omega_{y} & V_{A,y} \\
\omega_{z} & V_{A,z}
}_{\mathcal{B}}$

### Relation de Varignon en cinématique : \quad $\V{B}(2/1) = \V{A}(2/1) + \ovr{BA}\wedge \ovr \Omega(2/1)$

### Composition des vitesses : 

$\begin{aligned}[t]
\ovr \Omega(3/1) & = \ovr \Omega(3/2) + \ovr \Omega(2/1)
\\
\V{A}(3/1) &= \V{A}(3/2) + \V{A}(2/1)
\\
\tv{3/1} &= \tv{3/2} + \tv{2/1} \text{ \ (les torseurs doivent être écrits au même point pour le calcul)}
\end{aligned}$

\Begin{flushleft}

**Dérivée d'un vecteur unitaire : ** $\displaystyle \left [ \ddt[\vec y_{2}] \right]_{\mathcal{B}_{1}} = \ovr \Omega(2/1) \wedge \vec y_{2}$
	
\End{flushleft}

 

# Actions Mécaniques

### Grandeurs et notations

\begin{tabularx}{.9\textwidth}{rX}
	$\vec R(2 \to 1)$ & Résultante ou Force de l'action du solide $2$ sur le solide $1$ \\
	& Unité S.I. : N \\
	$\ovr{M_A}(2 \to 1)$ & Moment de l'action sur solide $2$ sur le solide $1$ exprimé en $A$ \\
	& Unité S.I. : N.m \\
	$\tam{2 \to 1}$ & Torseur des actions mécaniques transmissibles sur solide $2$ sur le solide $1$ \\ 
\end{tabularx}

### Torseur des actions mécaniques \quad $\tam{2 \to 1}
=\torseur{A}{ \vec R (2 \to 1) \\ \ovr{M_A}(2 \to 1) } = 
\torseur{A}
{ X_{21} & L_{A,21} \\ Y_{21} & M_{A,21}\\ Z_{21} & N_{A,21}}_{\mathcal{B}}$

\columnseprule=.4pt

\Begin{multicols}{2}

### Torseur Couple : 

Torseur dont la résultante est nulle. Ce torseur est un invariant de l'espace (Même valeur en tout point).

\ 

$$\tam{2 \to 1}
=\torseur{A}{ \vec 0 \\ \ovr{M_A}(2 \to 1) } = 
\torseur{A}
{ 0 & L_{A,21} \\ 0 & M_{A,21}\\ 0 & N_{A,21}}_{\mathcal{B}}$$

\vfill

### Torseur Glisseur : 

Torseur pour lequel il existe au moins un point où le moment  est nul. Ce torseur possède un axe sur lequel sa valeur est la même.

$$\tam{2 \to 1}
=\torseur{A}{ \vec R(2 \to 1) \\ \ovr{0} } = 
\torseur{A}
{ X_{21} & 0 \\ Y_{21} & 0\\ Z_{21} & 0}_{\mathcal{B}}$$

L'axe de ce torseur est l'axe $\left (A,\vec R(2 \to 1) \right )$
\End{multicols}


### Relation de Varignon : \quad $\ovr{M_B}(2 \to 1) = \ovr{M_A}(2 \to 1) + \ovr{BA} \wedge \vec R(2 \to 1)$


# Cinétique

### Grandeurs et notations

\begin{tabularx}{\textwidth}{rX}
	$m_{1}$ & Masse du solide $1$ \\
	& Unité S.I. : kg \\
	$G_{1}$  & Centre d'inertie du solide $1$, aussi appelé centre de gravité ou centre de masse. \\
	$\I_{G_{1}}(1)$ & Opérateur d'inertie ou matrice d'inertie du solide $1$ exprimé en $G$ \\
	& Unité S.I.: kg.m$^{2}$ \\
	$\text{I}_{G,\vec u}(1)$ ou $\text{I}_{\Delta}(1)$& Moment d'inertie par rapport à l'axe $\Delta=(G,\vec u)$ du solide $1$ \\
	& Unité S.I.: kg.m$^{2}$ \\
	$\ovr{C}(2/1) = m_{2} \cdot \V{G_{2}}(2/1)$ & Résultante cinétique du solide 2 dans son mouvement par rapport au solide (1). Aussi appelé \emph{quantité de mouvement} \\
	& Unité S.I. : kg.m/s \\
	$\Mcin{A}(2/1)$ & Moment cinétique du solide 2 dans son mouvement par rapport à $1$ exprimé au point $A$.\\
	& Unité S.I. : kg.m$^{2}$/s \\
	$\tcin{2/1}$ & Torseur Cinétique du solide $2$ dans son mouvement par rapport à $1$ \\
	$Ec(2/1)$ ou $T(2/1)$ & Énergie cinétique du solide $2$ dans son mouvement par rapport à $1$\\
	& Unité S.I. : J 
\end{tabularx}


### Torseur Cinétique : \quad $\tcin{2/1} = \torseur{A}{ m_{2} \cdot \V{G_{2}}(2/1) \\ \Mcin{A}(2/1)}$

### Relation de Varignon : $\Mcin{B}(2/1)=\Mcin{A}(2/1)+m_{2} \cdot \ovr{BA}\wedge \V{G_{2}}(2/1)$

\Begin{flushleft}

**Calcul du moment cinétique :** $\left .
\begin{array}{ll}
\text{ Si $A$ est un point fixe dans le mouvement} (2/1) &\left (\V{A}(2/1) = \vec 0 \right ) \\
\text{ Si $A$ est le centre de gravité du solide } 2 &(A=G_{2})
\end{array}
\right \rbrace{}
\Mcin{A}(2/1) = \I_{A}(2)\cdot \ovr \Omega(2/1)$
\End{flushleft}

Sinon, utiliser la relation de Varignon pour se ramener dans un de ces deux cas.

\ 

Exemple de point fixe : Centre (Instantanné) de rotation.

\ 

Remarque : Pour un solide en translation, $\Mcin{G_{2}}(2/1)=\vec 0$, mais ce n'est pas nécessairement vrai en d'autres point.

\pagebreak

### Calcul de l'énergie cinétique : $Ec(2/1) = 
\frac{1}{2} \tcin{2/1} \otimes \tv{2/1} = 
\dfrac{1}{2} \torseur{A}{ m_{2} \cdot \V{G_{2}}(2/1) \\ \Mcin{A}(2/1)} \otimes \torseur{A}{ \ovr \Omega(2/1)\\ \V{A}(2/1)}$


- Cas général : $Ec(2/1) = \dfrac{1}{2} \left ( m_{2} \cdot \V{G}(2/1) \cdot \V{A}(2/1) + \ovr \Omega(2/1).\Mcin{A}(2/1)\right )$

\ 

- Au centre de gravité : $Ec(2/1) = \dfrac{1}{2} \left ( m_{2} \cdot \V{G}(2/1)^{2} + \ovr \Omega(2/1).\I_{A}(2)\ovr \Omega(2/1)\right )$

\ 

- En un point $A$ fixe dans le mouvement (2/1): $Ec(2/1) = \dfrac{1}{2} \left ( m_{2} \cdot \V{G}(2/1)^{2} + \ovr \Omega(2/1).\I_{A}(2)\ovr \Omega(2/1)\right )$

\ 

- Solide en translation : $Ec(2/1) = \dfrac{1}{2} \, m_{2} \cdot \V{*}(2/1)^{2}$



# Dynamique

### Grandeurs et notations

\begin{tabularx}{\textwidth}{rX}
	$\vec D (2/1) = m_{2} \cdot \acc{G_{2}}(2/1)$ & Résultante cinétique ou \emph{quantité d'accélération} du solide $2$ dans son mouvement par rapport à $1$\\
	& Unité S.I. : kg.m/s$^{2}$ \\
	$\Mdyn{A}(2/1)$ & Moment dynamique du solide $2$ dans son mouvement par rapport à $1$ exprimé au point $A$ \\
	& Unité S.I. : kg.m$^{2}$/s$^{2}$ \\
	$\tdyn{2/1}$ & Torseur dynamique du solide $2$ dans son mouvement par rapport à $1$
\end{tabularx}


### Torseur dynamique : \quad $\tdyn{2/1} = \torseur{A}{ m_{2} \acc{G_{2}}(2/1) \\ \Mdyn{A}(2/1) }$ 

### Relation de Varignon : $\Mdyn{B}(2/1) = \Mdyn{A}(2/1) + m_{2} \cdot \ovr{BA} \wedge \acc{G_{2}}(2/1)$


\Begin{flushleft}

**Calcul du moment dynamique :** $\left .
\begin{array}{ll}
\text{ Si $A$ est un point fixe dans le mouvement} (2/1) &\left (\V{A}(2/1) = \vec 0 \right ) \\
\text{ Si $A$ est le centre de gravité du solide } 2 &(A=G_{2})
\end{array}
\right \rbrace{} ~
\Mdyn{A}(2/1) = \displaystyle \left [ \ddt[\I_{A}(2)\cdot \ovr \Omega(2/1)] \right]_{\mathcal{B}_{0}}$
\End{flushleft}


Sinon, utiliser la relation de Varignon pour se ramener dans un de ces deux cas.

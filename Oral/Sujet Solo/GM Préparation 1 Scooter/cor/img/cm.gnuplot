#!/usr/bin/gnuplot -persist
# vi:syntax=gnuplot 

reset

set terminal cairolatex pdf size 6,3 #mono
set palette cubehelix

unset key

set loadpath '~/.config/gnuplot-config/latex'
load 'xyborder.cfg'
load 'grid.cfg'

set xlabel 'V (km  /h)'
set ylabel '$C_m (N.m)$'

set xrange [0:55] 
set yrange [0:15] 
set xtics 2.5
set ytics 1

f2(x)=(14-1.55)/(25-52.5)*(x-25)+14

f1(x)=x<25 ? 14:f2(x)

plot f1(x) lw 4


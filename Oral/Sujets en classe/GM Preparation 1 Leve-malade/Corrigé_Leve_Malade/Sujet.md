---
titre: Leve Malade Orior
type: Corrigé Prépa Oraux
classe: ATS
CI:
competence: 
#question-section: 1
#ficheTD: 1
#document: twocolumn,landscape
---

\stepcounter{section}

# Analyse d'une solution constructive

## Etude générale

\question{La liaison 3/1 est un pivot d'axe $(A,\vec z)$}
\question{en posant $\mathcal{B}=(\vec x,\vec y,\vec z)$,

	$\tv{3/1}=\torseur{A}{ \omega_{31}\, \vec z_{}\\ \vec 0}=\torseur{A}{ 0 & 0 \\ 0 & 0\\ 0 &\omega_{31}}_{\mathcal{B}}$
	\quad	{}
	$\tam{1 \to 3} = \torseur{A}{ \vec R(3 \to 1)\\ L_{A}\, \vec x + M_{A}\, \vec y } = \torseur{A}{ X & L_{A} \\ Y & M_{A}\\ Z &0}_{\mathcal{B}}$
}


## Analyse d'architecture

\question{
$I_{C}=	9$, $\gamma=2$, $E_{c}=12$, $I_{S}=39$, $E_{S}=36$, $m=2$ (translation du vérin, rotation propre de la pièce 6)

$h=E_{c}-I_{C}+m = I_{S}-E_{S}+m = 5$
}

\question{Sous l'hypothèse mécanisme plan, la rotation de la pièce $(6)$ ne peut plus exister $\implies m=1$. De même, le degré de liberté en rotation de la liaison 8 --- 7 est en dehors du plan.

$I_{C}=7$, $E_{C}=3*2=6$ $\implies h=0$ 
}


# Vérification de la vitesse de déplacement de la personne



\question{le mouvement (3/1) est une rotation d'axe $(A,\vec z)$}

\question{$T_{F}(3/1)$ est un cercle de centre $A$ et de rayon $[AF]$}

\begin{flushleft}
\textbf{(Question 7 à 9)}


$\V{C}(3/1) = \V{A}(3/1) + \ovr{CA} \wedge \ovr \Omega(3/1) = \ovr{CA} \wedge \ovr \Omega(3/1)$. Or $\ovr{CA} \perp \ovr \Omega(3/1) \implies \norm{\ovr{CA} \wedge  \ovr \Omega(3/1)}=\norm{\ovr{CA}} \cdot \norm{\ovr \Omega(3/1)}$


De même, $\norm{\V{F}(3/1)} = \norm{\ovr{FA}} \cdot \norm{\ovr \Omega(3/1)}$.

D'où $\boxed{\norm{\V{C}(3/1)}=\norm{\V{F}(3/1)} . \dfrac{CA}{FA}}$



Le mouvement ($S_{1}/1$) étant une translation, $\boxed{\V{G}(3/1) =\V{C}(3/1)}$

\image[.6]{img/CorrigeCinematiqueGraphique.pdf}	
\end{flushleft}

\stepcounter{ques}
\stepcounter{ques}
\stepcounter{ques}


# Validation de l’effort developpe par l’actionneur


\question{
La pièce 2 est en équilibre sous l'action des deux glisseurs $\tam{1 \to 2}$ et $\tam{4 \to 2}$

Les résultantes de ces deux glisseurs sont donc égales en normes, opposées et portées par la droite reliant les points d'application des deux glisseurs.

\renewcommand{\|}{\mathbin{//}}

$
\left \lbrace 
\begin{array}{l}
	\vec R(1 \to 2) = - \vec R(4 \to 2)\\
	\vec R(1 \to 2) \| (DB)
\end{array}
\right .$
}

\question{
Bilan des actions mécaniques extérieures à $S_{1} = \{4,5,6,p\}$

$\tam{2 \to 4}=\torseur{D}{\vec R(2 \to 4) \\ \vec 0 }$
\quad
$\tam{3 \to 4}=\torseur{C}{ \vec R(3 \to 4) \\ \vec 0}$
\quad 
$\tam{pes \to P}=\torseur{G}{ - P \, \vec y \\ \vec 0 }$
}



\question{
	$\begin{aligned}
	\ovr{M_D}(2 \to 4) + \ovr{M_D}(3 \to 4)+\ovr{M_D}(pes \to P)= \vec 0 \\
	\vec 0 + \ovr{DC}\wedge \vec R(3 \to 4) + (\ovr{DC} + \ovr{CG}) \wedge \vec R(pes \to p) = \vec 0
	\end{aligned}$
	}


\question{Par relevé sur la courbe, la tige étant sortie de 100 mm, l'effort du vérin est de $2\,370$ N}
\question{Ce n'est pas la position la plus défavorable pour le vérin sur le critère de l'effort à fournir}


# Validation de la batterie

\question{$P_{max-verin}=15 \cdot 10^{-3} \times 2,41 \cdot 10^{3} = 36$ W}

\question{
\Begin{center}
	\includegraphics{img/graphe}
\End{center}}

\question{$\eta_{global}=\Pi \eta_{i} = 0,9 \times 0,7=0,63$ 

$P_{a}=\frac{P_{u}}{\eta_{g}} = \frac{36}{0,63}=57$ W}

\question{$P_{batterie} = U_{bat} \times I_{nom-bat} = 12\times6,5=78$ W}

\question{La puissance maximale nécessaire au déplacement est proche de la puissance nominale de la batterie ($-26$\%). En considérant que le moteur est la seule charge de la batterie, l'ensemble est légèrement surdimensionné.}


\question{}
	
	P = E(:,2)*V(:,2)

En utilisant la fonction max de Scilab,

	Pmax = max(P) 

Sans utiliser cette fonction,

	Pmax=0
	for i=1:size(E,1)
		if Pmax < P(i)
			Pmax=P(i)
		endif
	endfor

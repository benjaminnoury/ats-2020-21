b=.172
c=.195
alpha=-17.5*%pi/180

m=35
g=9.81
mg=m*g
h=.3
d=.184

XC=mg*h/d
YC=mg*(1+h/d/3**.5)

MA74=(b+c)*sin(alpha)*XC-c*YC*sin(alpha)-(b+c)*YC*cos(alpha)-c*XC*cos(alpha)


theta=54.3*%pi/180

fv=-MA74/(b*sin(theta-alpha))

I = 3E-4 #kg.m2
J=15E-3 #kg.m2
m=3 #kg
Cr=0.1 #Nm
Cm=0.2 #Nm
g=9.81 #N/kg
l=10E-2 # lambda m
r = 3E-2 # m
om=31.42 #rd/s
f=0.2


T=(-r*om)/(l/J*(Cr-l*f*m*g)+r/I*(Cm-r*f*m*g))

print("{:.2e}".format(T))
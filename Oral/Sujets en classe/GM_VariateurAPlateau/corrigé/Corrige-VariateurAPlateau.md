---
titre: Corrigé Variateur à Plateau
classe: ATS
chapitre: Dynamique + frottement
type: Corrigé



\question{Graphe des actions mécaniques
\image[.5]{img/grapheAM.pdf}
}
\question{Étude du contact Plateau (P) - Galet (G)
\begin{enumerate}
\item $
\begin{aligned}[t]
	\V{H}(P/G) &= \V{H}(P/B)-\V{H}(G/B)\\
	& = \left [\V{C}(P/B) + \ovr{HC}\wedge \ovr \Omega(P/B) \right]- \left [ \V{A}(G/B) + \ovr{HA} \wedge \ovr \Omega(G/B) \right] \\
	& = \left [\vec 0 - \lambda \vec x \wedge \dot \beta \vec z \right] - \left [\vec 0 - r \vec z \wedge \dot \alpha x\right]\\
	\end{aligned}
	$
	
	{$\boxed{\V{H}(P/G)  = \left (\lambda \dot \beta + r \dot \alpha \right	) \vec y  }$}

\item Lois du frottement de Coulomb
\begin{enumerate}
\item $\ovr{R}(G \rightarrow P) = \vec N + \vec T $ avec $\vec N$ normal au contact et $\vec T$  tangent au contact,		ici $\vec N = Z \vec z$ \  ($Z > 0$) et $\vec T = X \vec x + Y \vec y$

\item $\vec T$ s'oppose au mouvement relatif au contact $\V{H}(P/G)$ donc $\vec T = -T \vec y$. \fbox{X=0} et Y<0

\item $\norm{\vec T} \leq f \norm{\vec N}$ soit ici : $0 \leq Y \leq -f Z$
\end{enumerate}
\medskip
\item On isole (P)

\begin{minipage}{.45\lw}
	\image{img/grapheAM-P.pdf}
\end{minipage}\hfill
\begin{minipage}{.5\lw}
$$\begin{aligned}
\{pes \rightarrow  P\} &= \tc{C}{-m g \vec z \\ \vec 0}\\
\{G \rightarrow P\} &= \tc{H}{Y\vec y + Z \vec z \\ \vec 0} \\
\{C_{R} \rightarrow P\} &= \tc{*}{\vec 0 \\ C_{R} \vec z}\\
\{B \rightarrow P \} &= \tc[R]{C}{X_{C}&L_{C}\\Y_{C}&M_{C}\\0&0}\\
\end{aligned}$$
\end{minipage}

\medskip{}

Le centre de Gravité de (P) est C et $\vec a_{C}(P/B)=\vec 0$; donc, par projection du théorème de la résultante dynamique sur $\vec z$, on obtient:

$$Z - mg = 0 \Rightarrow \boxed{Z=mg}$$


\item On déduit de ce qui précède $\boxed{0 \leq Y \leq -f m g}$
\end{enumerate}
}
\newpage
\question{On isole (G)

\begin{minipage}{.45\lw}
	\image{img/grapheAM-G.pdf}
\end{minipage}\hfill
\begin{minipage}{.5\lw}
$$ \begin{aligned}
\{pes \rightarrow  G\} &= \tc{A}{-m g \vec z \\ \vec 0} \\
%
\{P \rightarrow G\} &= \tc{H}{-Y\vec y  - mg \vec z \\ \vec 0} = \tc{A}{-Y\vec y - mg \vec z \\  + r Y \vec x} \\
%
\{C_{m} \rightarrow G\} &= \tc{*}{\vec 0 \\ C_{m} \vec x}\\
		%
\{B \rightarrow G \} &= \tc[R]{A}{X_{A}&0\\Y_{A}&M_{A}\\Z_{A}&N_{A}}\\
\end{aligned}
$$
\end{minipage}

\subsubsection*{Calcul du moment dynamique $\vec x . \vec \delta_{A}(G/B)$}

A est un point fixe dans (G/B) ( car A $\in$ axe de rotation) donc : 
\begin{itemize}
\item $\vec \delta_{A}(G/B) = \left [\displaystyle  \ddt[\vec \sigma_{A}(G/B)] \right]_{R}$
\item $\vec \sigma_{A}(G/B) = \I_{A}(G) . \ovr \Omega(G/B)$
\end{itemize}

\medskip

Donc : $\vec x . \vec \delta_{A}(G/B) = \vec x . \left [ \displaystyle \ddt[\I_{A}(G) . \ovr \Omega(G/B)] \right]_{R} = \vec x \left [ \ddt[\I_{A}(G).\dot \alpha \vec x] \right]_{R} = \vec x .\I_{A}(G).  \ddot \alpha \vec x = \ddot \alpha \underbrace{\vec x . \I_{A}(G) . \vec x }_{I} = I \ddot \alpha$

$\vec x . \I_{A}(G). \vec x$ est par définition le moment d'inertie de (G) par rapport à l'axe $(A,\vec x)$. 

$(A,\vec x)$ et $(O,\vec x)$ étant le même axe, $\vec x . \I_{A}(G) \vec x = I$ (donnée du sujet)

\subsubsection*{Application du PFD à (G) en projection sur $(A,\vec x)$ :	$\boxed{C_{m} + r Y = I \ddot \alpha}$ }
} 

\question{On isole (P)

\begin{minipage}{.45\lw}
	\image{img/grapheAM-P.pdf}
\end{minipage}\hfill
\begin{minipage}{.5\lw}
$$\begin{aligned}
\{pes \rightarrow  P\} &= \tc{C}{-m g \vec z \\ \vec 0}\\
\{G \rightarrow P\} &= \tc{H}{Y\vec y + Z \vec z \\ \vec 0} = \tc{C}{Y\vec y + Z \vec z \\ \lambda Y \vec z}\\
\{C_{R} \rightarrow P\} &= \tc{*}{\vec 0 \\ C_{R} \vec z}\\
\{B \rightarrow P \} &= \tc[R]{C}{X_{C}&L_{C}\\Y_{C}&M_{C}\\0&0}\\
\end{aligned}$$
\end{minipage}

\subsubsection*{Calcul du moment dynamique $\vec z . \vec \delta_{C}(P/B)$}

C étant un point fixe dans le mouvement (P/B), on obtient par un raisonnement analogue à la question précédente : $$ \vec z . \vec \delta_{C}(P/B) = J \ddot \beta$$
{}

\subsubsection*{Application du PFD à (P) en projection sur $(C,\vec z)$ :	$\boxed{C_{r} + \lambda	Y = J \ddot \beta }$}
}

\question{Étude de la phase transitoire

\begin{enumerate}
\item Pendant la phase de glissement, $Y=-fmg$
\newpage
\item Calcul de la durée de glissement

$\displaystyle C_{m} + r Y = I \ddot \alpha \Rightarrow \ddot \alpha = \frac{C_{m} - r f m g }{I} \Rightarrow \dot \alpha (t) = \frac{C_{m} - r f m g }{I}  . t + \dot \alpha(0) = \frac{C_{m} - r f m g }{I}  . t + \omega$

$\displaystyle C_{r} + \lambda	Y = J \ddot \beta \Rightarrow \ddot \beta = \frac{C_{r}-\lambda f m g }{J} \Rightarrow \dot \beta (t)= \frac{C_{r}-\lambda f m g }{J} . t + \dot \beta (0) =\frac{C_{r}-\lambda f m g }{J} . t$

La phase de glissement s'achève lorsque la vitesse de glissement devient nulle :
$$
\begin{aligned}
\V{H}(P/G) &= \vec 0\\
\lambda \dot \beta  +r \dot \alpha &= 0 \\
\lambda \frac{C_{r}-\lambda f m g }{J} . t + r \left (\frac{C_{m} - r f m g }{I}  . t + \omega \right) &= 0 \\
\left (\lambda  \frac{C_{r}-\lambda f m g }{J} + r \frac{C_{m} - r f m g }{I} \right ) .  t  &= -r \omega{}\\ 
\end{aligned}
$$
$$
 \boxed{T = \frac{-r \omega}{ \lambda  \frac{C_{r}-\lambda f m g }{J} + r \frac{C_{m} - r f m g }{I}} }
$$

\subsubsection*{Vérification : Homogénéité}
$$T=\frac{[m][rd/s]}{[m]\frac{[N.m]+[m][kg][N/kg]}{[kg.m^{2}]}} = \frac{[m/s]}{[N/kg]} = \frac{[m/s]}{[m/s^{2}]} = [s]$$
\end{enumerate}
}


\question{Application numérique : T = 1.03 s}	

cd("/home/ben/Documents/Branly/ATS/2020-21/Info/Moyenne")
//test de moyenne glissante
clf
M=fscanfMat("Echelon 0-90° Essai 4.Txt")

T=M(:,1)/1000
P=M(:,6)


subplot(3,1,1)

N=4 // nb point moyenne glissante:

for i = 1:length(P)
    Pm(i) = mean(P(max([1 i+1-N]):i))
end

plot(T(1:$-floor(N/2)),Pm(1+floor(N/2):$),'k-')
plot(T,P,"r.")
title(msprintf("Moyenne glissante sur %i points",N))

// dérivation


dP=(P(2:$)-P(1:$-1))./(T(2:$)-T(1:$-1))
dP=[0;dP]

subplot(3,1,2)
title("Dérivée de la moyenne")

dPm=(Pm(2:$)-Pm(1:$-1))./(T(2:$)-T(1:$-1))
dPm=[0;dPm]
plot(T(1:$-floor(N/2)),dPm(1+floor(N/2):$),'k-')
plot(T,dP,'r.')


for i = 1:length(dP)
    dPm(i) = mean(dP(max([1 i-N]):i))
end
subplot(3,1,3)
title("Moyenne de la dérivée")

plot(T(1:$-floor(N/2)),dPm(1+floor(N/2):$),'k-')
plot(T,dP,'r.')

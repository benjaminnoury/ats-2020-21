% Partie SLCI 
% BN
% 26/01/18

# Étude de l'asservissement de position

## Contexte

La position angulaire du timon est traduite par un potentiomètre lié à l’axe de ce timon et qui donne une information électrique transmise à un calculateur : c’est la consigne de position, analogique, notée **w ( t )** (voir ﬁgure « Obtention du signal de commande du modulateur d’énergie »). 
Le calculateur reçoit également une information position angulaire du support orientable de roue 6 , grâce à un potentiomètre mis en rotation par la couronne d’entraînement de ce support orientable. Ce capteur de position fournit un signal analogique noté **« m ( t ) »**.

La partie analogique du montage fonctionne avec une alimentation en – 12V , +12 V réalisée à partir de la tension batterie U b = 24 V . Les signaux w ( t ) et m ( t ) sont toujours compris entre – 10V et +10 V .

Les signaux w ( t ) et m ( t ) sont convertis en informations numériques notées respectivement w n ( t ) et m n ( t ) à l’entrée du calculateur. Le calculateur génère le signal de commande e n ( t ) dont la conversion analogique est le signal e ( t ) envoyé à la commande du hacheur.

e ( t ) est une tension évoluant entre – 10V et +10 V . Lors de l’envoi d’une tension e ( t ) positive, le hacheur 1 est commandé, lors de l’envoi d’une tension e ( t ) négative, c’est le hacheur 2 qui est commandé. Chaque hacheur délivre au moteur une tension proportionnelle à la valeur absolue de la tension e ( t ) qu’il reçoit et pour une tension e ( t ) positive, on donne U ( t ) = b ⋅ e ( t ) , avec b = 2 , 4 .

U ( t ) est donc une tension continue dont la valeur est réglée par e ( t ) .

![](./img/CAN.png "Image Modulateur")

Dans toute cette partie, sauf indication contraire, le couple résistant ramené sur l’arbre du moteur et lié au frottement sec des roues sur le sol sera pris égal à sa valeur maximale soit : C_r_ = C_rm_ = 1 , 4 Nm .

## Évaluation de la nécessité d’une boucle de courant

Un essai préliminaire du moteur, monté sur le chariot, dans les conditions donnant C r = C rm et alimenté par un échelon de 24 V a donné la réponse en vitesse et courant donnée ci-après  

![Moteur : Réponse à un échelon de 24V](./img/courbeMoteur.png)


Un retard apparaît entre l’application de l’échelon de tension d’induit ( t = 0 ) et le début de la montée en vitesse du rotor du moteur.

**Q** Expliquer son origine et justiﬁer la valeur stabilisée du courant. Lors de cet essai, le courant ne dépasse pas le courant maximal ﬁxé à 25 A . Quel pourrait être l'intérêt d'un dispositif de limitation du courant max ?


### Réglage de la boucle de position

Du fait de la variation du ﬂux avec le courant d’induit propre au moteur à excitation série, la relation vitesse de rotation en fonction de la tension d’induit du moteur est une relation non linéaire. Cependant, pour de petites variations de tension du moteur autour d’un point de fonctionnement donné, cette relation peut être linéarisée et une fonction de transfert déﬁnie. On montre que cette fonction de transfert est du second ordre :


$H_m (p ) = (H_m0) / (1+ 2 z p/w_0 +p^2/w_0)$


Les paramètres H_m0 , z et w_0 dépendent cependant du point de fonctionnement autour duquel on étudie le système. 

p représente la variable de  Laplace. Le schéma bloc de la boucle de position est donné ﬁgure « Boucle de position » ci-après.


![Boucle de position](./img/SchBloc.png)


Ce schéma bloc n’est utilisable que pour des signaux de faible amplitude. Le correcteur est de type proportionnel et on pose C ( p ) = K (réel positif). 
On donne K r = 6 , 37 V ⋅ rad .

**Q** Ques représentent les blocs de fonction de transfert  **1/p** et **1/81**. 

**Q** Exprimer la fonction de tranfert en boucle ouverte du système notée $T(p)$

**Q** Quelle est la valeur de l'erreur indicielle ? En déduire l'angle de rotation de la roue correspondant à  **W= + 10 V**


Dans les conditions les plus défavorables pour la stabilité de la boucle de position, un échelon de 713 mV débutant
à t = 0 , 2 s a été appliqué au moteur. Sa réponse en vitesse est donnée sur la figure ci-dessous.

**Q** Relever la valeur finale, le temps de réponse à 5\% et le dépassement relatif de cette réponse. En vous aidant des abaques fournies, déterminer le gain statique **H_m0** , le facteur d'amortissement **z** et la pulsation propre **w_0** de la fonction de transfert du moteur. 


Le diagramme de bode du système complet, incluant le modèle de comportement de $H_m0(p)$ obtenu ci-dessus est tracé sur le document réponse **Numero**.

**Q** Le système est-il stable ?

**Q** Quelle la valeur de K permettant d'obtenir le système le plus rapide possible, tout en conservant une marge de phase de 45° et une marge de gain de 10 dB. Vous réaliserez les tracés nécessaire à votre réponse sur le document réponse **Numero**.


On fixe pour la suite K = 10


### Étude des conditions d'arrêt du moteur

Le modulateur d’énergie ne permet pas de freinage électrique, ce qui signifie que le freinage du moteur sera uniquement dû au couple résistant du moteur. On rappelle que ce couple est de type frottement solide et est donc indépendant de la vitesse.

Le moteur alimenté sous 24 V et chargé par le couple résistant $C_r=C_rm=1,4 Nm$, tourne à 138 rad.s^-1^. On coupe alors son alimentation.

Le Principe fondamental de la dynamique appliqué à l'ensemble des charges mobiles en projection sur l'axe de l'abre moteur donne :

$$
 - C_R = J  cdot ("d" \Omega (t) ) / ("d" t)
$$

Avec $J$ l'inertie totale ramenée à l'axe du moteur : J = 5,55 . 10^-4^ kg.m^2^

**Q** Représenter par l'évolution de la vitesse $\Omega(t)$ en fonction du temps.  Déterminer le temps d'arrêt du moteur.

**Q** De quels angle ont tourné l'arbre du moteur et celui du suport de roue pendant ce temps ?

**Q** Avec les réglages envisagés, déterminer l’écart angulaire entre la position roue demandée et la position roue courante pour laquelle le moteur cesse d’être alimenté à tension maximale. Comparer au résultat de la question précédente et conclure.

### Validation d'une donnée constructeur : durée d'un demi-tour

L’objectif de cette question est de valider le respect de la durée de 2 s maximale imposée par le cahier des charges pour réaliser un demi-tour du support orientable de roue motrice 6 .

La consigne de position passe de -10 V à +10 V, soit une rotation demandée de $- \pi/2$ à $+ \pi/2$

**Q** Montrer que, dès l’application de l’échelon en entrée, le moteur est alimenté sous tension maximale ( 24 V ) .


Dans ces conditions, les réponses en courant et vitesse du moteur sont celles données par la figure « Moteur : réponse à un échelon de tension de 24 V » (voir **page  **). On linéarise la courbe de vitesse de la manière suivante : on suppose que l’accélération est constante de t = 0 à t = 50 ms et qu’à t = 50 ms la vitesse
maximale est atteinte.

**Q** Déterminer la valeur de l’accélération angulaire. En déduire l’angle de rotation de l’arbre moteur et celui du support de la roue pendant ce démarrage.

La phase de ralentissement de la rotation de la roue commence 10° avant la fin de la rotation et dure 205 ms .

**Q** Déterminer la durée de la rotation à vitesse constante du support de la roue, puis conclure quant au respect du cahier des charges par la détermination de la durée totale du demi-tour du support de la roue.

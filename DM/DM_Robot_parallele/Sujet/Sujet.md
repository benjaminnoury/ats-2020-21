---
titre: Robot à parallélogrammes déformables
type: DM 1 --
classe: ATS
CI: 7 Transmettre l'énergie - Mouvements
# competence: 
# question-section: 1
# document: twocolumn,landscape
---

Le système étudié est un robot industriel destiné à la manutention de pièces lourdes (voir photos et schéma cinématique ci-dessous). Ce robot a une structure en parallélogramme déformable qui lui permet de déplacer son poignet dans l’aire de travail.

Ce type de structure de robot permet d'obtenir de grandes dynamiques (grandes vitesses et grandes accélérations) par rapport à un robot sériel. La contrepartie technique est la difficulté à élaborer la commande du robot.

\Begin{rem}{Objectif}

L'objectif de l'étude, est :

- d'une part, déterminer la surface de travail du robot ;
- d'autre part, obtenir la vitesse et l'accélération en bout d'effecteur en fonction des paramètres de la motorisation.

\End{rem}

\Begin{center}

\includegraphics[width=.3\lw]{img/photo1} \hfill
\includegraphics[width=.34\lw]{img/photo2} \hfill
\includegraphics[width=.3\lw]{img/photo3}


\includegraphics[width=.3\lw]{img/photo4}
\includegraphics[width=.6\lw]{img/schemaC}

\End{center}

On associe à chaque solide (i) une base orthonormée directe $B_{i}=( \vec x_{i} , \vec y_{i}, \vec z )$. Par exemple au solide (2) est associée la base $B_{2} = (\vec x_{2},\vec y_{2},\vec z)$.

L’outil de manutention est fixé au point J et le socle est le solide (0). La pièce (3) est l’ensemble des segments [CB] et [BJ], elle a donc une longueur CJ.

Au point A, il y a :

- Une liaison pivot entre (0) et (1) motorisée par un moteur $M_{1}$ tel que $(\vec x_{0}, \vec x_{1})=\alpha$ avec $\alpha \in \left [\frac{\pi}{3} ; \frac{2 \pi}{3} \right ]$ ;
- une liaison pivot entre (0) et (2) motorisée par un moteur $M_{2}$ tel que $(\vec x_{0}, \vec x_{2})=\beta$ avec $\beta \in \left [\frac{-\pi}{4} ; \frac{\pi}{4} \right ]$.

\ 

Ces deux motorisations sont bien sûr indépendantes.

On considérera le robot comme ayant un mouvement plan.

La géométrie est telle que : $AB=EC=L$ ; $EA = CB = D$ ; $BJ = H$ .

\question{Que signifie l'hypothèse de mouvement plan du mécanisme ?}

\question{Tracer le graphe des liaisons du mécanisme.}

\question{Que peut-on dire que les bases $B_{1}$ et $B_{4}$ d'une part et $B_{2}$ et $B_{3}$ d'autre part? 

En déduire les vitesses de rotations $\ovr \Omega_{1/0}$, $\ovr \Omega_{2/0}$, $\ovr \Omega_{3/0}$ et $\ovr \Omega_{4/0}$.}


\question{Déterminer les torseurs $\tv{1/0}$ et $\tv{2/0}$ en fonction des paramètres $\alpha$, $\beta$ et des longueurs aux points les plus simples.

En déduire $\tv{1/0}$ au point B et $\tv{2/0}$ au point E.

En déduire les torseurs $\tv{4/0}$ et $\tv{3/0}$ en fonction des paramètres $\alpha$, $\beta$ et des longueurs respectivement au point E et au point B.}

\ 

### Configuration 1 : Le moteur $M_{1}$ est à l'arrêt tel que $\alpha=\frac{\pi}{3}$ et $M_{2}$ fonctionne.

\question{Définir puis tracer sur le Document réponse la trajectoire $T_{J,3/0}$.}

\question{Déterminer la vitesse $\V{J,3/0}$, puis tracer, sans notion d'échelle ce vecteur sur la figure. }

\ 


### Configuration 2 : Le moteur $M_{2}$ est à l'arrêt tel que $\beta=0$ et $M_{1}$ fonctionne.

\question{Définir puis tracer sur le Document réponse la trajectoire $T_{J,3/0}$.}

\question{Déterminer la vitesse $\V{J,3/0}$, puis tracer, sans notion d'échelle ce vecteur sur la figure. }

\ 

### Configuration 3 : les 2 moteurs $M_{1}$ et $M_{2}$ fonctionnent.


\question{Sur le document réponse, tracer :

\Begin{itemize}
	\item les points extrêmes :

\Begin{multicols}{2}

\Begin{itemize}
	\item $B_{1}$ et $J_{1}$ quand $\alpha=\dfrac{\pi}{3}$ et $\beta=\dfrac{\pi}{4}$
	
	\ 
	
	\item $B_{2}$ et $J_{2}$ quand $\alpha=\dfrac{2\pi}{3}$ et $\beta=\dfrac{\pi}{4}$
	
	\item $J_{3}$ quand $\alpha=\dfrac{2\pi}{3}$ et $\beta=\dfrac{-\pi}{4}$
	
	\ 
	
	\item $J_{4}$ quand $\alpha=\dfrac{\pi}{3}$ et $\beta=\dfrac{-\pi}{4}$
\End{itemize}	

\End{multicols}

\item la {\em surface de travail du robot}, c'est-à-dire la surface fermée obtenue par la succession des positions de $J$ lorsque $\alpha$ et $\beta$ varient 
( autrement dit, c'est la surface dans laquelle se déplace le point $J$ dans le mouvement de 3 par rapport à $R_{0}$ ).

\ 

Pour vous aider au tracé, les orientations des vecteurs $\vec x_{1}$ et $\vec x_{3}$ sont données dans les conditions à étudier.
	
\End{itemize}

}


\ 

\ 


Lorsque les 2 moteurs sont en fonctionnement, d'après le théorème de superposition, la vitesse  $\V{J,3/0}$ est obtenue par :
$$
\V{J,3/0}
=\left . \V{J,3/0} \right |_{M_{1} \text{ bloqué}}+\left . \V{J,3/0} \right |_{M_{2} \text{ bloqué}} 
$$


\question{En déduire l'accélération $\vec \Gamma_{J,3/0}$}

\newpage

### \fbox{Document réponse}


### Configuration 1

\image[.7]{img/DR_conf1}

### Configuration 2

\image[.6]{img/DR_conf2}

\newpage
### Configuration 3

\ 

\ 

\ 


\image[.7]{img/DR_conf3}

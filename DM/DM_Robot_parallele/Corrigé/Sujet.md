---
titre: Corrigé DS2 - Robot parallèle
type: Corrigé DS2
classe: TSI
CI: "7 : Transmettre l'énergie - Mouvements"
#competence: 
#question-section: 1
document: twocolumn,landscape
---

\question{Cf. cours \emph{Cinématique du Solide} p.17}

\question{

\centering	\includegraphics{img/grapheLiaisons}}

\question{Du fait de la géométrie du mécanisme, en tout instant, les vecteurs de la base $B_{4}$ sont parallèles à ceux de la base $B_{1}$, donc $B_{1}=B_{4}$. De même, $B_{3}=B_{2}$

On en déduit : $\ovr \Omega_{1/0}=\ovr \Omega_{4/0}=\dot \alpha \cdot \vec z$ et $\ovr \Omega_{2/0}=\ovr \Omega_{3/0}=\dot \beta \cdot \vec z$}


\question{
	$$\tv{1/0}= \tc{A}{ \dot \alpha \cdot \vec z \\ \vec 0}
	\qquad
	\tv{2/0}=\tc{A}{\dot \beta \cdot \vec z\\ \vec 0 }$$	

$$
\tv{3/0}=\tv{3/1}+\tv{1/0}=\tc{B}{ \ovr \Omega_{3/1}\\ \vec 0 } + \tc{B}{ \ovr \Omega_{1/0} \\ \V{B,1/0}} = \tc{B}{ \ovr \Omega_{3/0}\\ \V{B,1/0} }
$$

Or $\V{B,1/0}=\V{A,1/0}+\ovr{BA}\wedge \ovr \Omega_{1/0}=\vec 0 - L \cdot \vec x_{1} \wedge \dot \alpha \cdot \vec z = L \cdot \dot \alpha \cdot \vec y_{1}$, 

$$\text{d'où }\boxed{\tv{3/0} = \tc{B}{\dot \beta \vec z \\ L \cdot \dot \alpha \cdot \vec y_{1} }}$$
}

De même,

$$
\tv{4/0}=\tv{4/2}+\tv{2/0}=\tc{E}{ \ovr \Omega_{4/2}\\ \vec 0} + \tc{E}{ \ovr \Omega_{2/0}\\\V{E,2/0}}
$$

Or $\V{E,2/0} = \V{A,2/0}+\ovr{EA}\wedge \ovr \Omega_{2/0}=\vec 0 + D \cdot \vec x_{2} \wedge \dot \beta \cdot \vec z = - D	\cdot \dot \beta \cdot \vec y_{2}$


$$\text{d'où }\boxed{\tv{4/0} = \tc{E}{ \dot \alpha \cdot \vec z\\ - D	\cdot \dot \beta \cdot \vec y_{2}}}$$

### Configuration 1 : Le moteur $M_{1}$ est à l'arrêt tel que $\alpha=\frac{\pi}{3}$ et $M_{2}$ fonctionne.

Dans ce cas, $\dot \alpha = 0$, donc $\tv{3/0}=\tc{B}{\dot \beta \cdot z\\ \vec 0}$. Le solide 3 est donc en rotation d'axe $(B,\vec z)$ par rapport au bâti 0.

\Begin{multicols}{2}

\question{\image{img/Corr_Traj_1.png}}

\question{$$\begin{aligned}
\V{J,3/0}& =\V{B,3/0} + \ovr{JB}\wedge \ovr \Omega_{3/0}\\
& =\vec 0 - H \cdot \vec x_{3} \wedge \dot \beta \vec z \\
& = H \dot \beta \cdot \vec y_{3}
\end{aligned}$$

$$\boxed{\left . \V{J,3/0}  \right |_{M_{1} \text{ bloqué}}= H \dot \beta \cdot \vec y_{3}}$$

}
\End{multicols}

### Configuration 2 : Le moteur $M_{2}$ est à l'arrêt tel que $\beta=0$ et $M_{1}$ fonctionne.

Dans ce cas, $\dot \beta = 0$, donc $\tv{3/0}=\tc{B}{\vec 0\\ L \dot \alpha \cdot \vec y_{1}}$. 
Le solide 3 est donc en translation par rapport au bâti 0.

\Begin{multicols}{2}
\question{
\image{img/Corr_Traj_2}

Le mouvement (3/0) est une translation, donc tous les points ont une trajectoire dans (3/0) supperposable.

$B$ étant sur l'axe de la liaison (3/1), $T_{B,3/0}=T_{B,1/0}$. Le mouvement (1/0) étant une rotation d'axe $(A,\vec z)$, la trajectoire $T_{B}(1/0)=T_{B,3/0}$ est un arc de cercle de centre $A$ et de rayon $AB$.

Donc $T_{J,3/0}$ est un arc de cercle de rayon $AB$ et de centre $A'$ avec $\ovr{AA'}=\ovr{BJ}$

\question{$$\boxed{
\left . \V{J,3/0} \right |_{M_{2} \text{ bloqué}}=\V{B,3/0}=L \cdot \dot \alpha \cdot \vec y_{1}}
$$}
}
\End{multicols}
\newpage
\question{
	\image[.5]{img/Corr_Traj_3.png}
}


\question{
$$
\V{J,3/0}
=\left . \V{J,3/0} \right |_{M_{1} \text{ bloqué}}+\left . \V{J,3/0} \right |_{M_{2} \text{ bloqué}} 
$$

$$\boxed{\V{J,3/0} = L \cdot \dot \alpha \cdot \vec y_{1} + H \cdot \dot \beta \cdot \vec y_{3}}
$$



$$
\begin{aligned}
\ovr{\Gamma}_{J,3/0} & 
= L \left (\ddot \alpha \cdot  \vec y_{1} + \dot \alpha \left [ \ddt[\vec y_{1}] \right]_{R_{0}}\right ) + H \left (\ddot \beta \cdot \vec y_{3} + \dot \beta \left [ \ddt[\vec y_{3}] \right]_{R_{0}} \right ) \\
& = L \left (\ddot \alpha \cdot  \vec y_{1} - \dot \alpha^{2} \vec x_{1} \right ) + H \left (\ddot \beta \cdot \vec y_{3} - \dot \beta^{2} \vec x_{3} \right ) 
\end{aligned}
$$	

$$
\boxed{ \ovr{\Gamma}_{J,3/0} = L \left (\ddot \alpha \cdot  \vec y_{1} - \dot \alpha^{2} \vec x_{1} \right ) + H \left (\ddot \beta \cdot \vec y_{3} - \dot \beta^{2} \vec x_{3} \right ) 
}
$$

}
